#!/usr/bin/python

import sys
import os

class Seq:
    def __init__(self, name):
        self.name = name
        self.lines = []

    def add_line(self, line):
        self.lines.append(line.strip())

    def get_text(self, line_len=50):
        txt = ''.join(self.lines)
        num_lines = int(len(txt) / line_len)
        new_lines = [self.name]

        if line_len > len(txt):
            if (int(self.name.split('_')[1].strip()) % 2 == 0):
                new_lines = txt + ' +1\n';
            else:
                new_lines = txt + ' -1\n';
            return new_lines
        
        for i in range( num_lines ):
            line = txt[i*line_len:(i+1)*line_len];
            new_lines.append(line + '\n');

        if (num_lines*line_len < len(txt)):
            new_lines.append(txt[num_lines*line_len:] + '\n');

        return ''.join(new_lines)
            

def main():
    filename = sys.argv[1]
    try:
        about = os.stat(filename)
    except OSError:
        print "The file (" + filename  + ")  does not exist or is not accessible"
        sys.exit(1)
        
    h = open(filename);
    lines = h.readlines();
    h.close();
    
    seqs = []
    for line in lines:
        if line[0] == '>':
            seqs.append(Seq(line))
            continue    
        seqs[-1].add_line(line)

    out = []
    for seq in seqs:
        out.append(seq.get_text(1000000))

    new_filename = filename + '.normed';
    f = open(new_filename, 'w');
    f.writelines(out);
    f.close()

    

if __name__=='__main__':
    main()
