import sys
import re

try:
    fname = sys.argv[1]
    seqname = sys.argv[2]
except:
    print 'Usage: gbk_to_fasta.py  gbk_fname  seq_name'
    sys.exit(1)

f = open(fname)
lines = f.readlines()
f.close()

for i, line in enumerate(lines):
    if line[:6]=='ORIGIN':
        i = i + 1
        break
j = lines.index("//\n")
lines = lines[i:j]
    
for i, line in enumerate(lines):
    lines[i] = re.sub(r'\s+[0-9]+(.*)', r'\1', line)
    lines[i] = lines[i].replace(' ', '')


lines.insert(0, '>' + seqname + '\n')

fname = fname.replace(".gbk", "")
fname = fname + ".fa"
f = open(fname, "w")
f.writelines(lines)
f.close()
    
