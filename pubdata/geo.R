
library(GEOquery)


geo.con.singleton <- NULL
get.geo.meta.con <- function(gse=NULL, gsm=NULL, check.db=F) {

    if (!is.null(geo.con.singleton)) return(geo.con.singleton)
  library(GEOmetadb)
  geo.metadb.dir <- "/home/aarvey/data/geo"
  geo.metadb.file <- "GEOmetadb.sqlite"
  geo.metadb.fname <- sprintf("%s/%s", geo.metadb.dir, geo.metadb.file)
   
  if (check.db) {
      if (!file.exists(geo.metadb.fname)) getSQLiteFile(geo.metadb.dir)
      sql.db.age <- as.numeric(format(Sys.time(), "%s")) - as.numeric(file.info("GEOmetadb.sqlite")["mtime"])
      if (sql.db.age > (60*60*24*30)) getSQLiteFile(geo.metadb.dir)
  }
  
  con <- dbConnect(SQLite(), geo.metadb.fname)
  geo.con.singleton <<- con
  return(con)
}



geo.gse.titles <- function(gse=NULL, string.format=F) {
    con <- get.geo.meta.con()
    ##source.fname <- "input/geo_gses.txt"
    ##accs <- readLines(source.fname)
    ##accs <- gsub(" ", "", accs)
    titles <- sapply(gse, function(x) { unlist(as.character(dbGetQuery(con,sprintf('select title from gse where gse.gse=="%s"',x))["title"])) })
    len <- 110
    if (string.format)
        titles <- sapply(titles, function(x) { paste(sapply(1:ceiling(nchar(x)/len), function(i){substr(x, (i-1)*len+1, i*len)}), collapse="\n         ") })
    return(titles)
}



geo.gse2gsm <- function(gse=NULL) {
    gsms <- dbGetQuery(con,sprintf('select gsm from gse_gsm where gse=="%s"',gse))[,1]
    gsm.names <- sapply(gsms, function(x) { gsub(", ", "_", dbGetQuery(con,sprintf('select title from gsm where gsm.gsm=="%s"',x))) })
    return(gsm.names)
}




geo.meta.test <- function() {
    con <- get.geo.meta.con()    
    geo.tables <- dbListTables(con)
    dbListFields(con,'gse')
    dbListFields(con,'gse_gsm')
    dbListFields(con,'gsm')
    dbGetQuery(con,'select * from gse_gsm limit 5')
    dbGetQuery(con,'select * from gse limit 5')[,1:5]
    dbGetQuery(con,'select * from gsm limit 5')[,1:5]
    rs <- dbGetQuery(con,'select * from gse where gse.gse=="GSE1"')
    ##skip <- c("GSE26478", "GSE22886", "GSE29094", "GSE7460", "GSE2554", "GSE2770", "GSE2389")
    
    source.fname <- "array_gse_human.txt"
    source.fname <- "~/chip_pbm/encode/ebv_gses.txt"
    source.fname <-  "array_gses.txt"
    accs <- readLines(source.fname)
    accs <- accs[which(substr(accs,1,1)!="#")]
    accs <- sapply(strsplit(accs, "#"), "[[", 1)
    accs <- gsub(" ", "", accs)
    titles <- sapply(accs, function(x) { unlist(as.character(dbGetQuery(con,sprintf('select title from gse where gse.gse=="%s"',x))["title"])) })
    if (F) {
        contacts <- sapply(accs, function(x) { dbGetQuery(con,sprintf('select contributor from gse where gse.gse=="%s"',x)) })
        summaries <- sapply(accs, function(x) { dbGetQuery(con,sprintf('select summary from gse where gse.gse=="%s"',x)) })
        org <- sapply(accs, function(x) { paste(unique(dbGetQuery(con,sprintf('select organism_ch1 from gsm where series_id=="%s"',x))[,1]),collapse=",") })
        out <- cbind(accs, org, titles, summaries)
        tmp <- as.matrix(paste(org, titles)); rownames(tmp) <- accs; tmp
        write.table(out, sprintf("%s_gse_table.txt", source.fname))
    }



    source.fname <- "input/geo_gses.txt"
    accs <- readLines(source.fname)
    accs <- gsub(" ", "", accs)
    titles <- sapply(accs, function(x) { unlist(as.character(dbGetQuery(con,sprintf('select title from gse where gse.gse=="%s"',x))["title"])) })
    
    
    
    contacts <- sapply(accs, function(x) { dbGetQuery(con,sprintf('select contributor from gse where gse.gse=="%s"',x)) })
    summaries <- sapply(accs, function(x) { dbGetQuery(con,sprintf('select summary from gse where gse.gse=="%s"',x)) })
    org <- sapply(accs, function(x) { paste(unique(dbGetQuery(con,sprintf('select organism_ch1 from gsm where series_id=="%s"',x))[,1]),collapse=",") })
    out <- cbind(accs, org, titles, summaries)
    tmp <- as.matrix(paste(org, titles)); rownames(tmp) <- accs; tmp
    write.table(out, sprintf("%s_gse_table.txt", source.fname))
    
    
    
    ## load series and platform data from GEO
    gset <- getGEO("GSE9650", GSEMatrix =TRUE)
    if (length(gset) > 1) idx <- grep("GPL81", attr(gset, "names")) else idx <- 1
    gset <- gset[[idx]]
    
    
    names(fData(gset))
    sapply(fData(gset), head)
    gset.s = gset[!is.na(fData(gset)[["Gene Symbol"]]), ]
    probes = tapply(apply(exprs(gset.s), 1, sd), as.factor(as.vector(fData(gset.s)[["Gene Symbol"]])),
        function(x) { names(x)[x==max(x)]})
    gset = gset.s[probes,]
    gset.s <- gset
    gs <- fData(gset)[["Gene Symbol"]]
    names(gs) <- fData(gset)[["ID"]]
    rownames(gset.s) <- gs[rownames(gset.s)]
  
  show(pData(gset.s)$group <- as.factor(tolower(gsub(" ", ".", gsub(" *$", "", gsub("infection| *$|\\+|22\\-", "",
                                                                                    sapply(strsplit(pData(gset.s)$title, "GP"), "[[", 1)))))))
  gset <- gset.s
  exprs(gset) <- log2(exprs(gset))
      design = model.matrix(~0 + group, pData(gset))
  colnames(design) = make.names(colnames(design))
  colnames(design)[1:4] = levels(pData(gset)$group)
  
  #ctr = makeContrasts(CN <- vs <- U = day.35.chronic - day.0,
  #    R8 <- vs <- U = day.8.acute - day.0,
  #    R30 <- vs <- U = day.30.acute - day.0,
  #    CN <- vs <- R = day.35.chronic - day.30.acute,
  #    levels = design)
  fit = lmFit(gset, design)
  fit = contrasts.fit(fit, ctr)
  fit = eBayes(fit)
  
  outs = sapply(colnames(fit), function(x){
      topTable(fit,coef=x,number=nrow(fit),sort.by="none")
  },simplify=F)
  
  save(outs, fit, gset, file="murine_exhaustion.rda")
  
  
}



    








get.gse.sets <- function() {
    library(GEOquery)
    library(GEOmetadb)
    con <- dbConnect(SQLite(),'~/unagidata/foxp3/GEOmetadb.sqlite')
    
    acc <- "GSE29123"
    ##gse.to.sets[["GSE4243"]] <- c(GSM96793="Treg.control", GSM96794="AICD.resistant.Treg")
    for (acc in accs) {
        resp <- dbGetQuery(con,sprintf('select gpl.title from gpl, gse_gpl where gse_gpl.gse=="%s" AND gpl.gpl==gse_gpl.gpl',acc,acc))
        if (length(grep("Affy", resp)) > 0) {
            resp <- dbGetQuery(con,sprintf('select gsm.* from gsm, gse_gsm where gse_gsm.gse=="%s" AND gsm.gsm==gse_gsm.gsm',acc,acc))
            cat(sprintf('gse.to.sets[["%s"]] <- c(', acc))
            cat(paste(sprintf('%s="%s"', resp[,"gsm"], resp[,"title"]), collapse=",\n"))
            cat(")\n\n")
        }
    }
    


        acc <- "GSE38266"
        response <- dbGetQuery(con,sprintf('select gsm.* from gsm, gse_gsm where gse_gsm.gse=="%s" AND gsm.gsm==gse_gsm.gsm',acc,acc))
        names <- response[,"title"]
        names <- gsub("\\-", "neg", gsub("\\+", "pos", gsub(" HNSCC sample FFPE [0-9]*", "", names)))
        cat(c(sprintf('gse.to.sets[["%s"]] <- c(\n', acc),  paste(sprintf('%s="%s"', response[,"gsm"], names), collapse=",\n"), ")\n\n"), sep="")


        acc <- "GSE29297"
        response <- dbGetQuery(con,sprintf('select gsm.* from gsm, gse_gsm where gse_gsm.gse=="%s" AND gsm.gsm==gse_gsm.gsm',acc,acc))
        names <- response[,"title"]
        names <- gsub(" ", "", gsub(" TES2 and IkBa super-repressor ", "ikb.sup", gsub("hr_rep_[A-C]", "", gsub(" TES2 Only ", "", names))))
        cat(c(sprintf('gse.to.sets[["%s"]] <- c(\n', acc),  paste(sprintf('%s="%s"', response[,"gsm"], names), collapse=",\n"), ")\n\n"), sep="")


        acc <- "GSE17721"
        response <- dbGetQuery(con,sprintf('select gsm.* from gsm, gse_gsm where gse_gsm.gse=="%s" AND gsm.gsm==gse_gsm.gsm',acc,acc))
        names <- response[,"title"]
        names <- gsub(",", ".", gsub("hours", "hr", gsub("| |, rep[0-9]|_", "", names)))
        cat(c(sprintf('gse.to.sets[["%s"]] <- c(\n', acc),  paste(sprintf('%s="%s"', response[,"gsm"], names), collapse=",\n"), ")\n\n"), sep="")


        acc <- "GSE11292"
        response <- dbGetQuery(con,sprintf('select gsm.* from gsm, gse_gsm where gse_gsm.gse=="%s" AND gsm.gsm==gse_gsm.gsm',acc,acc))
        names <- response[,"title"]
        names <- tolower(gsub("Human.", "", gsub("_repeated_|_", ".", names)))
        cat(c(sprintf('gse.to.sets[["%s"]] <- c(\n', acc),  paste(sprintf('%s="%s"', response[,"gsm"], names), collapse=",\n"), ")\n\n"), sep="")

        acc <- "GSE15759"
        response <- dbGetQuery(con,sprintf('select gsm.* from gsm, gse_gsm where gse_gsm.gse=="%s" AND gsm.gsm==gse_gsm.gsm',acc,acc))
        names <- response[,"title"]
        names <- gsub("macrophages ", "mphage.", gsub("dendritic cells ", "dendritic.", names))
        names <- gsub("CD14-deficient", "cd14.ko", gsub("wild type", "wt", names))
        names <- gsub("unstimulated", "unstim", gsub("stimulated with LPS ", "lps", names))
        names <- gsub("[()]", "", gsub("for | in presence of | ", ".", gsub(" hours", "hr", names)))
        cat(c(sprintf('gse.to.sets[["%s"]] <- c(\n', acc),  paste(sprintf('%s="%s"', response[,"gsm"], names), collapse=",\n"), ")\n\n"), sep="")


        acc <- "GSE17372"
        response <- dbGetQuery(con,sprintf('select gsm.* from gsm, gse_gsm where gse_gsm.gse=="%s" AND gsm.gsm==gse_gsm.gsm',acc,acc))
        response <- response[grep("HIV", response$title),]
        names <- chars <- response[,"characteristics_ch1"]
        names[grep("ebv status: positive", names)] <- "ebvpos"
        names[grep("ebv status: negative", names)] <- "ebvneg"
        chars <- gsub(" ", ".", tolower(sapply(strsplit(sapply(strsplit(chars, "molecular diagnosis: "), "[[", 2), ";"), "[[", 1)))
        names <- sprintf("%s.%s",names, chars)
        cat(c(sprintf('gse.to.sets[["%s"]] <- c(\n', acc),  paste(sprintf('%s="%s"', response[,"gsm"], names), collapse=",\n"), ")\n\n"), sep="")



        acc <- "GSE13996"
        response <- dbGetQuery(con,sprintf('select gsm.* from gsm, gse_gsm where gse_gsm.gse=="%s" AND gsm.gsm==gse_gsm.gsm',acc,acc))
        response <- response[grep("Age", response[,"characteristics_ch1"]),]
        response <- response[grep("Histology", response[,"characteristics_ch1"]),]
        response <- response[grep("EBV", response[,"characteristics_ch1"]),]
        names <- chars <- response[,"characteristics_ch1"]
        histology <- gsub("cellularity|sclerosis| ", "", tolower(sapply(strsplit(sapply(strsplit(chars, "Histology:"), "[[", 2), ";"), "[[", 1)))
        ebv.status <- gsub("negative", "ebv.neg", gsub("positive", "ebv.pos", tolower(sapply(strsplit(sapply(strsplit(chars, "EBV:"), "[[", 2), ";"), "[[", 1))))
        names <- paste(ebv.status, histology, sep=".")
        table(names)
        cat(c(sprintf('gse.to.sets[["%s"]] <- c(\n', acc),  paste(sprintf('%s="%s"', response[,"gsm"], names), collapse=",\n"), ")\n\n"), sep="")



        acc <- "GSE30788"
        response <- dbGetQuery(con,sprintf('select gsm.* from gsm, gse_gsm where gse_gsm.gse=="%s" AND gsm.gsm==gse_gsm.gsm',acc,acc))
        response <- response[grep("hpv", response[,"characteristics_ch1"]),]
        response <- response[grep("location", response[,"characteristics_ch1"]),]
        chars <- response[,"characteristics_ch1"]
        location <- gsub(" ", ".", gsub("^ ", "", gsub(" $", "", tolower(sapply(strsplit(sapply(strsplit(chars, "location:"), "[[", 2), ";"), "[[", 1)))))
        hpv.status <- gsub(" na", "na", gsub(" 0", "neg", gsub(" 1", "pos", tolower(sapply(strsplit(sapply(strsplit(chars, "hpv:"), "[[", 2), ";"), "[[", 1)))))
        names <- paste(location, hpv.status, sep=".hpv")
        table(names)
        cat(c(sprintf('gse.to.sets[["%s"]] <- c(\n', acc),  paste(sprintf('%s="%s"', response[,"gsm"], names), collapse=",\n"), ")\n\n"), sep="")


      acc <- "GSE9750"
        response <- dbGetQuery(con,sprintf('select gsm.* from gsm, gse_gsm where gse_gsm.gse=="%s" AND gsm.gsm==gse_gsm.gsm',acc,acc))
        chars <- response[,"characteristics_ch1"]
        type <- c(rep("line", 9), rep("normal", 24), rep("cancer", 33))
        cbind(type, chars)
        hpv.type <- c(rep("", 33), rep(".hpv16", length(chars)-33))
        hpv.type[intersect(grep("HPV 16", chars, invert=T), grep("HPV 45", chars))] <- ".hpv45"
        hpv.type[intersect(grep("HPV 16", chars, invert=T), grep("HPV 18", chars))] <- ".hpv18"
        hpv.type[grep("HPV negative", chars)] <- ".hpv.neg"
        cbind(hpv.type, chars)
        names <- paste(type, hpv.type, sep="")
        table(names)
        cat(c(sprintf('gse.to.sets[["%s"]] <- c(\n', acc),  paste(sprintf('%s="%s"', response[,"gsm"], names), collapse=",\n"), ")\n\n"), sep="")


        acc <- "GSE6791"
        lines <- system('curl "http://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?view=data&acc=GSE6791&id=3521&db=GeoDb_blob12"', intern=T)
        lines <- lines[grep("GSM", lines)]
        hpv.status <- gsub("\\-", "neg", gsub("\\+ ", "hpv", gsub("\\(|\\)", "", sapply(strsplit(lines, "\\t"), "[[", 4))))
        gsm <- gsub(" ", "", sapply(strsplit(lines, "\\t"), "[[", 12))
        type <- gsub(" ", "", sapply(strsplit(lines, "\\t"), "[[", 6))
        names <- tolower(gsub(" ", "", sprintf("%s.%s", type, hpv.status)))
        names[which(names %in% names(which(table(names) < 3)))] <- "other"
        cat(c(sprintf('gse.to.sets[["%s"]] <- c(\n', acc),  paste(sprintf('%s="%s"', gsm, names), collapse=",\n"), ")\n\n"), sep="")


        exist.accs <- sapply(strsplit(basename(system("find ~/ebv/array | grep 'ge.rda$'", intern=T)), "_"), "[[", 1)
        for (acc in exist.accs) {
                show(resp <- dbGetQuery(con,sprintf('select gse.title from gse where gse.gse=="%s"',acc,acc)))
                    next
                    resp <- dbGetQuery(con,sprintf('select gpl.title from gpl, gse_gpl where gse_gpl.gse=="%s" AND gpl.gpl==gse_gpl.gpl',acc,acc))
                    if (length(grep("Affy", resp)) > 0) {
                              resp <- dbGetQuery(con,sprintf('select gsm.* from gsm, gse_gsm where gse_gsm.gse=="%s" AND gsm.gsm==gse_gsm.gsm',acc,acc))
                                    cat(sprintf('gse.to.sets[["%s"]] <- c(', acc))
                                    cat(paste(sprintf('%s="%s"', resp[,"gsm"], resp[,"title"]), collapse=",\n"))
                                    cat(")\n\n")
                          }
            }



        acc <- "GSE11292"
        response <- dbGetQuery(con,sprintf('select gsm.* from gsm, gse_gsm where gse_gsm.gse=="%s" AND gsm.gsm==gse_gsm.gsm',acc,acc))
        names <- response[,"title"]
        names <- gsub("Human_|min|_repeated", "", names)
        table(names)
        cat(c(sprintf('gse.to.sets[["%s"]] <- c(\n', acc),  paste(sprintf('%s="%s"', response[,"gsm"], names), collapse=",\n"), ")\n\n"), sep="")



        acc <- "GSE38686"
        response <- dbGetQuery(con,sprintf('select gsm.* from gsm, gse_gsm where gse_gsm.gse=="%s" AND gsm.gsm==gse_gsm.gsm',acc,acc))
        names <- response[,"title"]
        names <- gsub("_", ".", tolower(gsub("iTreg_|_rep[1-3]", "", names)))
        table(names)
        cat(c(sprintf('gse.to.sets[["%s"]] <- c(\n', acc),  paste(sprintf('%s="%s"', response[,"gsm"], names), collapse=",\n"), ")\n\n"), sep="")


        acc <- "GSE42276"
        response <- dbGetQuery(con,sprintf('select gsm.* from gsm, gse_gsm where gse_gsm.gse=="%s" AND gsm.gsm==gse_gsm.gsm',acc,acc))
        names <- response[,"title"]
        names <- gsub("[_\\-]", ".", tolower(gsub("iTreg_|#[1-9]", "", names)))
              table(names)
        cat(c(sprintf('gse.to.sets[["%s"]] <- c(\n', acc),  paste(sprintf('%s="%s"', response[,"gsm"], names), collapse=", "), ")\n\n"), sep="")

  }
