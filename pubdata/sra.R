

get.sra.info <- function(projs=NULL, runs=NULL, expmnts=NULL) {
    library(SRAdb)
    sra.dir <- '~/data/sra/'
    ##  sqlfile <- getSRAdbFile()
    con <- dbConnect(SQLite(),sprintf('%s/SRAmetadb.sqlite',sra.dir));
    dbListTables(con);     dbListFields(con,'experiment');     dbListFields(con,'run');     dbListFields(con,'study')
    

    
    if (sum(c(is.null(projs), is.null(runs), is.null(expmnts))) != 2)  stop("Please specify only one argument!")

    
    if (!is.null(runs)) {
        response <- c()
        for (run in runs) {
            query <- sprintf('select run.*  from run where run_accession="%s"', run)
            query <- sprintf('select run.*, experiment.title, experiment.sample_name from run, experiment where run.experiment_accession=experiment.experiment_accession AND run.run_accession="%s"', run)
            resp <- dbGetQuery(con,query)
            response[run] <- resp["title"]
        }
        return(response)
    }
    
    if (F) {
        fname.to.desc <- samp
        names(fname.to.desc) <- names(response)
        clock.fnames <- tmp.files <- sprintf("~/foxp3/clock/%s/%s.m_musculus_cs.bowtie.align.bam.rmdup.bam", names(fname.to.desc), names(fname.to.desc))
        names(clock.fnames) <- names(tmp.files) <- as.character(fname.to.desc)
        fname.desc.to.header(tmp.files)
    }
    
    if (!is.null(projs)) {
        response <- c()
        for (proj in projs) {
            response[[proj]] <- paste(dbGetQuery(con,sprintf('select study_alias, study_title from study where study_accession="%s"',proj)), collapse=" ------ ")
        }
        return(response)
        if (F) {
            expmnts <- dbGetQuery(con,sprintf('select experiment_accession from experiment where study_accession="%s"',proj))[,1]
            response[[proj]]$expmnts <- get.sra.info(runs=expmnts)
        }
        return(response)
    }

    if (!is.null(expmnts)) {
        response <- list()
        for (expmnt in expmnts) {
            query <- sprintf('select run.*, experiment.title, experiment.sample_name from run, experiment where run.experiment_accession=experiment.experiment_accession AND run.experiment_accession="%s" limit 5',
                             expmnt,expmnt)
            show(dbGetQuery(con,query))
        }
        return(response)
    }
}


