import local_blast
import sys

f = open('mouse_chip_chip.fasta.out')
lines = f.readlines()
f.close()

f = open('bulyk_domains.fasta')
blines = f.readlines()
f.close()

f = open('mouse_chip_chip_domains.fasta')
dlines = f.readlines()
f.close()

f = open('homeodomains_mouse.fa')
hlines = f.readlines()
f.close()





superspecres = [47, 50, 54]
specres = [3, 5, 6, 25, 31, 44, 46, 47, 48, 50, 51, 53, 54, 55, 57]
specres = [x-2 for x in specres]
superspecres = [x-2 for x in superspecres]
specrestext = [' ' for i in range(60)]
superspecrestext = [' ' for i in range(60)]
for i in specres:
    specrestext[i] = '*'
for i in superspecres:
    superspecrestext[i] = '*'
specrestext = ''.join(specrestext)
superspecrestext = ''.join(superspecrestext)

barx1 = 'RRSRTVFTELQLMGLEKRFEKQKYLSTPDRIDLAESLGLSQLQVKTWYQNRRMKWKK'
domains = []
class Domain:
    pass
for i in range(0,len(blines),2):
    d = Domain()
    d.name = blines[i].strip()
    d.seq = blines[i+1].strip()
    if len(d.seq) != len(barx1): continue
    d.contact = [d.seq[k] for k in specres]
    print d.name
    print specrestext
    print superspecrestext
    print d.seq
    print d.contact
    domains.append(d)

for i in range(0,len(dlines),2):
    name = dlines[i].strip()
    seq = dlines[i+1].strip()
    if len(seq) != len(barx1): continue
    contact = [seq[k] for k in specres]
    dists = []
    for d in domains:
        dists.append(sum([x==y  for x,y in zip(contact, d.contact)]))
    idx_max = []
    m = -1
    for j,d in enumerate(dists):
        if d >= m:
            m = d
    for j,d in enumerate(dists):
        if d >= m:
            idx_max.append(j)
    print 'matches', m
    print specrestext
    print superspecrestext
    print seq, name
    for idx in idx_max:
        print domains[idx].seq, domains[idx].name
        #print domains[idx].contact
    print contact
sys.exit(0)




def get_name(s, blines):
    for i,l in enumerate(blines):
        idx = l.find(s)
        if idx >= 0:
            return blines[i-1][2:].split(' ')[0].strip()

for i in range(0,len(lines),2):
    name = lines[i]
    seq = lines[i+1]
    print '-----------------------------------------------------------------'
    results = local_blast.blast_seq(seq, 'query', 'bulykdomains', 'blastp')
    if len(results)==0: continue
    for i,r in enumerate(results):
        if i==0 or r.query==r.subject:
            #print '(' + str(int(r.query==r.subject)) + ')'
            print 'Actual gene:', name.strip()[2:]
            print 'Best hit:   ', get_name(r.subject.replace('-',''), blines)
            print 'Query:', r.query
            print 'Match:', r.match
            print 'Hit:  ', r.subject

import sys
sys.exit(0)



f = open('pbm/domains.txt')
lines = f.readlines()
f.close()

for line in lines:
    if line.strip()=='': continue
    dname = line.split(' ')[0]
    #print '---------------------------------------------------------'
    print '>', dname
    
    seq = line.split(' ')[-1].strip()
    print seq
    continue
    results = local_blast.blast_seq(seq, 'query', 'uniprot_sprot_human', 'blastp')
    if len(results)==0: continue
    for i,r in enumerate(results):
        if i==0 or r.query==r.subject:
            print '(' + str(int(r.query==r.subject)) + ')'
            print r.query
            print r.match
            print r.subject
