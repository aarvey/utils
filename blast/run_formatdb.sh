# From EST data
#./blast/bin/formatdb -t cdna -i mrna.fa -l mrna.formatdb.log -p F -n cdna

# From chromsome data
#./blast/bin/formatdb -t genome -i chr_all.fa -l chr_all.log -p F -n genome

# From protein data
#./blast/bin/formatdb -t proteome -i uniprot_sprot_human.fasta -l uniprot_sprot_human.log -p T -n uniprot_sprot_human
./blast/bin/formatdb -t bulykdomains -i bulyk_domains.fasta -l bulyk_domains.log -p T -n bulykdomains

