#!/usr/bin/env python

# Take a protein and do local blast search. Based on BioPython example.


import os
import sys

from Bio.Blast import NCBIStandalone
from Bio.Blast import NCBIXML
from Bio.Clustalw import MultipleAlignCL
from Bio.Clustalw import do_alignment
from Bio.Align import AlignInfo
from Bio.Alphabet import IUPAC
from Bio.SubsMat import FreqTable
from Bio.Alphabet import Gapped
import random

def usage():
    print './local_blast.py   blast_database_name sequence [blast_program=blastn]   '
    

def main():

    if len(sys.argv) < 3:
        usage()
        sys.exit(2)


    blast_db = sys.argv[1]
    seq = sys.argv[2].replace('\n','').replace(' ','')
    blast_type = 'blastn'
    if len(sys.argv) > 3:
        blast_type = sys.argv[3]
    seq = sys.argv[2].replace('\n','').replace(' ','')
    
    blast_seq(seq, 'query', blast_db, blast_type)


def align_seq(fname, seqname):
    cline = MultipleAlignCL(fname)
    outfname = seqname + '.aln'
    cline.set_output(outfname,
                     output_order='INPUT')
    align = do_alignment(cline, alphabet=IUPAC.protein)
    a_len = align.get_alignment_length()
    a_info = AlignInfo.SummaryInfo(align)

    #for i in range(a_len):
    #    print a_info.get_column(i)

    #a_pssm = a_info.pos_specific_score_matrix(axis_seq=align.get_seq_by_num(0), chars_to_ignore = ['X'])
    #print a_pssm

    #expect_freq = {
    #    'A' : .3,
    #    'G' : .2,
    #    'T' : .3,
    #    'C' : .2}
    #freq_table = FreqTable.FreqTable(expect_freq, FreqTable.FREQ, IUAPAC.protein)

    #my_pssm = summary_align.pos_specific_score_matrix(consensus,  chars_to_ignore = ['N'])
    #freq_table_info = FreqTable.FreqTable(expect_freq, FreqTable.FREQ,IUPAC.unambiguous_dna)
    #info_content = summary_align.information_content(5, 30,
    #                                                                                              chars_to_ignore = ['N'],
    #                                                                                              e_freq_table = \
    #                                                                                              freq_table_info)
    #print a_info.information_content()

    #for seq in align.get_all_seqs():
    #    print seq.description
    #    print seq.seq
    return outfname

def blast_seq(query, queryname, blast_db, blast_type):
    f = open(queryname + '.fasta', 'w')
    f.write(query + '\n')
    f.close()
    blastcmd = 'blastall'
    filename = queryname + '.fasta'
    my_blast_db = os.path.join(os.getcwd(), blast_db)
    my_blast_file = os.path.join(os.getcwd(), filename)
    my_blast_exe = os.path.join(os.getcwd(), 'blast', 'bin', blastcmd)

    #print 'Running blastall...'
    #blast_out, error_info = NCBIStandalone.blastall(my_blast_exe, 'blastall'
    #                                                my_blast_db, my_blast_file)
    blast_out, error_info = NCBIStandalone.blastall(my_blast_exe, blast_type,
                                                    my_blast_db, my_blast_file,
                                                    nprocessors=1,
                                                    expectation=2)
    
    #print 'Parsing Error...'
    
    #while 1:
    #    print 'Attempting to read line from error...'
    #    print error_info.readline()
    #    s = error_info.readline().strip()
    #    print s
    #    if s=="":
    #        break

    s = ''
    #print 'Parsing results...'
    while 1:
        #s = blast_out.readline().strip()
        #print s
        if s=="":
            break
    #print 'Parsing results...'
    #b_parser = NCBIStandalone.BlastParser()
    #b_parser = NCBIXML.BlastParser()
    #b_records = NCBIStandalone.Iterator(blast_out, b_parser)
    records = NCBIXML.parse(blast_out)
    #print 'b_records is:', b_records
    
    num_records = 0
    #print 'Printing results...'
    

    #print 'Query is:', query
    #print 'Query length is:', len(query)

    class Result:
        pass
    results = []

    recs = []
    try: 
        for r in records:
            recs.append(r)
    except ValueError:
        print 'XML file was empty'
        return results
    
    records = recs    
            
            
    

    for record in records:
        if record is None:
            if nu_records <= 0:
                junk = 0
                #print 'There were no similar records'
            break
        #print 'record is', record     
        #print 'record num_hits', record.num_hits
        #print 'record alignments are', record.alignments
        
        #     print 'Parsed', num_records
        #     if num_records % 20 == 0:
        #         print 'Parsed', num_records

        #     num_records += 1
        #     genes = []
        
        matches = []
        titles = []
        E_VALUE_THRESH = 0.4
        for alignment in record.alignments:
            for hsp in alignment.hsps:
                if hsp.expect < E_VALUE_THRESH:
                    r = Result()
                    r.title = alignment.title
                    r.len =  len(hsp.sbjct)
                    r.e = hsp.expect
                    r.query = hsp.query
                    r.match = hsp.match
                    r.subject = hsp.sbjct
                    results.append(r)
                    #print '****Alignment****'
                    #print 'sequence:', alignment.title
                    #print 'Match sequence length:', len(hsp.sbjct)
                    #print 'Alignment length:', alignment.length
                    #print 'Alignment length:', alignment
                    #print 'e value:', hsp.expect

                    if len(hsp.query) > 75:
                        dots = '...'
                    else:
                        dots = ''

                    #print hsp.query[0:75] + dots
                    #print hsp.match[0:75] + dots
                    #print hsp.sbjct[0:75] + dots
                    title = alignment.title.replace('\n','')

                    #if title in titles: continue
                    #titles.append(title)
                    #matches.append(title + '\n' + hsp.sbjct.replace('-','') + '\n')

    def esort(x,y):
        x = x.e
        y = y.e
        if x>y:
            return 1
        elif x==y:
            return 0
        else: # x<y
            return -1

    results.sort(cmp=esort)

    #outfname = queryname + '_hsp.fasta'
    #f = open(outfname, 'w')
    #f.writelines(matches)
    #f.close()
    #print 'Done'
    #return outfname

    return results


if __name__=="__main__":
    main()
