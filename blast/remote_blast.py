#!/usr/bin/env python

# Take a protein and do local blast search. Based on BioPython example.


import os
import sys

from Bio.Blast import NCBIStandalone
from Bio.Blast import NCBIWWW
from Bio.Blast import NCBIXML
from Bio.Clustalw import MultipleAlignCL
from Bio.Clustalw import do_alignment
from Bio.Align import AlignInfo
from Bio.Alphabet import IUPAC
from Bio.SubsMat import FreqTable
from Bio.Alphabet import Gapped
import random

def usage():
    print './remote_blast.py  blast_database_name fasta_file [blast_program=blastn]   '
    

def main():

    if len(sys.argv) < 3:
        usage()
        sys.exit(2)


    blast_db = sys.argv[1]
    seq = sys.argv[2].replace('\n','').replace(' ','')
    blast_type = 'blastn'
    if len(sys.argv) > 3:
        blast_type = sys.argv[3]
    seq = sys.argv[2].replace('\n','').replace(' ','')
    
    blast_seq(seq, 'query', blast_db, blast_type)

def parse_platapus():
    nc2chr = {}
    for i in range(9094,9113):
        nc2chr['NC_00%d'%(i,)] = '%d' % (i-9093,)
    for i in range(9114,9118):
        nc2chr['NC_00%d'%(i,)] = '%d' % (i-9093,)
        

def blastable_species():
    species = {}
    species['Homo sapiens'] = 'human'
    species['Macaca mulatta'] = 'rhesus macaque'          
    species['Pan troglodytes'] = 'chimpanzee'          
    species['Mus musculus'] = 'laboratory mouse'          
    species['Rattus norvegicus'] = 'rat'          

    species['Bos taurus'] = 'cattle'          
    species['Canis lupus familiaris'] = 'dog'          
    species['Equus caballus'] = 'horse'          
    species['Felis catus'] = 'cat'          
    species['Ovis aries'] = 'sheep'          
    species['Sus scrofa'] = 'pig'          


    species['Ornithorhynchus anatinus'] = 'duck-billed platypus'          
    species['Monodelphis domestica'] = 'opossum'          

    species['Danio rerio'] = 'zebrafish'          
    species['Taeniopygia guttata'] = 'zebra finch'          
    species['Gallus gallus'] = 'chicken'          
    

    species['Acyrthosiphon pisum'] = 'pea aphid'          
    species['Anopheles gambiae'] = 'African malaria mosquito'          
    species['Apis mellifera'] = 'honey bee'          
    species['Drosophila melanogaster'] = 'fruit fly'          
    species['Drosophila pseudoobscura'] = ''          
    species['Nasonia vitripennis'] = 'jewel wasp'          
    species['Tribolium castaneum'] = 'red flour beetle'          

    return species
    

def blast_seq(query, queryname, blast_db, blast_type, blast_species="Homo sapiens"):

    if not blastable_species().has_key(blast_species):
        raise 'Species "' + blast_species + '" not supported'
    
    blast_out = NCBIWWW.qblast(blast_type, blast_db,  query, expect=20, entrez_query=blast_species+'[organism]')
    
    records = NCBIXML.parse(blast_out)
    
    num_records = 0

    #print 'Query is:', query
    #print 'Query length is:', len(query)

    class Result:
        pass
    results = []

    recs = []
    try: 
        for r in records:
            recs.append(r)
    except ValueError:
        print 'XML file was empty'
        return results
    
    records = recs    
            
            
    

    for record in records:
        if record is None:
            if nu_records <= 0:
                junk = 0
                #print 'There were no similar records'
            break

        print 'record is', record     
        print 'record num_hits', record.num_hits
        print 'record alignments are', record.alignments
        
        print 'Parsed', num_records
        if num_records % 20 == 0:
            print 'Parsed', num_records
            
        num_records += 1
        genes = []
        
        matches = []
        titles = []
        E_VALUE_THRESH = 10
        for alignment in record.alignments:
            for hsp in alignment.hsps:
                if hsp.expect < E_VALUE_THRESH:
                    r = Result()
                    r.title = alignment.title
                    r.len =  len(hsp.sbjct)
                    r.e = hsp.expect
                    r.query = hsp.query
                    r.match = hsp.match
                    r.subject = hsp.sbjct
                    results.append(r)
                    print '************************************************************************************'
                    print '************************************************************************************'
                    print '************************************************************************************'
                    print '****Alignment****'
                    print 'sequence:', alignment.title
                    print 'Match sequence length:', len(hsp.sbjct)
                    print 'Alignment length:', alignment.length
                    print 'Alignment length:', alignment
                    print 'e value:', hsp.expect

                    if len(hsp.query) > 75:
                        dots = '...'
                    else:
                        dots = ''

                    print 'Query[', hsp.query_start, '-', hsp.query_start+len(hsp.query), ']'
                    print hsp.query[0:75] + dots
                    print hsp.match[0:75] + dots
                    print hsp.sbjct[0:75] + dots
                    print 'Subject[', hsp.sbjct_start, '-', hsp.sbjct_start+len(hsp.sbjct), ']'
                    title = alignment.title.replace('\n','')

                    if title in titles: continue
                    titles.append(title)
                    matches.append(title + '\n' + hsp.sbjct.replace('-','') + '\n')

    def esort(x,y):
        x = x.e
        y = y.e
        if x>y:
            return 1
        elif x==y:
            return 0
        else: # x<y
            return -1

    results.sort(cmp=esort)

    #outfname = queryname + '_hsp.fasta'
    #f = open(outfname, 'w')
    #f.writelines(matches)
    #f.close()
    #print 'Done'
    #return outfname

    return results


if __name__=="__main__":
    main()
