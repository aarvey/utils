#!/usr/bin/python

import sys


def main():
    fname = sys.argv[1]
    f = open(fname)
    lines = f.readlines()
    f.close()
    for i in xrange(len(lines)):
        if lines[i][0]=="f": continue
        fl = float(lines[i])
        if fl > 0.99:
            lines[i] = '99\n'
        else:
            lines[i] = lines[i][2:4] + '\n'
    f = open(fname, 'w')
    f.writelines(lines)
    f.close()
             
if __name__=="__main__":
    main()
