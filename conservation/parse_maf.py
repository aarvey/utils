#!/usr/bin/python

import sys
import commands
import string


def main():
    fname = sys.argv[1]
    species = sys.argv[2:]
    was_gzipped = False
    if fname[-3:] == '.gz':
        commands.getoutput('gzip -d ' + fname)
        fname = fname[:-3]
        was_gzipped = True

    fname2alignments(fname, species, do_print=True)
        
    if was_gzipped:
        commands.getoutput('gzip ' + fname)

def fname2alignments(fname, species, do_print=False):

    f = open(fname)
    lines = f.readlines()
    f.close()
    
    alignments = []
    header = []
    for i,line in enumerate(lines):

        line = line.strip()
        if line == '':
            continue
        if line[0] == '#':
            header.append(line)
            continue
        if line[0] == 'a':
            alignments.append(Alignment(line, species))
            continue

        alignments[-1].addline(line,i)

        #if len(alignments) > 10000:
        #    break


    conserved = []
    for a in alignments:
        keep = False
        num_cons = 0
        for s in species[1:]:
            num_cons += int(a.hasspecies(s))
        if num_cons > 0:
            conserved.append(a)


    for c in conserved:
        c.tostring(species[0], do_print)

    return conserved

    







def fname_to_seq(fname):
    f = open(fname)
    lines = f.readlines()
    f.close()
    lines = map(string.strip, lines)
    seq = ''.join(lines[1:])
    return seq


class Seq:
    pass

class Conserved:
    pass

class Alignment:
    def __init__(self, line, soi):
        self.score = float(line.strip().split('=')[1])
        self.soi = soi
        self.spec2idx = {}
        self.seqs = []
        


    def hasspecies(self, spec):
        ret = [spec==x.spec  for x in self.seqs]
        return (sum(ret) > 0)

    def get_seq(self, spec):
        return self.seqs[self.spec2idx[spec]]
        
    def get_counts_aligned(self, spec):
        return self.counts_aligned[self.spec2idx[spec]]

    def get_seq_aligned(self, spec):
        return self.seqs_aligned[self.spec2idx[spec]]
        
    
    def addline(self, line, line_num):
        x = Seq()
        s = line.split()
        x.spec = s[1].split('.')[0]
        if self.soi.count(x.spec)==0:
            return
        x.chr = s[1].split('.')[1]
        x.start = int(s[2])
        x.len = int(s[3])
        x.strand = s[4]
        unknown = s[5]
        x.seq = s[6].upper()
        x.lnum = line_num
        
        self.spec2idx[x.spec] = len(self.seqs)
        self.seqs.append(x)

    
    def find_conserved(self, species):
        x = self.seqs[self.spec2idx[species]].seq
        counts = [0 for i in range(len(x))]
        aligned = []
        for s  in self.soi:
            if self.spec2idx.has_key(s) and s!=species:
                for i,c in enumerate(self.seqs[self.spec2idx[s]].seq):
                    counts[i] += (x[i]==c and x[i]!='-')
        for i,c in enumerate(self.seqs[self.spec2idx[species]].seq):
            if c!='-':
                aligned.append(i)
        self.counts = counts
        self.aligned = aligned
        self.seqs_aligned = []
        self.counts_aligned = []
        self.counts_aligned_str = []
        for s in self.seqs:
            self.seqs_aligned.append(''.join([s.seq[i] for i in self.aligned]))
        for s in self.seqs:
            self.counts_aligned_str.append(''.join([str(counts[i]) for i in self.aligned]))
            self.counts_aligned.append([counts[i] for i in self.aligned])
            

    def tostring(self, species, do_print=True):
        verbose = False
        use_chrM = False
        if verbose:
            print 'a score=%0.2f' % (self.score,)

            for x in self.seqs:
                if x.spec==species:
                    print 's', x.spec, x.chr, x.start, x.len, x.strand, x.lnum
            for x in self.seqs:
                if x.spec!=species and self.soi.count(x.spec) > 0:
                    print 's', x.spec, x.chr, x.start, x.len, x.strand, x.lnum

        
            print self.seqs[self.spec2idx[species]].seq
            for x in self.seqs:
                if x.spec!=species and self.soi.count(x.spec) > 0:
                    print x.seq

        self.find_conserved(species)
        if verbose:
            print ''.join([str(i) for i in self.counts])
            print ''


        if verbose and use_chrM:
            chrM_fname = '../../../RepeatMap/data/human/chrM.fa'
            chrM = fname_to_seq(chrM_fname)

            seq = self.seqs[self.spec2idx[species]].seq
            seq = ''.join([seq[i] for i in self.aligned])
            start = chrM.find(seq)
            length = len(seq)
            end = start + length
            print length
            print start, end
            print chrM[start:end]
            print ''

        if verbose:
            print seq
            for x in self.seqs:
                if x.spec!=species and self.soi.count(x.spec) > 0:
                    print ''.join([x.seq[i] for i in self.aligned])

            self.find_conserved(species)
            print ''.join([str(self.counts[i]) for i in self.aligned])
            print ''


        s = self.seqs[self.spec2idx[species]]
        sa = self.seqs_aligned[self.spec2idx[species]]
        sc = self.counts_aligned_str[self.spec2idx[species]]
        s.sa = sa
        s.sc = sc
        s.end = s.start + s.len
        if do_print:
            print s.chr, s.start, s.end, s.strand, s.sa, s.sc

if __name__ == '__main__':
    main()

