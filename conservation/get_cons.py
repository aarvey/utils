#!/usr/bin/python

import parse_ucsc_phastcons as cons
import time
import random
import commands
import sys
import traceback
random.seed(143)


def usage():
    print 'get_cons.py    REGIONS_FILE    CONSERVATION_DIR'
    print '  REGIONS_FILE has format name \\t chr \\t start \\t end'
    print 'NOTE: By default, the program does chr conversion for human'


class Regions:
    def __init__(self, fname):
        f = open(fname, 'r')
        lines = f.readlines()
        f.close()
        
        self.headers = lines[0].split('\t')
        self.h2i = {}
        for i,h in enumerate(self.headers):
            self.h2i[h] = i

        self.d = []
        for l in lines[1:]:
            s = l.strip().split('\t')
            if s[1]=='23':
                s[1] = 'X'
            if s[1]=='24':
                s[1] = 'Y'
            if s[1]=='25':
                s[1] = 'M'
            s[1] = s[1]
            s[2] = int(s[2])
            s[3] = int(s[3])
            self.d.append(s)

    def get_field(self, col, idx):
        return self.d[idx][self.h2i[col]]

    def len(self):
        return len(self.d)
        
def main():
    verbose=False

    print 'Reading in regions'
    region_fname = sys.argv[1]
    r = Regions(region_fname)

    conservation_dir = sys.argv[2]
    print 'Reading in conservation indices'
    chr2idx = {}
    cmd = 'ls '+conservation_dir+'/chr*.idx'
    print cmd
    fnames = commands.getoutput(cmd).split('\n')
    for f in fnames:
        ##print f
        chr2idx[f.split('/')[-1].split('.')[0]] = cons.Index(f, f.replace('.idx', ''), False)

    print chr2idx.keys()
    #print len(x)
    #sys.exit(0)
    print 'Outputting regions'
    outlines = []
    for i in range(r.len()):
        if i % 1000 == 0: print i
        name = r.get_field('name', i)
        chr = r.get_field('chr', i)
        start = r.get_field('start', i)
        end = r.get_field('end', i) + 1
        try:
            x = chr2idx[chr].get_region(start, end)
        except:
            #print x
            print name + '\t' + chr + '\t' + str(start) + '\t' + str(end)  #+'\t' + ' '.join(['%0.3f' % a for a in x])
            traceback.print_exc(file=sys.stdout)
            exceptionType, exceptionValue, exceptionTraceback = sys.exc_info()
            traceback.print_exception(exceptionType, exceptionValue, exceptionTraceback,
                                      limit=2, file=sys.stdout)
            traceback.print_exc()
            
            break

        #print name, chr, start, end, end-start
        #print i, len(x)
        if end-start != len(x):
            print name, chr, start, end, end-start
            print i, len(x)
            x = [-999 for a in range(end-start)]
            print 'Lengths dont match up!'
            
            
        s = name + '\t' + chr + '\t' + str(start) + '\t' + str(end) + '\t'
        s = ','.join(['%0.3f' % a for a in x]) + '\n'
        
        outlines.append(s)

    f = open('%s.cons' %(region_fname,),'w')
    f.writelines(outlines)
    f.close()
    
        

        


if __name__=='__main__':
    try:
        main()
    except:
        usage()
        print "Unexpected error:", sys.exc_info()[0]
        raise
