import parse_maf
import commands
import sys

def main():
    fnames = commands.getoutput('ls *.maf.gz').split('\n')
    print fnames
    for fname in fnames:
        print fname
        outlines = fname2mirs(fname)
        f = open(fname+'.out', 'w')
        f.writelines(outlines)
        f.close()
        


def fname2mirs(fname, verbose=False):
    was_gzipped = False
    if fname[-3:] == '.gz':
        commands.getoutput('gzip -d ' + fname)
        fname = fname[:-3]
        was_gzipped = True
        
    alignments = parse_maf.fname2alignments(fname)
    outlines = []
    for a in alignments:
        s = a.get_seq('hg18')
        if verbose:
            print s.sa
            print s.sc

        is_conserved = [int(x)>=3 for x in a.get_counts_aligned('hg18')]
        if verbose:
            print ''.join([str(int(x)) for x in is_conserved])

        csum = []
        kmer = 6
        for i in range(kmer,len(is_conserved)):
            csum.append(sum(is_conserved[(i-kmer):i]))
        if verbose:
            print ' ' * (kmer-1) +  ''.join([str(x) for x in csum])

        csum_cutoff = [0 for x in range(len(is_conserved))]
        for i,c in enumerate(csum):
            if c>=kmer:
                #print i
                csum_cutoff[(i):(i+kmer)] = [1 for junk in range(kmer)]

        cons_mask = ''.join([str(x) for x in csum_cutoff])
        if verbose:
            print cons_mask
            
        if sum(csum_cutoff) == 0:
            #asdfasdfasdf = 0
            continue
        line = '\t'.join([s.chr, str(s.start), str(s.end), s.strand, s.sa, s.sc, cons_mask])
        if verbose:
            print line
        outlines.append(line+'\n')

        
    if was_gzipped:
        commands.getoutput('gzip ' + fname)
        
    return outlines




if __name__=='__main__':
    main()
