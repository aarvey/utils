#!/usr/bin/python

import commands
import os

fnames = commands.getoutput('ls chr*.maf.gz').split()
print fnames
for fname in fnames:
    fname = fname.replace('.gz', '')
    print fname

    waszip = False
    if os.path.exists(fname + '.gz'):
        waszip = True
        print commands.getoutput('gzip -d ' + fname + '.gz')

    print commands.getoutput('./parse_maf.py ' + fname + ' > ' + fname + '.5vert')

    if waszip:
        print commands.getoutput('gzip ' + fname)
