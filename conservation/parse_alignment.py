#!/usr/bin/python

# File to parse the multiple sequence alignment file and 
# get alignments only for desired organisms
# The nucelotide in the reference will be picked if all except
# one of the align organisms have the same nucleotide

# Manu N Setty
# 11/11/2009

# Revised         Comments
# 11/16/2009      Added comments and fixed bug when no of organisms=2
# 04/07/2010      Bug fixes: Look for donservation in n-1 organisms,
#                            Lower or upper case in sequence does not matter
# 04/09/2010      Handling last block properly

import sys
import commands
import string
import re
import math

def usage():
    print "./parse_alignment.py MAF_FILE REF_ORG ALIGN_ORG1 ALIGN_ORG2...."
    print "MAF_FILE   : Aligned file downloaded from UCSC genome browser"
    print "REF_ORG    : String indicating the reference genome"
    print "ALIGN_ORGi : Other organisms to be aligned against"
    sys.exit ()


class Seq:
    """
    Seq Class
    """
    def __init__ (self, seqs):
        self.n = len (seqs)
        self.seqs = seqs
        self.aligned_seq = ""
        
        for pos in range(len(self.seqs[ref])):
            nucleotide = self.seqs[ref][pos]
            if nucleotide == "-":
                continue

            same_count = 1
            for org in seqs.keys():
                if nucleotide == self.seqs[org][pos]:
                    same_count = same_count + 1
            
            if (len(organisms)==2):
                if same_count==2:
                    self.aligned_seq = self.aligned_seq + nucleotide
                else:
                    self.aligned_seq = self.aligned_seq + "-"
            else:
                if same_count>=len (organisms)-1:
                    self.aligned_seq = self.aligned_seq + nucleotide
                else:
                    self.aligned_seq = self.aligned_seq + "-"


def main():
    global ref
    global organisms

    try:
        fname = sys.argv[1]
        ref = sys.argv[2]
        organisms = [ref]
        for i in range (3, len(sys.argv)):
            organisms.append (sys.argv[i])
    except:
        usage ()

    total_organisms = len (organisms)
    
    f = open (fname)
    lines = f.readlines ()
    f.close ()

    lines = map (string.strip, lines)
    p = re.compile ('[ ]+')
    lines = [p.sub (' ', line)  for line in lines]
    outfname = fname.split(".")[0] + ".fa"
    outf = open (outfname, "w")
    outf.writelines (">" + outfname.split(".")[0] + "\n")

    conserved_sequence = ''
    seqs = {}
    
    for line in lines:
        tokens = line.split (" ");

        # "a" indicates beginning of new alignment block
        if tokens[0]=="a":
            if len (seqs) >= total_organisms - 1:
                conserved_sequence = conserved_sequence + "-" * (start - len (conserved_sequence))
                conserved_sequence = conserved_sequence +  Seq (seqs).aligned_seq
            seqs = {}

        if tokens[0]!="s":
            continue
        
        # Keep loading the bases of different organisms
        if tokens[1].split(".")[0] in organisms:
            seqs[tokens[1].split(".")[0]] = tokens[6].upper ()
        
        # Mark the start and stop if the reference organisms is found
        if tokens[1].split(".")[0]==ref:
            start = int (tokens[2])
            end = int (tokens[5])

    # Last block
    if len (seqs) >= total_organisms - 1:
       conserved_sequence = conserved_sequence + "-" * (start - len (conserved_sequence))
       conserved_sequence = conserved_sequence +  Seq (seqs).aligned_seq
    if len(conserved_sequence) != end+1:
        conserved_sequence = conserved_sequence + "-" * (end - len (conserved_sequence))
    
    # Write 50 bases per line
    line_length = 50
    counts= int (math.ceil (float (len (conserved_sequence))/line_length))
    outlines = [conserved_sequence[(i-1)*line_length:i*line_length] + "\n" for i in range (1, counts+1)]
    outf.writelines (outlines)
    outf.close()
        

if __name__=="__main__":
   main()
