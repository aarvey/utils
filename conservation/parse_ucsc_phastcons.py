#!/usr/bin/python

import sys
import bisect

class Index:
    
    def __init__(self, line_bytes, idx_fname, phast_fname, verbose=False):

        # variables
        self.lbytes = 6
        self.lbytes = 3
        self.lbytes = line_bytes
        self.verbose = verbose
        self.empty = False

        if self.verbose: print 'Opening idx: "%s"' % (phast_fname,)
        f = open(idx_fname , 'r')
        lines = f.readlines()
        f.close()

        if self.verbose: print 'Opening phast "%s"' % (phast_fname,)
        self.fh = open(phast_fname, 'rb')
        

        end_line = int(lines[-1].split(' ')[0].strip())
        lines = lines[:-1]
        self.lnum = [int(l.split(':')[0])-1   for l in lines]
        self.lnum.append(end_line)
        #print self.lnum

        self.len = [len(l.split(':')[1])   for l in lines]
        #print self.len

        self.chr_idx = [int(l.split('start=')[1].split(' ')[0])   for l in lines]
        #print self.chr_idx

        self.ints = []
        for i in range(len(self.chr_idx)):
            self.ints.append((self.chr_idx[i], self.chr_idx[i] + self.lnum[i+1] - self.lnum[i] - 1))
        #print self.ints

        # Compute cumulative offsets to add to seeks
        self.offsets = [0 for l in self.len]
        if len(self.len)==0:
            self.empty = True
            return
        
        self.offsets[0] = self.len[0]
        for i in range(1,len(self.len)):
            self.offsets[i] = self.offsets[i-1] + self.len[i]
        #print self.offsets



    def idx2offset(self, idx, line_offset=0):
        ret = self.lbytes*self.lnum[idx] + self.offsets[idx] + line_offset
        return ret

    def get_offset(self, chr_idx):
        if (self.chr_idx.count(chr_idx)>0):
            #chr_idx = chr_idx - 1
            junkjunkjunk = 0
        idx = bisect.bisect_left(self.chr_idx, chr_idx+1) - 1
        if idx < 0:
            print 'You want something that is before the first conserved region'
            return None, None
        #print 'idx', idx
        #print 'chr_idx', chr_idx
        #print 'self.chr_idx[idx]', self.chr_idx[idx]
        #print 'self.lnum[idx]', self.lnum[idx]
        #print 'self.lnum[idx+1]', self.lnum[idx+1]
        start = self.chr_idx[idx]
        end = start + (self.lnum[idx+1] - self.lnum[idx])
        if chr_idx > end:
            print 'You want something that is in between two conserved regions'
            return None, None
        #print 'self.offsets[0]', self.offsets[0]

        start_offset = chr_idx - self.chr_idx[idx]
        ret = self.lbytes*self.lnum[idx] + self.offsets[idx] + start_offset*self.lbytes - idx*self.lbytes
        #ret = self.idx2offset(idx, start_offset)
        #print 'ret', ret
        return (idx, ret)


    def get_phast(self, start_byte, end_byte):
        if self.verbose: print 'enter get_phast'
        self.fh.seek(start_byte)
        if start_byte>=end_byte:
            return []
        ret = self.fh.read(end_byte-start_byte-1)
        if self.verbose: print 'sb, eb', start_byte, end_byte
        if self.verbose: 
            ret2 = []
            ret3 = ret.split('\n')
            for i,x in enumerate(ret3[:]):
                print i, x, len(ret3)
                ret2.append(float(x))
        #print start_byte
        #print end_byte
        #print 'ret', ret
        #print 'ret2', ret2
        ret = [x  for x in ret.split('\n')[:]]
        ##print ret
        ##for i in range(len(ret)):
        ##    if ret[i]=='':
        ##        ret[i] = '0'
        try:
            ret = [float(x)  if x is not '' else 0  for x in ret[:]]
        except:
            print ret
            raise 'ERROR'
        if self.verbose: print 'exit get_phast'
        return ret

    def get_region(self, start, end):
        #if start == 91544157 or start == 39087203 or start == 73655023:
        #    self.verbose=True
        #end = end - 1
        if self.verbose: print '-------------------------------------------------------------------------------'
        if self.verbose: print 'Start:', start
        if self.verbose: print 'End:', end

        ret = [0   for junk in range(start, end)]

        if self.empty:
            return ret
            
        if start >= end:
            if self.verbose: print 'Start is greater than end!'
            return []

        
        a = max(bisect.bisect_left(self.chr_idx, start)-1, 0)
        b = min(bisect.bisect_left(self.chr_idx, end), len(self.chr_idx))
        if self.verbose: print 'a', a
        if self.verbose: print 'b', b
        if self.verbose: print self.chr_idx[a:b]
        if self.verbose: print self.ints[a:b]
        for s, e in self.ints[a:b]:
            if start >= e or s >= end: continue
            if self.verbose: print 'orig s, e', s, e
            s = max(s, start)
            e = min(e-1, end)
            if self.verbose: print 'real s, e', s, e
            
            start_idx, start_offset = self.get_offset(s)
            if self.verbose: print 'start_idx, start_offset', start_idx, start_offset
            #if start_idx!=None:
            #    start_byte = self.idx2offset(start_idx, start_offset) 
            end_idx, end_offset = self.get_offset(e)
            if self.verbose: print 'end_idx, end_offset', end_idx, end_offset
            #if end_idx!=None:
            #    end_byte = self.idx2offset(end_idx, end_offset-start_offset)


            start_byte = start_offset
            end_byte = end_offset
            x = (s-start)
            y = (e-start)
            if self.verbose: print 'x,y', x,y
            if self.verbose: print 'len(ret)', len(ret)
            if self.verbose: print 'ret', ret
            ret[x:y] = self.get_phast(start_byte, end_byte)
            if self.verbose: print ret
        
        if self.verbose: print '-------------------------------------------------------------------------------'
        #if start == 91544157 or start == 39087203 or start == 73655023:
        #    self.verbose=False
        return ret
        
        
def main():
    
    fname = sys.argv[1]
    
    idx = Index(4, fname+'.idx', fname, True)
    idx.get_region(14430000,14430000)
    idx.get_region(14430010,14430010)
    idx.get_region(14430020,14430010)
    idx.get_region(14430030,14430010)
    idx.get_region(14430001,14430002)
    idx.get_region(14430001,14430010)
    idx.get_region(14430002,14430010)
    idx.get_region(14430003,14430011)
    idx.get_region(14430004,14430011)
    idx.get_region(14430005,14430011)
    idx.get_region(14432517, 14432519)
    idx.get_region(14432518, 14432519)
    idx.get_region(14432518, 14432525)
    idx.get_region(14765563, 14765573)
    ##return
    idx.get_region(14432517, 14432528)
    idx.get_region(14432518, 14432528)
    idx.get_region(14432018, 14432528)
    idx.get_region(14443965, 14432528)
    idx.get_region(14443965, 14443965)
    idx.get_region(14443965, 14443966)
    idx.get_region(14430001, 14443970)
    #idx.get_region(14430002,14800010)
    #idx.get_region(14730002,14800010)
    #idx.get_region(14430000,99930010)

    sys.exit(0)

    f = open(fname)
    #lines = f.readlines(1000000)
    #lines = f.readlines(10000000)
    lines = f.readlines()
    f.close()


    class Region:
        def __init__(self, line):
            'fixedStep chrom=chr1 start=1017 step=1'
            s = line[:-1].split(' ')
            self.chr = s[1].split('=')[1]
            self.start = s[2].split('=')[1]
            self.step = s[3].split('=')[1]
            self.cons = []

        def addline(self, line):
            self.cons.append(float(line.strip()))

        def tostring(self):
            print self.chr + '\t' + self.start + '\t' + self.step + '\t' + \
                  ' '.join(['%0.3f' % x for x in self.cons])

    regions = []
    print_amount = 10
    for line in lines:
        if line[0] == 'f':
            if len(regions)>0: regions[-1].tostring()
            regions.append(Region(line))
            continue
        regions[-1].addline(line)

        if len(regions) % print_amount == 0 and do_print:
            print len(regions)
            do_print = False
        if len(regions) % print_amount == 1:
            do_print = True






if __name__=='__main__':
    main()
