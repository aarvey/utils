#!/usr/bin/python

import sys


def main():
    fname = sys.argv[1]
    f = open(fname)
    lines = f.readlines()
    f.close()

    for i in xrange(len(lines)):
        if i % 100000 == 0: print i
        if lines[i][0]=="f": continue
        fl = (float(lines[i])+10.0)/2.0

        if fl > 9.99:
            lines[i] = '999\n'
        elif fl < 0:
            lines[i] = '000\n'
        else:
            lines[i] = str(int(fl*100)) + '\n'

    f = open(fname, 'w')
    f.writelines(lines)
    f.close()
             
if __name__=="__main__":
    main()
