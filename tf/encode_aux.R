
enc.strip.tf.details <- function(tfs) {
  orig.tfs <- tfs

  ##bad.tfs <- c("Pax5n19", "Yy1c20Pcr1x", "Yy1sc281V0416101")
  ##for (btf in bad.tfs) {tfs[which(tfs==btf)] <- "BADTF"}
  tfs <- gsub("sc25388|116181ap|Pcr[0-9]x|Iggrab|Iggmus|Musigg|n494|ab9263|sc631|101388|cIggmus|Rabigg|sc13268|V0416101|c20|V0416102|Tnfa|n19|ab9263|sc281|n15|BADTF|V2|166181ap",
              "", tfs)
  ##tfs <- gsub("Control|Input|Tafii", "", tfs)
  tfs <- gsub("Setdb1Mnase", "Setdb1", tfs)
  tfs <- gsub("Bcl3Bcl3", "Bcl3", tfs)

  tfs[which(orig.tfs=="MusiggMusigg")] <- "Iggmus"
  tfs[which(orig.tfs=="Rabigg")] <- "Iggrab"
  tfs[which(orig.tfs=="InputMusigg")] <- "Iggmus"
  tfs[which(orig.tfs=="InputRabigg")] <- "Iggrab"
  return(tfs)
  ##tfs[which(orig.tfs)]
  ##InputIggrab
}





enc.align.to.name.new <- function(fname) {
  orig.fname <- fname
  fname <- strsplit(fname , "/")[[1]]
  fname <- fname[length(fname)]

  fname <- gsub("wgEncode", "", fname)
  fname <- strsplit(fname, "\\.")[[1]][1]


  cell.types <- c("Gm12878", "K562", "K562b", "Helas3", "Mcf7", "Raji", "Gm[0-9][0-9][0-9][0-9][0-9]")
  cell.hits <- sapply(cell.types, function(c){aa <- gregexpr(c,fname); if(length(aa) > 0) return(substr(fname, aa[[1]][1], aa[[1]][1] - 1 + attributes(aa[[1]])$match.length[1])); return("")})
  cell.type <- cell.hits[which.max(sapply(cell.hits, nchar))]

  aa <- strsplit(fname, cell.type)[[1]]
  lab <- aa[1]

  bb <- strsplit(aa[2], "Raw")[[1]]

  tf <- gsub("Std", "", bb[1])
  tf <- gsub("Pcr.x|V[0-9][0-9][0-9][0-9]*|sc[0-9][0-9][0-9]*|Iggrab|Iggmus|Musigg|Rabigg", "", tf)

  rep <- gsub("DataRep|Data|V.", "", bb[2])
  if (is.na(rep) || nchar(rep)==0) rep <- "1"

  assay <- "assay"
  if (length(grep("BroadHistone|BroadChip|UwHistone", fname)) > 0)
    assay <- "ChIP-Seq Histone"
  if (length(grep("^head", fname)) > 0) 
    assay <- "MNase-Seq"
  if (length(grep("OpenChromFaire|UncFAIRE", fname)) > 0) 
    assay <- "FAIRE-Seq"
  if (length(grep("OpenChromDnase|DukeDNase", fname)) > 0) 
    assay <- "DNase-Seq (No Gel)"
  if (length(grep("UwDnase", fname)) > 0) 
    assay <- "DNase-Seq (Gel Select)"
  if (length(grep("UwDgf", fname)) > 0) 
    assay <- "DGF DNase-Seq (Gel Select)"
  if (length(grep("RipSeq", fname)) > 0) 
    assay <- "RIP-Seq"
  if (length(grep("RepliSeq", fname)) > 0) 
    assay <- "RepliSeq"
  if (length(grep("RnaSeq", fname)) > 0) 
    assay <- "RNA-Seq"
  if (length(grep("Cage", fname)) > 0) 
    assay <- "CAGE-Seq"
  if (length(grep("Tfbs|HudsonalphaChip|YaleChIP|Ctcf|UtaChIP", fname)) > 0) 
    assay <- "ChIP-Seq TF"
  if (length(grep("vitamind|e2f4", orig.fname)) > 0) 
    assay <- "ChIP-Seq TF"
  if (length(grep("MethylSeq", fname)) > 0) 
    assay <- "Methyl Seq (MspI vs HpaII)"
  if (length(grep("MethylRrbs", fname)) > 0) 
    assay <- "Methyl Seq (RRBS)"


  if (length(grep("DNase", assay)) > 0) tf <- "DNase"
  if (length(grep("FAIRE", assay)) > 0) tf <- "FAIRE"
  
  name <- sprintf("%s_%s_%s_Rep%s", lab, cell.type, tf, rep)
  return(c(fname=fname, name=name, lab=lab, tf=tf, cell=cell.type, rep=rep, assay=assay))





  
  
  if (length(grep("HaibTfbs|SydhTfbs", fname)) > 0) 
    cell <- strsplit(fname, "Tfbs")[[1]][2]
  else  if (length(grep("YaleChIPseqRawDataK562.*Input", fname)) > 0) 
    cell <- strsplit(fname, "RawData")[[1]][2]
  else  if (length(grep("BroadHistone", fname)) > 0) 
    cell <- strsplit(fname, "BroadHistone")[[1]][2]
  else  if (length(grep("Cshl.*RnaSeq", fname)) > 0) 
    cell <- strsplit(fname, "RnaSeq")[[1]][2]
  else  if (length(grep("OpenDnase", fname)) > 0) 
    cell <- strsplit(fname, "Dnase")[[1]][2]
  else  if (length(grep("OpenChromDnase", fname)) > 0) 
    cell <- strsplit(fname, "Dnase")[[1]][2]
  else  if (length(grep("Faire", fname)) > 0) 
    cell <- strsplit(fname, "Faire")[[1]][2]
  else  if (length(grep("RipSeq", fname)) > 0) 
    cell <- strsplit(fname, "RipSeq")[[1]][2]
  else  if (length(grep("Cage", fname)) > 0) 
    cell <- strsplit(fname, "Cage")[[1]][2]
  else  if (length(grep("UtaChIPseqRawData[^R]", fname)) > 0) 
    cell <- strsplit(fname, "ChIPseq")[[1]][2]
  else  if (length(grep("Dgf", fname)) > 0) 
    cell <- strsplit(fname, "Dgf")[[1]][2]
  else  if (length(grep("YaleRnaSeqPolya", fname)) > 0) 
    cell <- strsplit(fname, "YaleRnaSeqPolya")[[1]][2]
  else  if (length(grep("RikenCageRawData", fname)) > 0) 
    cell <- strsplit(fname, "RawData")[[1]][2]
  else  if (length(grep("UwHistone", fname)) > 0) 
    cell <- strsplit(fname, "Histone")[[1]][2]
  else
    cell <- strsplit(fname, "Rep.")[[1]][2]

  
  next.cap.idx <- gregexpr("[A-Z\\.]", substr(cell, 2, nchar(cell)), ignore.case=F, perl=T)[[1]][1]
  cell <- substr(cell, 1, next.cap.idx)
  if (cell=="K562b") cell<-"K562"

  
  tf <- strsplit(fname, cell)[[1]][2]
  tf <- gsub("Rep.", "", strsplit(tf, "\\.")[[1]][1])
  tf <- gsub("^b", "", tf)
  tf <- strsplit(tf, "AlnRep|RawDataRep|StdRawData")[[1]][1]
  tf <- gsub("RawData", "", tf)
  if (length(grep("dnase", fname,ignore.case=T))>0)     tf <- "DNase"
  if (length(grep("faire", fname,ignore.case=T))>0)     tf <- "FAIRE"
  
  rep <- sprintf("Rep%s", substr(strsplit(fname, "Rep")[[1]][2], 1,1))

  
  
  lab <- ""
  if (length(grep("Broad", fname)) > 0) { lab <- "BROAD" }
  if (length(grep("Hudson|Haib", fname)) > 0) { lab <- "HA" }
  if (length(grep("Yale|Sydh", fname)) > 0) { lab <- "SYDH" }
  if (length(grep("Uw", fname)) > 0) { lab <- "UW" }
  if (length(grep("Cshl", fname)) > 0) { lab <- "CSHL" }
  if (length(grep("Riken", fname)) > 0) { lab <- "RIKEN" }
  if (length(grep("Unc", fname)) > 0) { lab <- "UNC" }
  if (length(grep("Duke|OpenChrom", fname)) > 0) { lab <- "DUKE" }
  if (length(grep("Suny", fname)) > 0) { lab <- "SUNY" }
  if (length(grep("Uta", fname)) > 0) { lab <- "UTA" }
  if (any(sapply(c(lab, cell, tf, rep), is.na))) {
    warning(sprintf("There are NA components of name! lab:%s, cell:%s, tf:%s, rep:%s", lab, cell, tf, rep))
    name <- tf <- cell <- rep <- fname
  } else {
    name <- sprintf("%s_%s_%s_%s", lab, cell, tf, rep)
    tf <- sprintf("%s", tf)
    cell <- sprintf("%s", cell)
    rep <- sprintf("%s", rep)
  }


  assay <- NA
  if (length(grep("BroadHistone|BroadChip|UwChIPSeq", fname)) > 0)
    assay <- "ChIP-Seq Histone"
  if (length(grep("^head", fname)) > 0) 
    assay <- "MNase-Seq"
  if (length(grep("OpenChromFaire|UncFAIRE", fname)) > 0) 
    assay <- "FAIRE-Seq"
  if (length(grep("OpenChromDnase|DukeDNase", fname)) > 0) 
    assay <- "DNase-Seq (No Gel)"
  if (length(grep("UwDnase", fname)) > 0) 
    assay <- "DNase-Seq (Gel Select)"
  if (length(grep("RipSeq", fname)) > 0) 
    assay <- "RIP-Seq"
  if (length(grep("RnaSeq", fname)) > 0) 
    assay <- "RNA-Seq"
  if (length(grep("Cage", fname)) > 0) 
    assay <- "CAGE-Seq"
  if (length(grep("Tfbs|HudsonalphaChip|YaleChIP|Ctcf|UtaChIP", fname)) > 0) 
    assay <- "ChIP-Seq TF"
  if (length(grep("vitamind|e2f4", orig.fname)) > 0) 
    assay <- "ChIP-Seq TF"
  if (length(grep("MethylSeq", fname)) > 0) 
    assay <- "Methyl Seq (MspI vs HpaII)"
  if (length(grep("MethylRrbs", fname)) > 0) 
    assay <- "Methyl Seq (RRBS)"
  
  
  return(c(fname=fname, name=name, lab=lab, tf=tf, cell=cell, rep=rep, assay=assay))
}


enc.align.to.name.old <- function(fname, cell="Gm12878") {
  fname <- strsplit(fname , "/")[[1]]
  fname <- fname[length(fname)]
  cell <- strsplit(fname, "Rep.")[[1]][2]
  next.cap.idx <- gregexpr("[A-Z\\.]", substr(cell, 2, nchar(cell)), ignore.case=F, perl=T)[[1]][1]
  cell <- substr(cell, 1, next.cap.idx)
  rep <- strsplit(fname, cell)[[1]][1]
  rep <- strsplit(rep, "Data")[[1]][2]
  tf <- strsplit(fname, cell)[[1]][2]
  tf <- gsub("IggrabIggrab|Pcr[0-9].*|Musigg", "", strsplit(tf, "\\.")[[1]][1])
  tf <- gsub("^b", "", tf)
  lab <- ""
  if (length(grep("broad", fname, ignore.case=T)) > 0) { lab <- "BI" }
  if (length(grep("hudson|haib", fname, ignore.case=T)) > 0) { lab <- "HA" }
  if (length(grep("yale|sydh", fname, ignore.case=T)) > 0) { lab <- "SN" }
  if (length(grep("uw|tfbs", fname, ignore.case=T)) > 0) { lab <- "UW" }
  if (any(sapply(c(lab, cell, tf, rep), is.na))) {
    name <- tf <- cell <- rep <- fname
  } else {
    name <- sprintf("%s_%s_%s_%s", lab, cell, tf, rep)
    tf <- sprintf("%s", tf)
    cell <- sprintf("%s", cell)
    rep <- sprintf("%s", rep)
  }  
  return(list(name=name, tf=tf, cell=cell, rep=rep))
}


enc.align.to.name <- enc.align.to.name.new
