###############################################################################################
##                                                                                           ##
## This is an example script showing how to use apply_pssm and read in pssm matrices         ##
##                                                                                           ##
###############################################################################################

source("~/utils/tf/apply_pssm.R")

stop("finished")

library(snow)
size.clust <- 10
cluster <- makeCluster(size.clust,type="SOCK")
#cluster <- NULL

  
pwm2scores <- function(seqs, pwm) {
  pwm.scores <- apply.pwm(pwm, seqs, cluster=cluster, group.size=25)
  return(pwm.scores)
}

##
## Read in jaspar pssm matrices
##
lines <- readLines("matrix_only.txt")
pssms <- list()
for (i in 1:(length(lines)/5)) {
  idx <- (i-1)*5+1
  pssms[[i]] <- list()
  pssms[[i]]$name <- gsub(" ", "-", substr(lines[idx], 2, nchar(lines[idx])))
  pssms[[i]]$name <- gsub("/", "-", pssms[[i]]$name)
  pssms[[i]]$name <- gsub(":", "-", pssms[[i]]$name)
  pssm <- read.pssm(lines=lines[(idx+1):(idx+4)], bracket=T, colon=F)
  pssm <- norm.pssm(pssm, epsilon=0, pseudocount=2)
  pssms[[i]]$pssm <- pssm
  pssms[[i]]$pwm <-pssm2pwm(pssm, bg.type="human")
  show(pssms[[i]]$pwm)
}


##
## Read in transfac matrices
##




seqs <- readLines("SOLID_0041_FoxP3_F3.unique.csfasta.ma.50.4.tagAlign.output_peak_coords.txt.fasta")
seqs <- seqs[-grep('>', seqs)]
seqs <- seqs[4000:4500]

fname.suffix <- "mid"
for (i in 1:length(pssms)) {
  show(i)
  show(pssms[[i]])
  fname <- sprintf("%s-Foxp3-%s", pssms[[i]]$name, fname.suffix)
  if (file.exists(fname)) {
    next;
  }
  start.time <- as.numeric(format(Sys.time(), "%s"))
  scores <- pwm2scores(seqs, pssms[[i]]$pwm)
  scores <- sapply(scores, function(x){paste(sprintf("%0.3f", x), collapse=",")})
  writeLines(scores, fname)
  end.time <- as.numeric(format(Sys.time(), "%s"))
  show(sprintf("Time is %d", end.time-start.time))
}






load("tvals.RData")
for (i in length(tvals):length(pssms)) {
  fname <- sprintf("%s-Foxp3-%s", pssms[[i]]$name, fname.suffix)
  d <- read.csv(fname, header=F)
  d <- colSums(d)
  mid <- round(length(d)/2)
  inside <- d[(mid-100):(mid+100)]
  outside <- d[c(1:(mid-100), (mid+100):length(d))]
  ht <- t.test(inside, outside)
  show(fname)
  show(as.numeric(ht$statistic))
  tvals[i] <- as.numeric(ht$statistic)
}
save(tvals, file="tvals-mid.RData")


names <- sapply(pssms, function(x){x$name})
tvals <- c(tvals, rep(0, length(names)-length(tvals)))
names(tvals)
show(cbind(names(sort(tvals)), as.numeric(sort(tvals))))
idx.up <- which(tvals > 20)
idx.down <- which(tvals < -20)
idx.middle <- which(abs(tvals) > 0.1 & abs(tvals) < 0.5)
idx.middle <- idx.middle[1:5]

c(idx.middle, idx.up, idx.down)
library(seqLogo)
for (i in c(idx.middle, idx.down)) {
  fname <- sprintf("%s-Foxp3-%s", pssms[[i]]$name, fname.suffix)
  show(fname)
  pwm <- makePWM(t(pssms[[i]]$pssm))
  pdf(sprintf("%s-logo.pdf", fname))
  seqLogo(pwm)
  dev.off()
  #show(pssm2consensus(pssms[[i]]$pssm))
  d <- read.csv(fname, header=F)
  d <- colSums(d)
  pdf(sprintf("%s.pdf", fname))
  plot(1:length(d), d, type="n")
  lines(1:length(d), d)
  dev.off()
}


