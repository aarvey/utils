#!/usr/bin/python

import sys, time, gzip

class Platform:
    def __init__(self):
        self.header = {}
        self.probes2info = {}
        self.probes2tab = {}
        self.name2probes = {}
        self.probes = []
        self.name = None
        self.colnames = None

    def setname(self, name):
        self.name = name

    def addheader(self, name, val):
        self.header[name] = val

    def addcolnames(self, colnames):
        self.colnames = colnames

    def colname2idx(self, colname):
        ret = None
        for i,c in enumerate(colnames):
            if c==colname:
                ret = i
        return ret

    def getprobes(self):
        return self.probes

    def addprobe(self, id, gbname, line):
        self.probes.append(id)
        self.probes2info[id] = gbname
        self.probes2tab[id] = line
        if not self.name2probes.has_key(gbname):
            self.name2probes[gbname] = []
        self.name2probes[gbname].append(id)

    def probeid2name(self, id):
        if not self.probes2info.has_key(id):
            return None
        return self.probes2info[id]

    
    def probeid2tab(self, id):
        if not self.probes2tab.has_key(id):
            return None
        return self.probes2tab[id]

    

class Sample:
    def __init__(self, name):
        self.header = {}
        self.platform = None
        self.vals = []
        self.id2validx = {}
        self.name2validx = {}
        self.name = name
        self.description = None
        self.fields = []


    def setfieldnames(self, line):
        self.fields = [x.strip() for x in line.split('\t')]
        self.fields = self.fields[1:]

    def numfields(self):
        return len(self.fields)

    def getfield(self, i):
        if i >= len(self.fields):
            return None
        return self.fields[i]

    def getfields(self):
        return self.fields
        
    def setplatform(self, platform):
        self.platform = platform

    def setdescription(self, description):
        self.description = description

    def setname(self, name):
        self.name = name

    def getname(self):
        return self.name

    def addheader(self, name, val):
        self.header[name] = val

    def addval(self, id, val):
        idx = len(self.vals)
        self.vals.append(val)
        
        if not self.id2validx.has_key(id):
            self.id2validx[id] = []
        self.id2validx[id].append(idx)

        name = self.platform.probeid2name(id)
        if not self.name2validx.has_key(name):
            self.name2validx[name] = []
        self.name2validx[name].append(idx)

    def probeid2val(self, id):
        if not self.id2validx.has_key(id):
            return None
        idx = self.id2validx[id]
        if len(idx) > 1:
            print idx
        return self.vals[idx[0]]

    def probeid2vals(self, id):
        if not self.id2validx.has_key(id):
            return None
        idx = self.id2validx[id]
        return self.vals[idx]

    def gene2val(self, gname):
        return None

class ProbeValue:
    pass

class Feature:
    def __init__(self):
        self.feats = []
        
    def add(self, val, i):
        x = {}
        x['val'] = val
        x['linenum'] = i
        self.feats.append(x)

    def tostring(self):
        s =  []
        for f in self.feats:
            s.append(str(f['linenum']) + '->' + f['val'])
        return '\n'.join(s)
    
def get_feat(line):
    #print line
    feature = line[1:].split('=')[0].strip()
    #print feature
    feature_val = line[1:].split('=')[1].strip()
    #print feature_val
    return (feature, feature_val)
        
class SoftData:
    def __init__(self):
        self.platforms = []
        self.name2platform = {}
        self.samples = []
        self.features = {}


    def readfile(self, fname, name_idx, name2_idx):

        self.fname = fname

        if fname[-3:] == '.gz':
            f = gzip.open(fname)
        else:
            f = open(fname)
        lines = f.readlines()
        f.close()

        in_sample = False
        in_platform = False
        in_sample_table = False
        in_platform_table = False
        just_in_sample_table = False
        just_in_platform_table = False
        start_time_in_sample = 0
        
        for i,line in enumerate(lines):
            line = line.strip()

            if just_in_sample_table:
                just_in_sample_table = False
                self.samples[-1].setfieldnames(line)
                continue
            
            if just_in_platform_table:
                just_in_platform_table = False
                self.platforms[-1].addcolnames(line.strip().split('\t'))
                continue


            if line == '!sample_table_end':
                in_sample = False
                in_sample_table = False
                end_time_in_sample = time.time()
                print 'Time in sample', '%0.2f' % (end_time_in_sample - start_time_in_sample, )
                #if len(self.samples) > 2:
                #    break

            if line == '!platform_table_end':
                in_platform = False
                in_platform_table = False

            if in_platform and line=='!platform_table_begin':
                in_platform_table = True
                just_in_platform_table = True
                continue
           
            if in_sample and line=='!sample_table_begin':
                in_sample_table = True
                just_in_sample_table = True
                start_time_in_sample = time.time()
                continue

            if line[0] == "^":
                feature, feature_val = get_feat(line)
                
                if self.features.has_key(feature):
                    self.features[feature].add(feature_val, i)
                else:
                    self.features[feature] = Feature()
                    self.features[feature].add(feature_val, i)
                    
                if feature == "SAMPLE":
                    in_sample = True
                    self.samples.append(Sample(feature_val))

                if feature == "PLATFORM":
                    in_platform = True
                    self.platforms.append(Platform())
                    self.platforms[-1].setname(feature_val)
                    self.name2platform[feature_val] = self.platforms[-1]
                #print feature, self.features[feature].tostring()
                print feature_val

            if line[:19] == '!Sample_platform_id':
                platform_name = line.split('=')[1].strip()
                self.samples[-1].setplatform(self.name2platform[platform_name])

            
            if line[:13] == '!Sample_title':
                descrip = line.split('=')[1].strip()
                self.samples[-1].setdescription(descrip)

            if in_platform and line[0]=='!':
                featname, featval = get_feat(line)
                self.platforms[-1].addheader(featname, featval)
                
            if in_platform_table:
                s = line.split('\t')
                #print s
                #while len(s) < 5:
                #    s.append('')
                ##print s
                if len(s) <= name_idx:
                    gene_name = 'Unkown'
                else:
                    gene_name = s[name_idx]
                    #print s[:13]
                #print s[name_idx]
                #sys.exit(0)
                if name2_idx != None:
                    s2 = gene_name.replace(' ', '').split('//')
                    if len(s2) <= name2_idx:
                        gene_name = 'Unkown'
                    else:
                        gene_name = s2[name2_idx]
                self.platforms[-1].addprobe(s[0], gene_name, line)

            if in_sample and line[0]=='!':
                featname, featval = get_feat(line)
                self.samples[-1].addheader(featname, featval)
                
            if in_sample_table:
                s = [x.strip() for x in line.split('\t')]
                self.samples[-1].addval(s[0], s[1:])


    def writefile(self, fname, sep='\t'):
        f = open(fname+'.desc.txt', 'w')
        lines = [s.platform.name + sep + s.name + sep + s.description + '\n'   for s in self.samples]
        f.writelines(lines)
        f.close()
        
        for platform in self.platforms:
            f = open(fname+'.'+platform.name+'.txt', 'w')
            lines = []
            
            line = 'Platform' + sep + 'ProbeId' + sep + 'Name'
            for sample in self.samples:
                if sample.platform.name != platform.name:
                    continue
                for field in sample.getfields():
                    line = line + sep + sample.getname() + field
            line = line + '\n'
            lines.append(line)
            print line
            
            print self.platforms
            print self.name2platform
            plat_lines = []
            for probe in platform.getprobes():
                line = []
                line.append(platform.name)
                line.append(probe)
                line.append(self.name2platform[platform.name].probeid2name(probe))
                plat_lines.append(self.name2platform[platform.name].probeid2tab(probe).replace('\t\t', '\tNA\t'))
                for sample in self.samples:
                    if sample.platform.name != platform.name:
                        continue
                    vals = sample.probeid2val(probe)
                    if vals==None:
                        vals = ['' for i in range(sample.numfields())]
                    for v in vals:
                        line.append(v)
                #print line
                line = sep.join(line) + '\n'
                lines.append(line)
                #print line
            
            f.writelines(lines)
            f.close()
            
            f = open(fname+'.'+platform.name+'.platform.txt', 'w')
            f.writelines([pl+'\n' for pl in plat_lines])
            f.close()
            

def usage():
    print ''
    print 'Usage:'
    print '\t python parse_soft.py filename.soft'
    print '\t Where the filename.soft was downloaded from GEO in SOFT format'
    print ''
    print 'Common edits:'
    print ''
    print '\t Change the column considered to be the gene name in the platform specification'
    print '\t\t In readfile(), there is a line that looks like'
    print '\t\t\t  self.platforms[-1].addprobe(s[0], s[9])'
    print '\t\t Here s[0] refers to the ID of the probe and s[9] refers to the name of the probe'
    print '\t\t Change the 9 to an appropriate value.'
    print ''
    print '\t Change what is output into the description file *.desc.txt'
    print '\t\t In readfile(), there is a line that looks like'
    print '\t\t\t if line==Sample_title ...'
    print '\t\t Change Sample_title to whatever part of the sample header '
    print '\t\t contains the appropriate information'
    print ''

import traceback
def main():
    try:
        fname = sys.argv[1]
        name_idx = int(sys.argv[2])
        name2_idx = None
        if len(sys.argv) > 3:      name2_idx = int(sys.argv[3])
    except:
        ##traceback.print_tb()
        traceback.print_exc()
        usage()
        sys.exit(1)
    d = SoftData()
    d.readfile(fname, name_idx, name2_idx)
    d.writefile(fname)
    
    
    

if __name__=='__main__':
    main()
