##grep -v "gene_name \"SCAR\|gene_name \"SNOR\|gene_name \"MIR\|gene_name \"RP[LS]\|gene_name \"[A-Za-z0-9]*-AS\|gene_name \"RN7\|gene_name \"RNA5\|gene_name \"RNU\|gene_name \"RP[0-9]\|gene_name \"[A-Za-z0-9]*-\|gene_name \"[A-Za-z0-9]*\-IT\|gene_name \"CTA\-[A-D]\|gene_name \"KB\-[0-9]*\|gene_name \"RP[0-9]*\|gene_name \"AC[0-9][0-9][0-9][0-9]*\|gene_name \"AL[0-9][0-9][0-9][0-9]*\|gene_name \"AP[0-9][0-9][0-9][0-9]*"  gencode.v19.annotation.gtf > gencode.v19.mine.gtf


## HUMAN COMMAND
##grep -v "gene_name \"SCAR\|gene_name \"SNOR\|gene_name \"MIR\|gene_name \"RP[LS]\|gene_name \"[A-Za-z0-9]*-AS\|gene_name \"RN7\|gene_name \"RNA5\|gene_name \"RNU\|gene_name \"RP[0-9]\|gene_name \"[A-Za-z0-9]*\-IT\|gene_name \"CTA\-[A-D]\|gene_name \"KB\-[0-9]*\|gene_name \"RP[0-9]*\|gene_name \"AC[0-9][0-9][0-9][0-9]*\|gene_name \"AL[0-9][0-9][0-9][0-9]*\|gene_name \"AP[0-9][0-9][0-9][0-9]*"  gencode.v19.annotation.gtf > gencode.v19.mine.gtf


## MOUSE COMMAND
##grep -v -i  "gene_name \"SCAR\|gene_name \"SNOR\|gene_name \"MIR\|gene_name \"RP[LS]\|gene_name \"[A-Za-z0-9]*-AS\|gene_name \"RN7\|gene_name \"RNA5\|gene_name \"RNU\|gene_name \"RP[0-9]\|gene_name \"[A-Za-z0-9]*\-IT\|gene_name \"CTA\-[A-D]\|gene_name \"KB\-[0-9]*\|gene_name \"RP[0-9]*\|gene_name \"AC[0-9][0-9][0-9][0-9]*\|gene_name \"AL[0-9][0-9][0-9][0-9]*\|gene_name \"AP[0-9][0-9][0-9][0-9]*"  gencode.vM2.annotation.gtf > gencode.vM2.mine.gtf


grep -v -i  "gene_name \"SCAR\|gene_name \"SNOR\|gene_name \"MIR\|gene_name \"RP[LS]\|gene_name \"[A-Za-z0-9]*-AS\|gene_name \"RN7\|gene_name \"RNA5\|gene_name \"RNU\|gene_name \"RP[0-9]\|gene_name \"[A-Za-z0-9]*\-IT\|gene_name \"CTA\-[A-D]\|gene_name \"KB\-[0-9]*\|gene_name \"RP[0-9]*\|gene_name \"AC[0-9][0-9][0-9][0-9]*\|gene_name \"AL[0-9][0-9][0-9][0-9]*\|gene_name \"AP[0-9][0-9][0-9][0-9]*"  gencode.vM1.annotation.gtf > gencode.vM1.mine.gtf


##   nohup nice -n 15 featureCounts -g gene_name -T 20 -a ~/ll-utils/rna/gencode.v19.mine.gtf -o counts_names.txt `ls s_D*merge.bam` &

## GET SJDB
##  nohup grep 'exon' gencode.vM2.mine.gtf  | cut -f1,4,5,7 > gencode.vM2.mine.sjdb.txt  &
##  nohup nice -n 15 featureCounts -g gene_name -T 20 -a ~/ll-utils/rna/gencode.vM2.mine.gtf -o counts_names.txt `ls .bam` &
##  featureCounts -g gene_name -O -M  -T 20 -a ~/ll-utils/rna/gencode.vM2.mine.gtf -o counts_names_genes.txt `ls *bam` &



## Gm15818 chr1;chr1       9972468;9981845 9972520;9982447 +;+     656     0       0       0       0       0       0       0       0       0       0       0       0       0
## counts_names_genes_vM1_pair_sbn_multi.txt

featureCounts -g gene_name  -p  -T   20   -a   /home/aarvey/ll-utils/rna/gencode.vM1.mine.gtf -o counts_names_genes_vM1_pair_sbn.txt `ls *sort_by_name`

x <- my.read.table("counts_names_genes_vM1_pair_sbn.txt")