
import pysam
class alnfile:
   def __init__(self, fname, long_x0=True, keep_orig=False):
      self.fname = fname
      self.samfile = pysam.Samfile(fname, "rb" )
      self.chr2len = {}
      lens = zip(self.samfile.references, self.samfile.lengths)
      for chr, len in lens:    self.chr2len[chr] = len
      self.tid2chr = [self.samfile.getrname(tid)  for tid in range(self.samfile.nreferences)]
      self.long_x0 = long_x0
      self.keep_orig = keep_orig
      

   def __iter__(self):
       return self
   
   def next(self):
      return self.bam2record(self.samfile.next())
   
   def read_alns(self, keeplines=True):
      fname = self.fname
      lines = []
      records = []
      if (fname[-3:]=='bam'):
         i = 0
         for s in self.samfile.fetch():
            if keeplines:   lines.append(s)
            if (i+1) % 1000000 == 0:
               print i
            i += 1
            records.append(self.bam2record(s))
      elif (fname[-3:]=='star'):
         ref_idx = 26
         read_idx = 27
         qual_idx = 28
      else:
         f = open(fname)
         if keeplines:
            lines = f.readlines()
         else:
            records = [self.bwt2record(l.split('\t'))  for l in f]
         f.close()
      self.records = records
      self.lines = lines

   def close(self):
      self.samfile.close()



   class aln:
      def __init__(self):
         self.ref = ''
         self.pos = ''
         self.tags = ''
         self.naln = ''
         self.chr = ''
         self.strand = ''
         self.read = ''
         self.qual = ''
      def __str__(self):
         return ' '.join([self.ref, str(self.pos), self.chr, str(self.strand), self.read, str(self.naln), str(self.tags)])
   def bwt2record(self, line):
      r = self.aln()
      r.ref = line[0]
      r.strand = int(line[1]=='+')*2-1
      r.chr = line[2]
      r.pos = lint(ine[3])
      r.read = line[4]
      r.qual = line[5]
      r.naln = int(line[6])
      return(r)
   def bam2record(self, line):
      r = self.aln()
      r.ref = line.qname
      r.pos = line.pos
      ##print line.tags
      ##print dict(line.tags)
      ##r.naln = 1
      ##r.naln = line.tags[1][1]
      ##print r.naln
      if  self.long_x0:
         tags = dict(line.tags)
         r.naln = 0
         if tags.has_key('X0'):
            r.naln += tags['X0']
         elif tags.has_key('X1'):
            r.naln += tags['X1']
         elif tags.has_key('AS'):
            r.naln += tags['AS']
         else:
            raise 'you have no X0, X1, AS field in tags'
      else:
         if dict(line.tags).has_key('X0'):
            r.naln = dict(line.tags)['X0']

      r.chr = self.samfile.getrname(line.tid)
      #r.chr = self.tid2chr[line.tid]
      
      r.strand = (not line.is_reverse)*2-1
      r.read = line.query
      r.qual = line.qqual
      if self.keep_orig:
         r.orig = line
      return(r)
   
   def record2bam(self, r, update_read=False):
      if self.keep_orig:
         line = r.orig
         if update_read:
            line.seq = r.read
      else:
         line = pysam.AlignedRead()
         line.qname = r.ref
         line.pos = r.pos
         if  self.long_x0:
            tags = dict(line.tags)
            r.naln = 0
            if tags.has_key('X0'):
               r.naln += tags['X0']
            elif tags.has_key('X1'):
               r.naln += tags['X1']
            elif tags.has_key('AS'):
               r.naln += tags['AS']
            else:
               raise 'you have no X0, X1, AS field in tags'
         else:
            if dict(line.tags).has_key('X0'):
               r.naln = dict(line.tags)['X0']

         r.chr = self.samfile.getrname(line.tid)
         r.strand = (not line.is_reverse)*2-1
         r.read = line.query
         r.qual = line.qqual
      return(line)
      
