import sys, io, string, os, bisect

def main():
    label_idx = 5
    fields = sys.argv[1]
    if fields.find(',') < 0 and os.path.isfile(fields):
        ff = open(fields); fields = [int(f.strip())-1 for f in ff];  ff.close()
        ##ff = open(sys.argv[1] + '.train'); train_idx = [int(x)  for x in ff]; ff.close()
        ##ff = open(sys.argv[1] + '.test'); test_idx = [int(x)  for x in ff]; ff.close()
    else:
        print 'no field file!'
        raise NameError('no field file!')
        ##fields = [int(f)-1 for f in sys.argv[1].split(',')]
    
    f = open(sys.argv[2], 'r', buffering=2**30)
    trainf = open(sys.argv[3] + '.train', 'w', buffering=2**26)
    testf = open(sys.argv[3] + '.test', 'w', buffering=2**26)
    realtestf = open(sys.argv[3] + '.real.test', 'w', buffering=2**20)
    trainmetaf = open(sys.argv[3] + '.train.meta', 'w', buffering=2**20)
    testmetaf = open(sys.argv[3] + '.test.meta', 'w', buffering=2**20)
    realtestmetaf = open(sys.argv[3] + '.real.test.meta', 'w', buffering=2**20)

    #lines = f.readlines()
    flist = [trainf,testf,realtestf]
    metaflist = [trainmetaf,testmetaf,realtestmetaf]
    for i,line in enumerate(f):
        if i == 0: continue
        if i % 1000==0: print i
        line = line.strip().split(' ')

        fi = (i-1)%2
        if line[label_idx] == '-9':  fi = 2
        flist[fi].write(string.join([line[j] for j in fields], ','))
        flist[fi].write(','+line[label_idx]+';\n')
        metaflist[fi].write(string.join(line[:6], ' ')+'\n')
        ##if i >= 1000: break
    [tmp.close() for tmp in flist]
    [tmp.close() for tmp in metaflist]
    f.close()
    f = open(sys.argv[3] + '.cut.finished', 'w');    f.write("finished\n");    f.close();

if __name__=='__main__':
    main()






    ##has_multi_char_fields = fields.index(0) >= 0 or fields.index(1) >= 0
    ##has_multi_char_fields_0 = fields.index(0) >= 0
    ##has_multi_char_fields_1 = fields.index(1) >= 0
    #print 'Multi char: %d' % (has_multi_char_fields,)
    
    if False:
        if False:
            j = k = 0
            while True:
                k += 1
                if line[k]==' ': j += 1
                if j==2: break
            line = line.replace('NA', 'N')
            print k
            print fields
            print len(line)
            print 'fast?', string.join([line[k+l*2-2]  for l in fields], ' ')
            
        
        if i % 2 == 1:
            trainf.write(out)
            trainf.write(','+line[label_idx]+';\n')
        if i % 2 == 0 and i > 0:
            testf.write(out)
            testf.write(','+line[label_idx]+';\n')
