import sys, io, string, os, bisect

def main():
    f = open(sys.argv[1], 'r', buffering=2**29)
    outf = open('%s.tped' % (sys.argv[1],), 'w', buffering=2**29)
    for j,line in enumerate(f):
        if j % 1000==0:
            print j
        line = line.strip().split(' ')
        outline = [line[0], line[1], '0', line[2]]
        nuc1 = line[3]
        nuc2 = line[4]
        ##print nuc1, nuc2
        nuc_opts = [nuc1+' '+nuc1, nuc1+' '+nuc2, nuc2+' '+nuc2]
        ##print nuc_opts
        ##print len(line)
        line = map(float, line[5:])
        ##print len(line)
        ##print len(line) / 3.0
        for i in range(0,len(line),3):
            ##print line[i:(i+3)]
            ##print line[i],  line[i+1],   line[i+2]
            ##print line[i:(i+3)].find(line[i:(i+3)])
            ##print line[i:(i+3)].index(max(line[i:(i+3)]))
            ##print nuc_opts[line[i:(i+3)].index(max(line[i:(i+3)]))]
            outline.append(nuc_opts[line[i:(i+3)].index(max(line[i:(i+3)]))])
        outline[-1] = outline[-1]+'\n'
        outf.write(string.join(outline, ' '))
    f.close()
    outf.close()

if __name__=='__main__':
    main()


