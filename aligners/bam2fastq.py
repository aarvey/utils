#!/usr/bin/python
import sys, string, bisect, loc2seq

def main():
    f = sys.stdin
    i = 0

    k = 1
    output_all = 1; count_only = 0; 
    if len(sys.argv) > k: output_all = int(sys.argv[k]); k += 1
    if len(sys.argv) > k: count_only = int(sys.argv[k]); k += 1
    
    ############# # EXPORT  #
    name_idx = 0  # 4       #
    strand_idx = 1  # 4       #
    read_idx = 9  # 8       #
    qual_idx = 10 # 9       #
    if len(sys.argv) == 4:
        name_idx = int(sys.argv[1])
        strand_idx = int(sys.argv[1])
        read_idx = int(sys.argv[2])
        qual_idx = int(sys.argv[3])
        sys.stderr.write('Using other')
    refs_seen = []
    refs_seen = {}
    i = 0
    for line in f:
        line = line.split('\t')
        i += 1
        if count_only:
            refs_seen[line[name_idx]] = 1
            continue
                              
        ##insert_idx = bisect.bisect_left(refs_seen, line[name_idx])
        ##if len(refs_seen) > 0  and insert_idx != len(refs_seen) and refs_seen[insert_idx] == line[name_idx]:
        if not output_all and refs_seen.has_key(line[name_idx]):
            ##sys.stderr.write('Have a repeat\n')
            junk = 0 
        else:
            read = line[read_idx]
            if line[strand_idx] == '16':
                read = loc2seq.revcomp(read)
            out = '@' + line[name_idx] + '\n' + read + '\n+\n' + line[qual_idx] + '\n'
            sys.stdout.write(out)
            refs_seen[line[name_idx]] = 1
            ##refs_seen.insert(insert_idx, line[name_idx])


    sys.stderr.write('%d unique references out of %d records\n' % (len(refs_seen),i))
    if output_all:
        sys.stderr.write('Output all!!!')
    f.close()

try:     
    main()
except IOError:
    sys.exit(0)
