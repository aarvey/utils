import sys


newlines = []
fname = sys.argv[1]
ofname = sys.argv[2]
f = open(fname)
lines = f.readlines()
f.close()
previd = ''
for j,line in enumerate(lines):
    s = line.split('\t')
    if int(s[-2]) > 0:
        #print 'skipping'
        continue
    #print s[-2]
    id = s[0]
    #if id==previd:
    #    print newlines
    #    previd = id
    #    continue
    #previd = id
    print s
    strand = s[1]
    chr = 'chr' + s[2].split('|')[4].split('_')[0].replace('Mm','')
    start = s[3]
    seq = s[4]
    end = str(int(start) + len(seq) - 1)
    newlines.append('\t'.join([chr, start, end, seq, '1000',  strand]) + '\n')
    print newlines[-1]
    if j%100000==0:
        print 'count', j
        print 'number of unique hits', len(newlines)
    #print '\n'.join(newlines)
    #break


f = open(ofname, 'w')
f.writelines(newlines)
f.close()



