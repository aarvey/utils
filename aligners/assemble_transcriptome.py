
import sys, bisect, os


class Params:
    pass

def main():
    params = Params()
    fname = 'wgEncodeCshlLongRnaSeqGm12878CellLongnonpolyaFastqRd%dRep1'
    fname = 'wgEncodeCshlLongRnaSeqGm12878NucleusLongnonpolyaFastqRd%dRep2'
    fname = 'wgEncodeCshlLongRnaSeqGm12878CytosolLongnonpolyaFastqRd%dRep2'
    params.fname1 = fname % (1)
    params.fname2 = fname % (2)
    run_bowtie(params)
    ##parse_full_reads(params.fname1)
    

def usage():
    print 'python match_pairend_alignments.py  alignfile1  alignfile2'
    print '\t alignfile{1,2}  -- Bowtie alignment files for the left and right ends'
    print '\t                    of paired end reads'
    print '\t                    '
    print '\tThere are components of this code that need to be edited:  '
    print '\t\t get_name(line) --  Should return a unique identifier for a line'
    print '\t\t                    that would match only its pair in the other file'


def run_bowtie_aux(fname, lefttrim, righttrim, runname):
    tfname = fname.replace('.fastq', '')
    bowtie = '/home/aarvey/usr/local/bin/bowtie'
    index = '/unagidata/leslielab/local/share/bowtie/indexes/ebv'
    cmd = '''nice -n 10 %s %s --trim5 %d  --trim3 %d -p 5 -v 1 -k 20 -m 20  -t -q %s --max /dev/null  %s.ebv.bowtie.align.%s >> %s.%s.bowtie.out 2>> %s.ebv.bowtie.align.%s.stats &'''  \
          % (bowtie, index, lefttrim, righttrim, fname, tfname, runname, tfname, runname, tfname, runname)
    print cmd
    ##os.system(cmd)
    
def run_bowtie(params):
    fname = params.fname1
    run_bowtie_aux(fname,0,38,'left')
    run_bowtie_aux(fname,38,0,'right')
    run_bowtie_aux(fname,19,19,'middle')

    fname = params.fname2
    run_bowtie_aux(fname,0,38,'left')
    run_bowtie_aux(fname,38,0,'right')
    run_bowtie_aux(fname,19,19,'middle')


# def get_lines(fname, n=None):
#     f = open(fname)
#     if n==None:
#         lines = f.readlines()
#     else:
#         lines = f.readlines(n)
#     f.close()
#     return lines

# class Align:
#     def __init__(self, line):
#         s = line.split('\t')
#         self.name = s[0][:-2]
#         self.strand = s[1]
#         self.chr = s[2]
#         self.loc = s[3]
#         self.read = s[4]


# class Read:
#     def __init__(self, name='', seq='', qual=''):
#         self.name = name
#         self.seq = seq
#         self.qual = qual
    
# class Reads:
#     def __init__(self, lines, nameonly=False):
#         self.reads = [Read() for i in range(len(lines)/4)]
#         self.names = []
#         r = None
#         for i in range(len(lines)):
#             idx = (i - (i % 4)) / 4
#             if i % 4 == 0:
#                 self.reads[idx].name = lines[i][1:-3]
#             elif i % 4 == 1:
#                 self.reads[idx].seq = lines[i]
#             elif i % 4 == 3:
#                 self.reads[idx].qual = lines[i]
#                 self[(i-3)/4] = r
#         self.names = [r.name for r in self.reads]
#     def sort(self):
#         ord = range(len(self.names))
#         ord.sort(key = lambda j : self.names[j])
#         ##self.names.sort()
#         self.ord = ord

# def parse_full_reads(fname, params):

# if True:
#     leftreads = map(Align, get_lines('%s.ebv.bowtie.align.left' % (fname,)))
#     rightreads = map(Align, get_lines('%s.ebv.bowtie.align.right' % (fname,)))
#     middlereads = map(Align, get_lines('%s.ebv.bowtie.align.middle' % (fname,)))
#     lnames = set([r.name for r in leftreads])
#     rnames = set([r.name for r in rightreads])
#     mnames = set([r.name for r in middlereads])
#     lr = lnames.intersection(rnames)
#     lm = lnames.intersection(mnames)
#     mr = mnames.intersection(rnames)
#     all = lnames.union(rnames).union(mnames)
#     print len(lr), len(lm), len(mr), len(all)


# if True:
#     all = list(all)
#     all.sort()
#     fastq_fname = '%s.fastq' % (fname)
#     names = all
#     parse_full_reads_aux(fastq, names, params, 1000000, True)
    

# parse_full_reads(params)



# def parse_full_reads_aux(fastq_fname, names, params, n=None, testing=False):

# if True:
#     try:
#         fastq = Reads(get_lines(fastq_fname))
#     except:
#         print 'Reading in fastq failed!'


# if True:
#     print len(fastq.reads)
#     if sum([n1==n2  for n1, n2 in zip(names, sorted(names))]) == len(names):
#         print 'names is sorted'
#     else:
#         print 'names is NOT sorted'
    
        
# if True:
#     names1 = names
#     fastq.sort()

# if True:
#     names2 = fastq.names
#     names2.sort()
#     print 'names2:'
#     for i in range(10):
#         print('\t%s' % (names2[i],))
#     ord = fastq.ord
#     print 'names2 ordering:'
#     for i in range(10):
#         o = ord[i]
#         print('\t%d -> %d ->  %s == %s' % (i, o, names2[i], fastq.reads[o].name))


# if True:
#     ret = []
#     outlines = []
#     i = 0
#     kk = len(names2)
#     for i1, n1 in enumerate(names1):
#         tmpidx = bisect.bisect_left(names2, n1)
#         if tmpidx!=kk and names2[tmpidx] == n1:
#             print n1
#             i2 = ord[tmpidx]
#             #print '\n-------------------------------------------------------------\n'
#             #print i1, n1, lines1[i1].strip()
#             #print tmpidx, ord[tmpidx], i2, names2[tmpidx], lines2[i2].strip()
#             #ret.append(fastq.reads[i2])
#             ret.append(names2[tmpidx])
#             ##outlines.append(lines1[i1])
#             ##outlines.append(lines2[i2])

#     return ret



# def order(x, NoneIsLast = True, decreasing = False):
#     """
#     Returns the ordering of the elements of x. The list
#     [ x[j] for j in order(x) ] is a sorted version of x.
#     Missing values in x are indicated by None. If NoneIsLast is true,
#     then missing values are ordered to be at the end.
#     Otherwise, they are ordered at the beginning.
#     """
#     omitNone = False
#     if NoneIsLast == None:
#         NoneIsLast = True
#         omitNone = True
        
#     n  = len(x)
#     ix = range(n)
#     if None not in x:
#         ix.sort(reverse = decreasing, key = lambda j : x[j])
#     else:
#         # Handle None values properly.
#         def key(i, x = x):
#             elem = x[i]
#             # Valid values are True or False only.
#             if decreasing == NoneIsLast:
#                 return not(elem is None), elem
#             else:
#                 return elem is None, elem
#             ix = range(n)
#             ix.sort(key=key, reverse=decreasing)
            
#     if omitNone:
#         n = len(x)
#         for i in range(n-1, -1, -1):
#             if x[ix[i]] == None:
#                 n -= 1
#         return ix[:n]
#     return ix
        





if __name__=="__main__":
    main()
