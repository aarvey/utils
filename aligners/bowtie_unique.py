import sys

maxalign_skip = True

fnames = sys.argv[1:]
for i in range(len(fnames)):
    fname = fnames[i]
    print fname
    f = open(fname)
    lines = f.readlines()
    f.close()
    newlines = []
    name2line = {}
    for j,line in enumerate(lines):
        s = line.split('\t')
        if maxalign_skip and int(s[-2]) > 0:
            continue
        newlines.append(line)
        name = s[0]
        if name2line.has_key(name):
            name2line[name].append(j)
        else:
            name2line[name] = [j]
        if j%100000==0:
            print 'count', j
            print 'number of unique hits', len(newlines)
    newlines = [nl   for i, nl in enumerate(newlines)
                if len(name2line[nl.split('\t')[0]]) == 1]
    f = open('%s.unique' % (fname,), 'w')
    f.writelines(newlines)
    f.close()



