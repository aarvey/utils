

for f in `ls *.bam`; do 
    if [ ! -f $f.unaligned.fastq ]; then
	bam2fastq --unaligned --no-aligned -o $f.unaligned.fastq  $f 
    fi
done