
import sys, commands, traceback
import bisect


## head -n 100 wgEncodeCaltechRnaSeqGm12878R2x75NaIl200FastqRd1Rep1.fastq.left > left1.fastq && tail -n 100 wgEncodeCaltechRnaSeqGm12878R2x75NaIl200FastqRd1Rep1.fastq >> left1.fastq 
## head -n 100 wgEncodeCaltechRnaSeqGm12878R2x75NaIl200FastqRd2Rep1.fastq.right > right1.fastq && tail -n 100 wgEncodeCaltechRnaSeqGm12878R2x75NaIl200FastqRd2Rep1.fastq >> right1.fastq
##  head left1.fastq  right1.fastq
## python ~/ll-utils/aligners/match_pairend_fastq.py left1.fastq right1.fastq
##  head left1.fastq.left right1.fastq.right
## nohup python ~/ll-utils/aligners/match_pairend_fastq.py wgEncodeCaltechRnaSeqGm12878R2x75NaIl200FastqRd1Rep2.fastq wgEncodeCaltechRnaSeqGm12878R2x75NaIl200FastqRd2Rep2.fastq &


def usage():
    print 'python match_pairend_alignments.py  alignfile1  alignfile2'
    print '\t alignfile{1,2}  -- Bowtie alignment files for the left and right ends'
    print '\t                    of paired end reads'
    print '\t                    '
    print '\tThere are components of this code that need to be edited:  '
    print '\t\t get_name(line) --  Should return a unique identifier for a line'
    print '\t\t                    that would match only its pair in the other file'

    

def main():
    testing = False
    try:

        left_name = right_name = sys.argv[1] 
        
        fnames1 = [sys.argv[1]]; left_name = fnames1[0]
        ##fnames1 = commands.getoutput('ls *_1.hg19.bowtie.unmapped.fastq | head -n 75 | tail -n 23').split();
        
        ##lines1 = []
        ##for fname in fnames1:
        ##    print 'Appending', fname, 'to lines1'
        ##    f = open(fname)
        ##    lines1.extend(f.readlines())
        ##    f.close()
        ##    if testing: break

        fnames2 = [sys.argv[2]]; right_name=fnames2[0]
        ##fnames2 = commands.getoutput('ls *_2.hg19.bowtie.unmapped.fastq | head -n 75 | tail -n 23').split(); 
        lines2 = []
        for fname in fnames2:
            print 'Appending', fname, 'to lines2'
            f = open(fname)
            lines2.extend(f.readlines())
            f.close()
            if testing: break

        
        
    except:
        print sys.exc_info()[0]
        traceback.print_exc()
        
        usage()
        sys.exit(1)

    if testing:
        lines2 = lines2[:2000]

    print 'Lines in file 2:', len(lines2)

    ##lines1 = [''.join(lines1[(i*4):((i+1)*4)]) for i in range(len(lines1)/4)]
    lines2 = [''.join(lines2[(i*4):((i+1)*4)]) for i in range(len(lines2)/4)]

    
    #return(line[:line.index('length')])
    def get_name(line):
        return line.split('\n')[0][:-2]

    ##print 'Start sort'
    ##names1 = map(get_name, lines1)
    ##print 'got names for left'
    
    print 'Start sort'
    names2 = map(get_name, lines2)
    print 'got names for right'
    ord = range(len(names2))
    ord.sort(key = lambda j : names2[j])
    names2.sort()
    print 'sorted right (for bisection)'


    ##print 'names1:'
    ##for i in range(10):
    ##    print('\t%s ->  %s' % (names1[i], lines1[i].replace('\n', '\t')))

    print 'names2:'
    for i in range(10):
        print('\t%s -> %s' % (names2[i], lines2[ord[i]].replace('\n', '\t')))

    print 'names2 ordering:'
    for o in ord[:10]:
        print('\t%d ->  %s' % (o, names2[o]))


    testing = False
    if testing:
        joint_names = set(names1)
        joint_names = joint_names.intersection(set(names2))
        print 'Found joint names:'
        tmp = list(joint_names)
        tmp.sort()
        print tmp[:20]
    
    left_outlines = []
    right_outlines = []
    i = 0
    kk = len(names2)




    ##lines1 = []
    for fname in fnames1:
        print 'Appending', fname
        f = open(fname)
        i = 0
        for line in f.xreadlines():
            if i % 1000000 == 0:
                print i
            if i % 4 == 0:
                tmp_lines = []
            tmp_lines.append(line)
            i = i + 1
            if i % 4 != 0: continue

            n1 = get_name(tmp_lines[0])
            tmpidx = bisect.bisect_left(names2, n1)
            if tmpidx!=kk and names2[tmpidx] == n1:
                i2 = ord[tmpidx]
                #print '\n-------------------------------------------------------------\n'
                #print i1, n1, lines1[i1].strip()
                #print tmpidx, ord[tmpidx], i2, names2[tmpidx], lines2[i2].strip()
                right_outlines.append(lines2[i2])
                left_outlines.extend(tmp_lines)
                
    f.close()
    

    print '\n'
    print 'Number of outlines.  Left: ', len(left_outlines), 'Right:',  len(right_outlines)
    print '(Left should be 4x)'

    f = open('%s.left' % (left_name,), 'w')
    f.writelines(left_outlines)
    f.close()

    f = open('%s.right' % (right_name,), 'w')
    f.writelines(right_outlines)
    f.close()
    

if __name__=="__main__":
    main()
