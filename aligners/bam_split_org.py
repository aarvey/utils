#!/usr/bin/python


import os, sys, gzip, pickle, argparse, time
import pysam
from subprocess import call



verbose = False
testing = False


def main(): 
    args = parse_args()
    print(args)

    if not (os.path.exists('%s.bai' % args.b1) and os.path.exists('%s.bai' % args.b2)):
        baif = re.sub('.bam$', '.bai', args.b1)
        print(baif)
        if os.path.exists(baif):
            print('BAI EXISTS!')
        raise NameError("NEED TO HAVE SAMTOOLS INDEX!")

    sf1 = pysam.Samfile(args.b1)
    sf2 = pysam.Samfile(args.b2)
    sys.stderr.write('Org 1 (fname: %s)\n' % (args.b1))
    sys.stderr.write('\tnreads mapped %d\n' % (sf1.mapped))
    sys.stderr.write('\tnreads unmapped %d\n' % (sf1.unmapped))
    sys.stderr.write('Org 2 (fname: %s)\n' % (args.b2))
    sys.stderr.write('\tnreads mapped %d\n' % (sf2.mapped))
    sys.stderr.write('\tnreads unmapped %d\n' % (sf2.unmapped))


    t1 = time.clock()


    n1 = {s.qname : s  for i,s in enumerate(sf1.fetch())}
    n2 = {s.qname : s  for i,s in enumerate(sf2.fetch())}
    names = set(n1.keys()).union(set(n2.keys()))


    def getscore(s): return(dict(s.get_tags())['AS'])

    ofn1 = '%s.org1.bam' % (args.out)
    ofn2 = '%s.org2.bam' % (args.out)
    ofen = '%s.equal.bam' % (args.out)
    of1 = pysam.Samfile(ofn1, "wb", template=sf1)
    of2 = pysam.Samfile(ofn2, "wb", template=sf2)
    ofe = pysam.Samfile(ofen, "wb", template=sf2)

    for name in names:
        if name in n1 and name in n2:
            s1 = getscore(n1[name])
            s2 = getscore(n2[name])
            if (s1 > (s2 + args.delta_min)): 
                of1.write(n1[name])
            elif (s1 < (s2 - args.delta_min)): 
                of2.write(n2[name])
            else:
                ofe.write(n2[name])
        elif name in n1: 
            of1.write(n1[name])
        elif name in n2: 
            of2.write(n2[name])
        else: 
            raise('You have read name with no mapping')
        
    of1.close(); of2.close(); ofe.close()



    call(['samtools', 'sort', '-m', '100000000000',  '-o', ofn1, ofn1])
    call(['samtools', 'index', ofn1])
    call(['samtools', 'sort', '-m', '100000000000',  '-o', ofn2, ofn2])
    call(['samtools', 'index', ofn2])
    call(['samtools', 'sort', '-m', '100000000000',  '-o', ofen, ofen])
    call(['samtools', 'index', ofen])

    
    ##call(['featureCounts', '-g', 'gene_name', '-O', '-M', '-T', '36', '-a', args.gtf1, '-o', '{}.org1.counts'.format(args.out),  ofn1])
    ##call(['featureCounts', '-g', 'gene_name', '-O', '-M', '-T', '36', '-a', args.gtf2, '-o', '{}.org2.counts'.format(args.out),  ofn2])

    t2 = time.clock()
    sys.stderr.write('time to look/edit/write %d\n' % (t2-t1))



def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-out', default='', help='')
    parser.add_argument('-delta_min', default=4, help='')
    parser.add_argument('-b1', default='', help='')
    parser.add_argument('-b2', default='', help='')
    ##parser.add_argument('-gtf1', default='', help='')
    ##parser.add_argument('-gtf2', default='', help='')
    args = parser.parse_args()

    if args.b1=='' or args.b2 == '':
        parser.print_help()
        raise Exception('Arguments missing')

    if not os.path.exists(args.b1):
        parser.print_help()
        raise Exception('File b1 "' + args.b1 + '" is missing')

    if not os.path.exists(args.b2):
        parser.print_help()
        raise Exception('File b2 "' + args.b2 + '" is missing')

    args.delta_min = int(args.delta_min)

    ##if args.gtf1 != '' and args.gtf2 != '' and (not os.path.exists(args.gtf1) or not os.path.exists(args.gtf2)):
    ##    parser.print_help()
    ##   raise Exception('GTF files specified, but files are missing')


    return(args)


if __name__=='__main__':
    main()
