#!/bin/sh
BOWTIE_BUILD_EXE=`which bowtie-build`


INPUTS="../../genomes/mm9/mm9_bs_plus.fa"
${BOWTIE_BUILD_EXE} ${INPUTS} mm9_bs_plus
INPUTS="../../genomes/mm9/mm9_bs_minus.fa"
${BOWTIE_BUILD_EXE} ${INPUTS} mm9_bs_minus

exit 0

INPUTS="../../genomes/ebv/ebv_bs_plus.fa"
${BOWTIE_BUILD_EXE} ${INPUTS} ebv_bs_plus
INPUTS="../../genomes/ebv/ebv_bs_minus.fa"
${BOWTIE_BUILD_EXE} ${INPUTS} ebv_bs_minus



INPUTS="../../genomes/hg19/hg19_bs_plus.fa"
${BOWTIE_BUILD_EXE} ${INPUTS} hg19_bs_plus
INPUTS="../../genomes/hg19/hg19_bs_minus.fa"
${BOWTIE_BUILD_EXE} ${INPUTS} hg19_bs_minus
