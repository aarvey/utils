#!/usr/bin/python

import sys
import commands
import os
import string


def fname2coords(fname, around_center=-1):
    f = open(fname)
    lines = f.readlines()
    f.close()
    coords = []
    for i,line in enumerate(lines):
        s = line.strip().split()
        chr = s[0]
        try:
            start = int(s[1])
            end = int(s[2])
        except:
            start = int(float(s[1]))
            end = int(float(s[2]))
        if around_center > 0:
            center = (start+end)/2
            start = center - around_center
            end = center + around_center 
        if start < 0:
            print 'Warning: start is less than zero on line', i
            start = 0
        if end < 0:
            print 'End is less than zero on line', i
            end = 1
        strand = '+'
        if len(s) > 3:
            strand = s[3]
        name = None
        if len(s) > 4:
            name = '\t'.join(s[4:])
        coords.append(Loc(chr, start, end, strand, name))
    return coords
    

# Get the names of all chromosomes
# The names must be UCSC chromosome names since we use the UCSC promoter
# file for the coordinates
def dir2genome(dir):
    chr_fnames = commands.getoutput('ls ' + os.path.join(dir, 'chr*.fa'))
    chr_fnames = chr_fnames.split('\n')
    chr_seqs = {}    
    for chr_fname in chr_fnames:
        key = chr_fname.replace('.fa','')
        key = key.split('/')[-1]
        print key
        chr_seqs[key] = fname_to_seq(chr_fname)
    return chr_seqs


def coords2records(coords, chr_seqs):
    records = []
    for coord in coords:
        if chr_seqs.has_key(coord.chr):
            seq = chr_seqs[coord.chr][coord.start:coord.end].lower()
            if (coord.strand!=coord.PLUSSTRAND1 and coord.strand!=coord.PLUSSTRAND2) and \
                   (coord.strand==coord.NEGSTRAND1 or coord.strand==coord.NEGSTRAND2):
                seq = revcomp(seq)
        else:
            seq = 'Chromosome is not found'
        records.append(Record(coord, seq))
    return records


def usage():
    print 'loc2seq.py genome_dir out_suffix coords_fname[s] '
    print '   genome_dir is a directory with files named chr*.fa'
    print '   out_suffix is appended to each coords_fname as an outfile'
    print '   coords_fname is file with following format:'
    print '          chr    start    end     strand    [name]'
    

class Loc:
    """
    Location
    """
    def __init__(self, chr, start, stop, strand, name):
        self.chr = chr
        self.start = start
        self.end = stop
        self.strand = strand
        self.name = name
        self.PLUSSTRAND1='1'
        self.PLUSSTRAND2='+'
        self.NEGSTRAND1='0'
        self.NEGSTRAND2='-'
    def tostring(self):
        name = self.name
        if name==None:
            name=''
        else:
            name = name + '\t'
        return name + self.chr + ':' + str(self.start) + '-' + str(self.end) + \
               '(' + self.strand + ')'
        
class Record:
    def __init__(self, loc, seq):
        self.seq = seq
        self.loc = loc
        self.name = ''

    def tostring(self):
        return '>' + self.name + '' + self.loc.tostring() + '\n' + self.seq + '\n'
    
def complement(seq):
        abuffer = []
        for i in seq:
            if i == 'A':
                abuffer.append('T')
            elif i == 'C':
                abuffer.append('G')
            elif i == 'T':
                abuffer.append('A')
            elif i == 'G':
                abuffer.append('C')
            elif i == 'N':
                abuffer.append('N')
            elif i == 'a':
                abuffer.append('t')
            elif i == 'c':
                abuffer.append('g')
            elif i == 't':
                abuffer.append('a')
            elif i == 'g':
                abuffer.append('c')
            elif i == 'n':
                abuffer.append('n')
        return abuffer
            
def revcomp(s):
    s = s.lower()
    s = s[::-1]
    s = complement(s)
    return ''.join(s)

        
def fname_to_seq(fname):
    f = open(fname)
    lines = f.readlines()
    f.close()
    lines = map(string.strip, lines)
    #comment_linenums = [line[0]=='>'  for line in lines]
    if lines[0][0]=='>':
        lines = lines[1:]
    seq = ''.join(lines)
    return seq

def fname_to_seqs(fname):
    f = open(fname)
    lines = f.readlines()
    f.close()
    seqs = []
    for line in lines:
        if line[0]=='>':
            seqs.append([])
        seqs[-1].append(line.strip())
    for i in range(len(seqs)):
        seqs[i] = (seqs[i][0], ''.join(seqs[i][1:]))
    return seqs


def write_fasta(out_fname, records):
    f = open(out_fname, 'w')
    lines = [record.tostring() for record in records]
    f.writelines(lines)
    f.close()

def main():
    try: 
        genome_dir = sys.argv[1]
        out_suffix = sys.argv[2]
        coords_fnames = sys.argv[3:]
    except:
        usage()
        sys.exit(1)
    around_center = -1000
    if around_center > 0:
        print 'You are using a center region, not the absolute coords!'
        var = raw_input('Are you okay with doing this? [Y/n]')
        if var.strip().lower()=='n':
            sys.exit(0)
    chrs = dir2genome(genome_dir)
    for coords_fname in coords_fnames:
        coords = fname2coords(coords_fname, around_center)
        print [c.tostring() for c in coords[:5]]
        records = coords2records(coords, chrs)
        print [r.tostring() for r in records[:1]]
        write_fasta(coords_fname + '.' + out_suffix, records)

if __name__=='__main__':
    main()
