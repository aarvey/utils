#!/usr/bin/python
import sys, string

def main():
    f = sys.stdin
    i = 0
    for line in f:
        line = line.split('\t')
	chr = line[0]
	pos = line[1]
	pos2 = line[2]
	strand = line[5].strip()
        out = '\t'.join(['n%d'%(i,), strand, chr, pos, 'A'*(int(pos2)-int(pos)), 'I'*(int(pos2)-int(pos)), '0', ' \n'])
        sys.stdout.write(out)
        i += 1
    f.close()


#HWUSI-EAS1614:1:1:3391:1141#0/1	+	chr2	32279288	CATGGNACCCACTCAGCATACAGACCTGCTTCCATT	aZZZ_B````aa^aaggacggcgeggedagdcb_de 0 5:C>N
#chr3	116725145	116725170	U0	0	-


try:     
    main()
except IOError:
    sys.exit(0)
