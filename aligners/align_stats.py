#!/usr/bin/python

import sys


fastq_reads = '?'
try:
    fastq_reads = sys.argv[2]
except:
    pass


f = open(sys.argv[1])
lines = f.readlines()
f.close()

i = 0
unique_hits = 0
total_alignments = len(lines)
while i < len(lines):
    x = lines[i].split('\t')
    query = x[0]
    j = i 
    while j < len(lines):
        y = lines[j].split('\t')
        if y[0]!=query: break
        j = j + 1
    i = j

    unique_hits += 1



output = '''Stats:
# reads processed: %s
# reads with at least one reported alignment: %d
# reads that failed to align: ?
# reads with alignments suppressed due to -m: ?
Reported %d alignments to 1 output stream(s)
'''  %  (fastq_reads, unique_hits, total_alignments)
##'''  %  (fastq_reads, unique_hits, noalign, overalign, total_alignments)
    

f = open('%s.stats' % (sys.argv[1],), 'w')
f.writelines(output)
f.close()

