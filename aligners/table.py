#!/usr/bin/python

import sys, pysam

f = open(sys.argv[2])
fai = f.readlines()
f.close()
gene2len = {}
expr = {}
print 'setting up table'
for i in range(len(fai)):
    x = fai[i].split('\t')
    gene2len[x[0]] = float(x[1]) / float(1000.0)
    expr[x[0]] = 0.0

print 'reading in data'
records = []
if sys.argv[1][-5:]=='align':
    print '\t bowtie format'
    f = open(sys.argv[1])
    lines = f.readlines()
    records = [l.split('\t') for l in lines]
    f.close()
elif sys.argv[1][-3:]=='bam':
    print '\t bam format'
    records = []
    samfile = pysam.Samfile(sys.argv[1], "rb" )
    i = 1
    for s in samfile:
        rec = [s.qname, 0, samfile.getrname(s.tid)]
        records.append(rec)
        if i % 1000000 == 0:
            print i
        i += 1
    samfile.close()
print 'Number of records: %d' % (len(records),)


print 'getting values'
i = 0
unique_hits = 0
while i < len(records):
    x = records[i]
    query = x[0]
    genes = []
    j = i 
    while j < len(records):
        y = records[j]
        if y[0]!=query: break
        genes.append(y[2])
        j = j + 1
    i = j
    incr = 1.0 / len(genes)
    for gene in genes:
        expr[gene] = expr[gene] + incr
    unique_hits += 1

print 'writing values'
f = open('%s.uniquehits.python' % (sys.argv[1],), 'w')
f.writelines('%d\n' % (unique_hits,))
f.close()

norm = float(len(records)) / float(1000000.0)
f = open('%s.expr.txt' % (sys.argv[1],), 'w')
f.writelines(['%s\t%0.4f\n' % (k, expr[k] / norm / gene2len[k])  for k in sorted(gene2len.keys())])
f.close()
    
