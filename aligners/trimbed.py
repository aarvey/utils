#!/usr/bin/python
import sys, string, bisect, loc2seq

def main():
    gsize_fname = sys.argv[1]
    chr_sizes = {}
    f = open(gsize_fname)
    lines = f.readlines()
    for i in range(len(lines)):
        chr = lines[i].split()[0]
        size = lines[i].split()[1]
        chr_sizes[chr] = int(size)
    f.close()

        
    f = sys.stdin
    i = 0
    ############# # EXPORT  #
    chr_idx = 0    # 4       #
    start_idx = 1  # 4       #
    end_idx = 2    # 8       #
    ref_idx = 3    # 9       #
    strand_idx = 4 # 9       #
    i = 0
    for line in f:
        line = line.split('\t')
        i += 1

        line[start_idx] = str(max(int(line[start_idx]), 1))
        line[end_idx] = str(min(int(line[end_idx]), chr_sizes[line[chr_idx]]-1))
        strand = line[strand_idx].strip()
        if strand=='+' or strand == '-':
            line[strand_idx] = '0\t' + line[strand_idx]
        sys.stdout.write('\t'.join(line))

    f.close()

try:     
    main()
except IOError:
    sys.exit(0)
