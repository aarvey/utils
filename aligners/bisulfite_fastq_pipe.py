# The Computer Language Benchmarks Game
# http://shootout.alioth.debian.org/
#
# contributed by Jacob Lee, Steven Bethard, et al
# modified by Justin Peel

import sys, string

import signal
signal.signal(signal.SIGPIPE, signal.SIG_DFL)

def main():
    fname = sys.argv[1]
    minus = int(sys.argv[2])

    find = 'C'
    replace = 'T'
    if minus > 0:
        find = 'G'
        replace = 'A'


    if len(sys.argv) > 3:
        find = sys.argv[3]
        replace = sys.argv[4]
        
    if fname=='-':
        f = sys.stdin
    else:
        f = open(fname)
    i = 0

    for line in f:
        if i % 4 == 1:
            line = line.replace(find, replace)
        sys.stdout.write(line)
        i += 1
    f.close()

main()
