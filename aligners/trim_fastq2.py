#!/usr/bin/python

import sys

ln = int(sys.argv[1])
s = 0
if len(sys.argv) > 2:
    s = int(sys.argv[2])
for i,line in enumerate(sys.stdin):
    if i % 4 == 1 or i % 4 == 3:
        line = line[s:ln] + '\n'
    sys.stdout.write(line)
