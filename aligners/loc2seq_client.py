#!/usr/bin/python

import sys
import commands
import os
import string
from loc2seq_server import Loc, Record, genome_dir_to_port, coords2records, test_client
import pickle, socket


def fname2coords(fname):
    f = open(fname)
    lines = f.readlines()
    f.close()
    coords = []
    for i,line in enumerate(lines):
        s = line.strip().split()
        chr = s[0]
        try:
            start = int(s[1])
            end = int(s[2])
        except:
            start = int(float(s[1]))
            end = int(float(s[2]))
        if start < 0:
            print 'Warning: start is less than zero on line', i
            start = 0
        strand = '+'
        if len(s) > 3:
            strand = s[3]
        name = None
        if len(s) > 4:
            name = '\t'.join(s[4:])
        coords.append(Loc(chr, start, end, strand, name))
    return coords

def usage():
    print 'loc2seq.py genome_dir out_suffix coords_fname[s] '
    print '   genome_dir is a directory with files named chr*.fa'
    print '   out_suffix is appended to each coords_fname as an outfile'
    print '   coords_fname is file with following format:'
    print '          chr    start    end     strand    [name]'
    

def write_fasta(out_fname, records):
    f = open(out_fname, 'w')
    lines = [record.tostring() for record in records]
    f.writelines(lines)
    f.close()

def main():
    try: 
        genome_dir = sys.argv[1]
        out_suffix = sys.argv[2]
        coords_fnames = sys.argv[3:]
    except:
        usage()
        sys.exit(1)

    if False and around_center > 0:
        print 'You are using a center region, not the absolute coords!'
        var = raw_input('Are you okay with doing this? [Y/n]')
        if var.strip().lower()=='n':
            sys.exit(0)
            
    ip = 'localhost'
    port = genome_dir_to_port(genome_dir)
    for coords_fname in coords_fnames:
        coords = fname2coords(coords_fname)
        print [c.tostring() for c in coords[:5]]
        records = coords2records(coords, ip, port)
        print [r.tostring() for r in records[:1]]
        write_fasta(coords_fname + '.' + out_suffix, records)


def test():
    genome_dir = sys.argv[1]
    out_suffix = sys.argv[2]
    coords_fnames = sys.argv[3:]

    ip, port = 'localhost', genome_dir_to_port(genome_dir)
    print 'Using host, port', ip, port
    
    test_client(ip, port)
    test_client(ip, port)




if __name__=='__main__':
    test()
    main()
