import sys

ncchr_to_chr = {}
chr_to_num = {}
for i in range(1,23):
    ncchr_to_chr['NC_0000%.2d' % i] = 'chr%d' % i
    ncchr_to_chr['chr%d' % i] = 'chr%d' % i
    chr_to_num['chr%d' % i] = str(i)
ncchr_to_chr['NC_000023'] = 'chrX'
ncchr_to_chr['chrX'] = 'chrX'
chr_to_num['chrX'] = '23'
ncchr_to_chr['NC_000024'] = 'chrY'
ncchr_to_chr['chrY'] = 'chrX'
chr_to_num['chrY'] = '24'
ncchr_to_chr['NC_001807'] = 'chrM'
ncchr_to_chr['chrY'] = 'chrM'
chr_to_num['chrM'] = '25'
ncchr_to_chr['chrEBV'] = 'chrEBV'

maxalign_skip = False
chr_idx = 2
skip = 2

fnames = sys.argv[1:]
for i in range(len(fnames)):
    fname = fnames[i]
    f = open(fname)
    lines = f.readlines()
    f.close()

    lines = lines[2:len(lines)]

    try:
        line = lines[2]
        s = line.split('\t')
        ncchr = s[chr_idx].split('|')[3].split('.')[0]
        chr = ncchr_to_chr[ncchr]
    except:
        #print "Unexpected error:", sys.exc_info()[0]
        print 'exception made for file', fname
        continue
        raise
    newlines = []
    for j,line in enumerate(lines):
        s = line.split('\t')
        if maxalign_skip and int(s[-2]) > 0:
            print 'skipping'
            continue
        #id = s[0]
        #strand = s[1] # str(int(s[1]=='+'))
        ncchr = s[chr_idx].split('|')[3].split('.')[0]
        chr = ncchr_to_chr[ncchr]
        #chr = chr_to_num[ncchr_to_chr[ncchr]]
        #start = s[3]
        newline = s
        newline[chr_idx] = chr
        newlines.append('\t'.join(newline))
        if j%100000==0:
            print 'count', j
            print 'number of unique hits', len(newlines)

    
    f = open('%s' % (fname,), 'w')
    f.writelines(newlines)
    f.close()



