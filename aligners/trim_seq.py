#!/home/aarvey/usr/local/bin/python

import sys

def main():
    fname = sys.argv[1]
    if (fname=="-"):
        f = sys.stdin
    else:
        f = open(sys.argv[1])
    for line in f:
        s = line.split('\t')
        strand = s[1]
        seq = s[4]
        if strand=="-":  
            s[4] = seq[(-len(s[5])):]
        else:
            s[4] = seq[:len(s[5])]
        sys.stdout.write('\t'.join(s))
    f.close()

if __name__=="__main__":
    main()
 
