#!/usr/bin/python


##import sys, os, bisect, time, pysam, aln, gzip
import sys, os, bisect, time,  aln, gzip

verbose = True


def main():
    fname = sys.argv[1]
    bwt_idx = sys.argv[2]
    bwt_idx_name = bwt_idx.split('/')[-1]
    seed_len = 36
    if len(sys.argv) > 3: mismatch = int(sys.argv[3])
    if len(sys.argv) > 4:  seed_len = int(sys.argv[4])
    if len(sys.argv) > 5:  bowtie_ver = sys.argv[5]


    bowtie_cmd = 'bowtie %s -p 16 -l %d -n %d -e 100000 -k 1 -t -q - %s.rm.align' % (bwt_idx, seed_len, mismatch, fname)
    if bowtie_ver == '2':
        bowtie_cmd = 'bowtie2 -x %s  -t -p 16 --local -D 20 -R 6 -N 1 -L 24 -i S,1,1.0 -q - --un /dev/null -S - | awk \'BEGIN { FS = "\t" }; $2 != 4\' > %s.rm.align' % (bwt_idx, fname)
    
    print 'bwt_idx', bwt_idx
    print 'seed_len', seed_len
    print 'bowtie_ver', bowtie_ver
    print 'bowtie_cmd', bowtie_cmd
      
    use_star = 0
    if len(sys.argv) > 6:
        use_star = int(sys.argv[6])
    if use_star:
        raise "STAR not yet supported!"



    
    t1 = time.clock()
    alnfile = aln.alnfile(fname,long_x0=False)
    alnfile.read_alns()
    lines = alnfile.lines
    records = alnfile.records
    samfile = alnfile.samfile
    t2 = time.clock()
    print 'Time to read in', t2-t1
    



    t1 = time.clock()
    lastref = ''
    refs = []
    fastq_lines = []
    for i, r in enumerate(records):
        ref = r.ref
        refs.append(ref)
        if lastref==ref: continue

        fastq_lines.append('@' + ref + '\n')
        fastq_lines.append(r.read + '\n')
        fastq_lines.append('+\n')
        fastq_lines.append(r.qual + '\n')

        lastref = ref
    ##print(fastq_lines)
    f = gzip.open('%s.fastq.gz' % (fname,), 'wb')
    f.writelines(fastq_lines)
    f.close()
    cmd = 'gzip -cd %s.fastq.gz | %s' % (fname, bowtie_cmd)
    if len(fastq_lines) > 5*10^6:
        cmd = 'nice -n 10 %s' % (cmd,)
    os.system(cmd)
    del fastq_lines
    t2 = time.clock()
    print 'Time to find index matches', t2-t1


    t1 = time.clock()
    f = open('%s.rm.align' % (fname,))
    rm_refs = unique([l.split('\t')[0]  for l in f.readlines()])
    rm_refs = [rr  for rr in rm_refs if rr[0] != '@']
    f.close()
    ord = range(len(refs))
    ord.sort(key = lambda j : refs[j])
    refs.sort()
    t2 = time.clock()
    print 'Time to find bad refs and sort orig refs', t2-t1
        

    t1 = time.clock()
    outlines = []
    kk = len(refs)
    rm_idx = []
    for i1, rm_ref in enumerate(rm_refs):
        tmpidx = bisect.bisect_left(refs, rm_ref)
        while tmpidx!=kk and refs[tmpidx]==rm_ref:
            i2 = ord[tmpidx]
            rm_idx.append(i2)
            if verbose:
                print i1, rm_ref
                print tmpidx, ord[tmpidx], refs[tmpidx], records[ord[tmpidx]].ref
            tmpidx += 1


    t2 = time.clock()
    print 'Time to find bad refs in orig refs', t2-t1



    t1 = time.clock()
    if verbose:
        print rm_refs
        print rm_idx

    rm_set = set(rm_idx)
    nset = set([i for i in range(len(lines))])
    nset = nset.difference(rm_set)
    lines_i = sorted(list(nset))
    lines = [lines[i]  for i in lines_i]
    t2 = time.clock()
    print 'Time to remove bad refs in orig refs', t2-t1
    
    t1 = time.clock()
    print 'here'
    out_fname = '%s.sub_%s.align' % (fname, bwt_idx_name)
    print 'here'
    if (fname[-3:]=='bam'):
        print 'here'
        out_fname = '%s.bam' % (out_fname)
        print 'here'
        f = pysam.Samfile(out_fname, "wb", template=samfile)
        print 'here'
        nada = [f.write(l) for l in lines]
        f.close()
    else:
        f = open(out_fname, 'w')
        f.writelines(lines)
        f.close()

    t2 = time.clock()
    print 'Time to write to disk', t2-t1
    
            
    
def unique(seq):
    return {}.fromkeys(seq).keys()
        

if __name__=='__main__':
    main()



