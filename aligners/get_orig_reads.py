#!/usr/bin/python


import sys, os, bisect, string, gzip, aln

revcomp_table = string.maketrans('ACBDGHKMNSRUTWVYacbdghkmnsrutwvy',
                                 'TGVHCDMKNSYAAWBRTGVHCDMKNSYAAWBR')
def revcomp(seq):
    s = (''.join(seq)).translate(revcomp_table, '\n')[::-1]
    return s

        
import sys, os, bisect, time, pysam, aln, gzip
def main():
    aln_fname = sys.argv[1]
    fastq_fname = sys.argv[2]

    print 'Reading in bam file...'
    if (aln_fname[-3:]=='.gz' or aln_fname[-6:]=='.align'):
        f = open(aln_fname)
        aln_lines = f.readlines()
        f.close()
        aln_refs = [l[:l.find('\t')]  for l in aln_lines]    
    elif (aln_fname[-4:]=='.bam'):
        alnfile = aln.alnfile(aln_fname)
        alnfile.read_alns()
        lines = alnfile.lines
        aln_lines = alnfile.records
        aln_sams = alnfile.lines
        samfile = alnfile.samfile
        aln_refs = [l.ref  for l in aln_lines]
        
    ord = range(len(aln_refs))
    ord.sort(key = lambda j : aln_refs[j])
    aln_refs.sort()


    verbose = False
    i = -1
    j = 0
    tmpidx = 0
    has_match = False
    s = ['']*4
    len_aln_refs = len(aln_refs)


    if (aln_fname[-3:]=='bam'):
        print 'Doing bam'
        out_fname = '%s_orig.bam' % (aln_fname)
        outf = pysam.Samfile(out_fname, "wb", template=samfile)


    print 'Going through fastq file...'
    if fastq_fname[-3:] == '.gz':
        fastq_fh = gzip.open(fastq_fname)
    else:
        fastq_fh = open(fastq_fname)
    for line in fastq_fh:
        i += 1
        if i % 4000000 == 0:
            sys.stderr.write(str(i/4)+'\n')
        if i % 4 == 0:
            has_match = False
            fastq_ref = line[1:-1].split()[0]
            tmpidx = bisect.bisect_left(aln_refs, fastq_ref)
            if tmpidx == len_aln_refs:
                continue
            j = ord[tmpidx]
            if aln_lines[j].ref != fastq_ref:
                continue
            has_match = True
            if verbose:
                print i, j, tmpidx, fastq_ref
        if i % 4 == 1 and has_match:
            fastq_read = line.strip()
            aln_lines[j].read = aln_sams[j].seq = fastq_read
            pysam.AlignedRead()
            if (aln_lines[j].strand == -1):
                aln_lines[j].read = aln_sams[j].seq = revcomp(aln_lines[j].read)
        if i % 4 == 3 and has_match:
            aln_sams[j].qual = line.strip()
            aln_sams[j].cigar = [(0,len(aln_sams[j].seq))]
            outf.write(aln_sams[j])
            #if verbose:
            #    print tmpidx, j, s, aln_lines[j], fastq_ref
    fastq_fh.close()
    outf.close()
    print 'Completed'

if __name__=='__main__':
    main()
