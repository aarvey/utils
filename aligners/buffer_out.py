


import sys, io
def main():
    fi = open('/dev/stdin', 'rb', buffering=2**22)
    fo = open(sys.argv[1], 'wb', buffering=2**22)
    for line in fi.read():
        ##print 'I/O happening'
        fo.write(line)
        ##print 'I/O done write'
    fo.close()
    fi.close()
    

if __name__=='__main__':
    main()
