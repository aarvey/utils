#!/usr/bin/python


import os, sys, pickle, pysam
import methyl_count as mc

class record:
	pass

def get_consensus(align_file, genome):
	consensus = {}
	for c in nucs:
		consensus[c] = [0  for i in range(len(genome))]
	verbose=False



	i = 0
	records = []
	if (align_file[-3:]=='bam'):
		samfile = pysam.Samfile(align_file, "rb" )
		##for s in samfile.fetch('chr1', 0, len(genome)):
		for s in samfile.fetch():
			records.append(s)
			if (i+1) % 100000 == 0:
				print i
			i += 1
		samfile.close()
	else:
		f = open(align_file)
		lines = f.readlines()
		f.close()
		for i, line in enumerate(lines):
			if (i+1) % 100000 == 0:
				print i
			s = line.split('\t')
			r = record()
			r.seq = s[4]
			r.pos = int(s[3])
			records.append(r)
			i += 1
		
		
	for s in records:
		read = s.seq
		read_len = len(read)
		pos = s.pos
		if verbose:
			print '----------------------------------------------'
			if s.is_reverse:
				print read
				print genome[pos:(pos+read_len)]
			else:
				print read
				print genome[pos:(pos+read_len)]
			
		for j in range(6,read_len-6):
			consensus[read[j]][pos+j] = consensus[read[j]][pos+j] + 1


	print 'Reads processed', len(records)
	return consensus

nucs = ['A', 'C', 'G', 'T', 'N', '.']
def output_perc(consensus, genome):
	not_counted = 0
	counted = 0
	var_count = 0
	
	for i in range(len(genome)):
		c = genome[i]
		count = sum([consensus[n][i] for n in nucs])
		
		if count < 10:
			not_counted += 1
			continue

		bscount = count - consensus[c][i]
		
		var = bscount / float(count)
		idx = 0
		
		if var > 0.15:
			var_count += 1
			print i, genome[i-2:i+3], count, bscount, var, 'Genome:', genome[i], ' A:', consensus['A'][i], ' C:', consensus['C'][i], ' G:', consensus['G'][i], ' T:', consensus['T'][i]
			
		counted += 1


	print 'Counted', counted
	print 'Not Counted', not_counted
	print 'meC', var_count
	
def main():

	genome_fname = sys.argv[1]
	align_fname = sys.argv[2]
	
	genome = mc.get_genome(genome_fname)
	consensus_fname = '%s.consensus' % (align_fname,)
	##if os.path.exists(consensus_fname):
	if False and os.path.exists(consensus_fname):
		f = open(consensus_fname)
		consensus = pickle.load(f)
		f.close()		
	else:
		consensus = get_consensus(align_fname, genome)
		f = open(consensus_fname, 'w')
		pickle.dump(consensus, f)
		f.close()
	output_perc(consensus, genome)


if __name__=='__main__':
	main()
