#!/usr/bin/python
import sys, string, bisect, loc2seq

def main():
    f = sys.stdin
    i = 0

    strand_idx = 1
    chr_idx = 2
    pos_idx = 3
    read_idx = 9

    prev_pos = ''
    for line in f:
        i += 1
        if (i % 1000000 ==0):  sys.stderr.write(str(i)+'\n')
        line = line.split('\t')

        pos = line[pos_idx]
        
        strand = ''
        if line[strand_idx] == '16':
            strand = '-'
            pos = str(int(pos) + len(line[read_idx]))

        pos = strand + pos
        if pos==prev_pos: continue
        prev_pos = pos

        out = line[chr_idx] + '\t' + pos + '\n'
        sys.stdout.write(out)

    f.close()

try:     
    main()
except IOError:
    sys.exit(0)
