#!/usr/bin/python


## Here are some notes on this script

# It randomly selects the read to include.  This is in contrast to
# samtools rmdup, which uses best mapping providing a major bias
# towards reference (especially for highly expressed genes in
# RNA-seq).  Picard takes the sum of qualities of mapping base pairs,
# which dramatically reduces the bias of samtools, but leaves open to
# issues of sequencing iteration nucleotide bias.  I always felt
# better with 100% random selection.

# It removes (not marks) duplicates.

# It assumes coordinate sorting (not query sorting)

# It has not been properly tested.

# It looks like it handles strandedness by allowing start/stop on a
# given position if on opposite strand

#  It does not consider paired reads.  Picard considers pairs as a
#  single read extended to 5'.  This seemed a bit aggressive to me and
#  randomly picking paired ends seemed an improvement when pairing as
#  it would avoid double counting a given haplotype and loosing
#  spatial IID.


import sys, os, bisect, time, pysam, aln, gzip, random, re
random.seed(1*3*5*7*11*13*17*19*23-1)
verbose = True
testing = False

def main():
    fname = sys.argv[1]
    
    t1 = time.clock()

    if not os.path.exists('%s.bai' % fname):
        baif = re.sub('.bam$', '.bai', fname)
        print baif
        if os.path.exists(baif):
            print 'BAI EXISTS!'
        raise NameError("NEED TO HAVE SAMTOOLS INDEX!")

    samfile = pysam.Samfile(fname)
    sys.stderr.write('nreads mapped %d\n' % (samfile.mapped))
    sys.stderr.write('nreads unmapped %d\n' % (samfile.unmapped))

    header = samfile.header
    print header
    print ''
    print ''
    print header['PG']
    print ''
    print ''
    pg_dict = {'ID':'rmdup_gatk.py', 'VN':'0.1', 'CL:':' '.join(sys.argv)}
    header['PG'].append(pg_dict)
    print header['PG']
    print ''
    print ''
    
    out_fname = '%s.myrmdup.bam' % (fname)
    ##out_fname = '/dev/stdout'
    ##out_fname = 'asdf.bam'
    ##out_fname = '/dev/null'
    outf = pysam.Samfile(out_fname, "wb", template=samfile)
    ##outf = pysam.Samfile(out_fname, "wb", header=header)
    last_pos = -123456789
    last_s = -123456789
    i = 0
    records = []
    for i,s in enumerate(samfile.fetch()):
        ##if (i > 1*10**6):  break
        if (testing and i > 50000):  break
        ##if i > 10**7:  break
        if i % 10**6 == 0:  sys.stderr.write('On record %d\r'%i)
        records.append(s)
        pos = records[-1].pos
        if last_pos > -1000 and pos!=last_pos:
            if testing: sys.stderr.write('\nWriting records on record %d\n'%i)
            write_records(records[:-1], outf)
            records = [s]
        last_pos = pos
        
    outf.close()
    t2 = time.clock()
    sys.stderr.write('time to look/edit/write %d\n' % (t2-t1))

    sys.exit(0)

def write_records(tmp_recs, outf):
    strands = [s.is_reverse for s in tmp_recs]
    strand_idx = [[tmp for tmp,strand in enumerate(strands) if strand], [tmp for tmp,strand in enumerate(strands) if not strand]]
    if testing: 
        sys.stderr.write('selecting records for position %d\n' % tmp_recs[0].pos)
        print strands
        sys.stderr.write('Strands: [%s]\n' % (','.join([str(tmp) for tmp in strands])))
        sys.stderr.write('Strands idx: [%s] [%s]\n' % (','.join([str(tmp) for tmp in strand_idx[0]]), ','.join([str(tmp) for tmp in strand_idx[1]])))
    for strand in range(2):
        if len(strand_idx[strand]) == 0: continue
        ridx = random.sample(strand_idx[strand], 1)[0]
        if testing:
            sys.stderr.write('Selected read:\n')
            sys.stderr.write('\t %d\n' % strand)
            sys.stderr.write('\t %s\n' % ','.join([str(tmp) for tmp in strand_idx[strand]]))
            sys.stderr.write('\t idx selected for strand %d\n' % ridx)
        s = tmp_recs[ridx]
        s.is_duplicate = 0
        outf.write(s)


if __name__=='__main__':
    main()
