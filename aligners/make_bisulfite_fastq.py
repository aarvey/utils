import sys
def main():
    for fname in sys.argv[1:]:
        print fname
        f = open(fname)
        lines = f.readlines()
        f.close()

        galines = lines[:]
        for i in range(1, len(lines), 4):
            galines[i] = lines[i].replace('G', 'A')
            lines[i] = lines[i].replace('C', 'T')
            if i % 25000000 == 0:
                print '\t', i

        
        f = open(fname.replace('fastq', 'bs.ct.fastq'), 'w')
        f.writelines(lines)
        f.close()
        
        f = open(fname.replace('fastq', 'bs.ga.fastq'), 'w')
        f.writelines(galines)
        f.close()
        

    
main()
            
