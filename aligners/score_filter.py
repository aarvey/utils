#!/usr/bin/python


import sys, os, bisect, time, pysam, aln, gzip
def main():
    fname = sys.argv[1]
    
    t1 = time.clock()
    alnfile = aln.alnfile(fname,long_x0=True)
    print 'here 1'
    alnfile.read_alns()
    print 'here 2'
    lines = alnfile.lines
    records = alnfile.records
    samfile = alnfile.samfile
    t2 = time.clock()
    print 'Time to read in', t2-t1
    
    f = pysam.Samfile('%s.score_filter.bam' % (fname,), "wb", template=samfile)
    for i,r in enumerate(records):
        if r.naln > 100:
            f.write(lines[i]) 
        

if __name__=='__main__':
    main()



