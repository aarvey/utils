#!/usr/bin/python

import sys


def th_sort(fname):
    verbose = False
    id2pnt = {}
    ids = []

    print 'before read lines'
    f = open(fname)
    lines = f.readlines()
    f.close()
    print 'after read lines'
    print 'collecting ids'
    for i,line in enumerate(lines):
        if line[0]=='>':
            if verbose:
                print line
            id = line[1:-1]
            ids.append(id)
            id2pnt[id] = i
        if i % 100000 == 0:
            print i, len(lines)
    print 'collected ' + str(len(ids)) + ' ids'
    leftps = []
    rightps = []
    for i, id in enumerate(ids):
        id = id[:-1]
        if id2pnt.has_key(id + '1') and id2pnt.has_key(id + '2'):
            leftps.append(id2pnt[id + '1'])
            rightps.append(id2pnt[id + '2'])
        if i % 100000 == 0:
            print i, len(ids)
    print 'got left/right for', len(leftps)

    leftlines = []
    rightlines = []
    seen = [False  for i in range(len(lines))]
    i = 0
    for i in range(len(leftps)):
        leftp = leftps[i]
        rightp = rightps[i]
        if verbose:
            print leftp, rightp
            print lines[leftp].strip()
            print lines[leftp+1].strip()
            print lines[rightp].strip()
            print lines[rightp+1].strip()
        if seen[leftp] and seen[rightp]:
            continue
        seen[leftp] = True
        seen[rightp] = True
        leftlines.extend(lines[leftp:(leftp+2)])
        rightlines.extend(lines[rightp:(rightp+2)])
        if i % 100000 == 0:
            print i, len(leftlines), len(leftps)
    f = open(fname + '.left', 'w')
    f.writelines(leftlines)
    f.close()
    print 'wrote left'
    f = open(fname + '.right', 'w')
    f.writelines(rightlines)
    f.close()
    print 'wrote right'

    
        
def main():
    fnames = sys.argv[1:]
    for fname in fnames:
        th_sort(fname)

if __name__=='__main__':
    main()

