diff.peaks.get.plots <- function(di) {
  ##source("~/path.R"); library(ks); library(gplots)
  joint <- di

  name.add <- "selected_joints"
  if (!is.null(di$name.add)){name.add <- di$name.add}
  name1 <- di$name1;
  name2 <- di$name2;  
  
  x <- joint$mat[,1]
  y <- joint$mat[,2]
  base.name <- sprintf("%s_%s_%s_jointreads_kde_peaks%d_radius100", name.add, name1, name2, length(x))
  pdf(file=sprintf("%s.pdf", base.name))
  plot(x, y, cex=0.5, log="", xlab=name1, ylab=name2)
  dev.off()
  
  qn <- quant.norm(cbind(x,y))
  x <- qn$qn[,1]; normed.x <- x
  y <- qn$qn[,2]; normed.y <- y
  pdf(file=sprintf("%s_norm.pdf", base.name))
  plot(x,y, xlab=name1, ylab=name2, cex=0.5, log="")
  dev.off()
  
  pdf(file=sprintf("%s_mva.pdf", base.name))
  m <- (x+y)/2
  a <- x-y
  plot(m, a, xlab="Geometric Mean", ylab=sprintf("Log(%s / %s)", name1, name2), cex=0.5, log="")
  dev.off()
  
  h <- hist(m, 20);
  min.num <- 10
  ths <- c(h$breaks[h$counts>min.num])
  var.dat <- list()
  for (i in 2:length(ths)) {
    idx <- which(m >= ths[i-1] & m < ths[i])
    if (length(idx)<min.num){next}
    var.dat[[length(var.dat)+1]] <- list()
    var.dat[[length(var.dat)]]$idx <- idx;       var.dat[[length(var.dat)]]$ths <- ths;
    var.dat[[length(var.dat)]]$vals <- a[var.dat[[length(var.dat)]]$idx];       var.dat[[length(var.dat)]]$i <- i
    var.dat[[length(var.dat)]]$low <- ths[length(var.dat)]
    var.dat[[length(var.dat)]]$high <- ths[i]
    var.dat[[length(var.dat)]]$sd <- sd(a[var.dat[[length(var.dat)]]$idx])
    var.dat[[length(var.dat)]]$mu <- mean(a[var.dat[[length(var.dat)]]$idx])
  }
  pdf(file=sprintf("%s_mva_model.pdf", base.name))
  plot(m, a, xlab="Geometric Mean", ylab=sprintf("Log(%s / %s)", name1, name2), cex=0.5, log="", xlim=c(min(m),max(m)))
  for (v in var.dat) {
    x <- mean(c(v$low,v$high));       ##show(c(v$low,v$high,x,v$high-v$low,v$sd))
    symbols(x=x, y = v$mu, rectangles=matrix(c(v$high-v$low, 2*v$sd),1,2), lwd=2, fg="red",
            add=T, inches=F)
    arrows(x, v$mu+v$sd, x, v$mu+v$sd*2, code=2, angle=90, length=0.1, col="red")
    arrows(x, v$mu-v$sd, x, v$mu-v$sd*2, code=2, angle=90, length=0.1, col="red")
  }
  dev.off()
  
  var.dat <- list()
  sorted.a <- a[order(m)]
  sorted.m <- sort(m)
  win.size <- 100;   win.move <- 20
  win.size <- round(length(sorted.a)/50); win.move <- round(win.size / 5)
  for (i in seq(win.size, length(sorted.a), win.move)) {
    var.dat[[length(var.dat)+1]] <- list()
    start <- i - win.size + 1
    end <- i
    var.dat[[length(var.dat)]]$low <- sorted.m[start]
    var.dat[[length(var.dat)]]$high <- sorted.m[end]
    var.dat[[length(var.dat)]]$sd <- sd(sorted.a[start:end])
    var.dat[[length(var.dat)]]$q1 <- quantile(sorted.a[start:end], 0.05)
    var.dat[[length(var.dat)]]$q2 <- quantile(sorted.a[start:end], 0.95)
    var.dat[[length(var.dat)]]$mu <- mean(sorted.a[start:end])
  }
  pdf(file=sprintf("%s_mva_model_window.pdf", base.name))
  plot(m, a, xlab="Geometric Mean", ylab=sprintf("Log(%s / %s)", name1, name2), cex=0.5, log="", xlim=c(min(m),max(m)))
  for (v in var.dat) {
    x <- mean(c(v$low,v$high))
    points(x, v$mu+v$sd, col="red", cex=0.5, pch=4)
    points(x, v$mu-v$sd, col="red", cex=0.5, pch=4)
  }
  dev.off()
  return(list(x=normed.x, y=normed.y, var.dat=var.dat))
}






diff.peaks.joints2models <- function(joints) {
  joint.models <- list()
  for (k in 1:length(joints)) {
    show(k)
    show(names(joints)[k])
    joint.models[[k]] <- diff.peaks.get.plots(joints[[k]])
  }
  return(joint.models)
}





diff.peaks.avg.vars <- function(model1, model2) {
  rep.model <- list()
  lows1 <- sapply(model1$var.dat, "[[", "low")
  highs1 <- sapply(model1$var.dat, "[[", "high")
  means1 <- rowMeans(cbind(sapply(model1$var.dat, "[[", "low"), sapply(model1$var.dat, "[[", "high")))

  lows2 <- sapply(model2$var.dat, "[[", "low")
  highs2 <- sapply(model2$var.dat, "[[", "high")
  means2 <- rowMeans(cbind(sapply(model2$var.dat, "[[", "low"), sapply(model2$var.dat, "[[", "high")))

  to.fit.both <- unique(rbind(cbind(means1, sapply(model1$var.dat, "[[", "sd")), cbind(means2, sapply(model2$var.dat, "[[", "sd"))))
  to.fit1 <- unique(cbind(means1, sapply(model1$var.dat, "[[", "sd")))
  to.fit2 <- unique(cbind(means2, sapply(model2$var.dat, "[[", "sd")))

  to.fit.bigger <- to.fit1
  if (mean(to.fit2[,2]) > mean(to.fit1[,2])) {
    show("using bigger two")
    to.fit.bigger <- to.fit2
  } else {
    show("using bigger one")
  }

  to.fit.q1 <- unique(cbind(means1, sapply(model1$var.dat, "[[", "q2")))
  to.fit.q2 <- unique(cbind(means2, sapply(model2$var.dat, "[[", "q2")))


  make.monotone <- function(y) {
    for (i in (length(y)-10):1) {
      y[i] <- max(mean(y[i:(i+10)]), y[i])
    }
    return(y)
  }
  
  init.params <- c(alpha=0.2, beta=0.4, gamma=0.5)

  ##for (to.fit in list(to.fit.bigger, to.fit.both, to.fit1, to.fit2, to.fit.q1, to.fit.q2)) {
  to.fit.list <- list(bigger=to.fit.bigger, both=to.fit.both, one=to.fit1, two=to.fit2)
  for (fit.name in names(to.fit.list)) {
    to.fit <- to.fit.list[[fit.name]]
    colnames(to.fit) <- c("m", "sd")
    ##rm.idx <- which(to.fit[,1] < 1.5)
    ##if (length(rm.idx) > 0) {to.fit <- to.fit[-rm.idx,] }
    y <- to.fit[,2]
    y <- make.monotone(to.fit[,2])
    x <- to.fit[,1]
    show(rbind(x,y))
    fit <- try(nls(y ~ alpha + beta*exp(-gamma*x), start=init.params, algorithm="port"), silent=F)
    is.error <- is(fit, "try-error")
    if (is.error) {
      fit <- try(nls(y ~ alpha + beta*exp(-gamma*x), start=init.params), silent=F)
      is.error <- is(fit, "try-error")
    }

    if (!is.error) { show(sprintf("USING FIT: '%s'", fit.name)); break; }
  }
  if (is.error) {
    show("NO FIT WORKED!!!!   NOW DOING GHETTO LINEAR FIT TO SD AND NOISE ETC");
    to.fit <- to.fit1
    y <- to.fit[,2]
    show(y[seq(1,length(y),length(y)/20)])
    y <- make.monotone(to.fit[,2])
    show(y[seq(1,length(y),length(y)/20)])
    x <- to.fit[,1]
    show(to.fit)
    show(cbind(x,y))
    show(length(y))
    show(length(x))
    fit <- lm(y~x);
    ret <- list();
    show(fit)
    ret$fit.x <- fit.x <- sort(c(x, seq(-200,2000,10)/100))
    ret$fit.y <- fit.y <- fit$coef[1] + fit$coef[2] * ret$fit.x
    show(fit.x[seq(1,length(fit.x), length(fit.x)/30)])
    show(fit.y[seq(1,length(fit.x), length(fit.x)/30)])
    return(ret)
  }
  ##return()
  show(fit)
  if(F && any(abs(coefficients(fit) - coefficients(fit2))  / (abs(coefficients(fit) + coefficients(fit2))/2)  > 0.1)) {
    show(fit)
    show(fit2)
  }
  
  alpha <- coefficients(fit)["alpha"]
  beta <- coefficients(fit)["beta"]
  gamma <- coefficients(fit)["gamma"]

  fit.x <- sort(c(gamma*to.fit[,1], seq(10,2000)/100))
  fit.y <- alpha + beta*exp(-gamma*fit.x)
  ret <- list()
  ret$fit.x <- fit.x
  ret$fit.y <- fit.y
  ret$params <- coefficients(fit)
  ret$dat <- to.fit
  return(ret)
}
diff.peaks.get.sd <- function(model, val) {
  idx <- c()
  for (i in 1:length(val)) {
    idx[i] <- which.min(abs(model$fit.x - val[i]))
  }
  return(model$fit.y[idx])
}


diff.peaks <- function(joint, model1, gm.name, model2, k.name, fname.root) {
  show("Getting average model")
  rep.model <- diff.peaks.avg.vars(model1, model2)
  diff <- joint$x - joint$y
  show("Getting std dev estimates and pvals")
  p.vals <- pnorm(diff-mean(diff), sd=diff.peaks.get.sd(rep.model, rowMeans(cbind(joint$x, joint$y))))
  p.vals <- pmin(p.vals, 1-p.vals)
  return(list(p=p.vals, rep.model=rep.model))

  hyp.vals <- seq(10, 10000, 20) / 500
  hyp.sds <- diff.peaks.get.sd(rep.model, hyp.vals)
  hyp.diffs <- seq(-10000, 0, 10)/1000
  hyp.p.vals <- lapply(hyp.sds, function(sigma) { pnorm(hyp.diffs, sd=sigma) } )
  hyp.cutoffs <- abs(sapply(hyp.p.vals, function(hp){  hyp.diffs[max(which(hp < 0.05))] }))
  hyp.cutoffs[is.na(hyp.cutoffs)] <- 0.01

  pdf(sprintf("%s_mva_poly.pdf", fname.root))
  x <- rowMeans(cbind(joint$x, joint$y))
  y <- joint$x-joint$y
  plot(x, y, type="n", xlab=gm.name, ylab=k.name)

  tmp.x <- c(0, hyp.vals, hyp.vals[length(hyp.vals)], -100)
  tmp.y <- c(hyp.cutoffs[1], hyp.cutoffs, 0, 0)
  ##polygon(tmp.x, tmp.y, col="red")
  tmp.y <- -tmp.y
  ##polygon(tmp.x, tmp.y, col="red")

  points(x,y, cex=0.5, pch=16)
  lines(c(-10,20), c(0, 0), lwd=2)

  dev.off()
  
  pdf(sprintf("%s_poly.pdf", fname.root))
  plot(joint$x, joint$y, type="n", xlab=gm.name, ylab=k.name)

  ##tmp.x <- c(hyp.vals, rev(hyp.vals))
  ##tmp.y <- c(hyp.vals-0.00, rev(hyp.vals)+rev(hyp.cutoffs))
  tmp.x <- c(0, hyp.vals, hyp.vals[length(hyp.vals)], -100)
  tmp.y <- c(hyp.cutoffs[1], hyp.cutoffs, 0, 0)
  tmp <- cbind(tmp.x, tmp.y) * sqrt(2)
  r <- pi/4
  tmp <- t(matrix(c(cos(r), sin(r), -sin(r), cos(r)), 2, 2) %*% t(tmp))
  tmp.x <- tmp[,1]
  tmp.y <- pmax(tmp[,2], tmp.x+min(abs(hyp.cutoffs)))
  tmp.y <- tmp[,2]
  ##polygon(tmp.x, tmp.y, col="red")

  tmp.tmp <- tmp.x
  tmp.x <- tmp.y
  tmp.y <- tmp.tmp
  ##polygon(tmp.x, tmp.y, col="red")

  points(joint$x, joint$y, cex=0.5, pch=16)
  lines(c(-5,20), c(-5, 20), lwd=2)
  dev.off()
  ##system("scp selected_joints*poly.pdf cbio.mskcc.org:~/public_html/chipseq/.")

  return(list(p=p.vals, rep.model=rep.model))
}

