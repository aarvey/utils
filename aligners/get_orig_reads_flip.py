#!/usr/bin/python


import sys, os, bisect, string

revcomp_table = string.maketrans('ACBDGHKMNSRUTWVYacbdghkmnsrutwvy',
                                 'TGVHCDMKNSYAAWBRTGVHCDMKNSYAAWBR')
def revcomp(seq):
    s = (''.join(seq)).translate(revcomp_table, '\n')[::-1]
    return s

        
def main():
    aln_fname = sys.argv[1]
    fastq_fname = sys.argv[2]

    f = open(aln_fname)
    aln_lines = f.readlines()
    f.close()

    f = open(fastq_fname)
    fastq_lines = f.readlines()
    f.close()

    aln_refs = [l[:l.find('\t')]  for l in aln_lines]    
    fastq_refs = [fastq_lines[i][1:-1]  for i in range(0, len(fastq_lines), 4)]
    fastq_reads = [fastq_lines[i].strip()  for i in range(1, len(fastq_lines), 4)]
    
    ord = range(len(fastq_refs))
    ord.sort(key = lambda j : fastq_refs[j])
    fastq_refs.sort()

    kk = len(fastq_refs)
    verbose = False
    for i1, aln_ref in enumerate(aln_refs):
        tmpidx = bisect.bisect_left(fastq_refs, aln_ref)
        s = aln_lines[i1].split('\t')
        if verbose:
            print tmpidx, kk, s[4]
        i2 = ord[tmpidx]
        s[4] = fastq_reads[i2]
        if (s[1]=='-'):
            s[4] = revcomp(s[4])
            
        if verbose:
            print s[4]
        aln_lines[i1] = '\t'.join(s)

    f = open('%s.origreads' % (aln_fname), 'w')
    f.writelines(aln_lines)
    f.close()

if __name__=='__main__':
    main()
