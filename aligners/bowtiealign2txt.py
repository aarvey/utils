#!/usr/bin/python

import sys

def convert(fname):
    f = open(fname)
    lines = f.readlines()
    f.close()

    for i in range(len(lines)):
        s = lines[i].split('\t')
        lines[i] = s[2] + '\t' + s[3] + '\t' + s[1] + '\t' + str(len(s[4])) + '\n'

    f = open('%s.txt' % (fname,), 'w')
    f.writelines(lines)
    f.close()


def main():
    fnames = sys.argv[1:]
    for fname in fnames:
        convert(fname)

if __name__=='__main__':
    main()





