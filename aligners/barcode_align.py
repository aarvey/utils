#!/usr/bin/python

import os
import sys

def fastx_rename(fname):
    f = open(fname)
    lines = f.readlines()
    f.close()

    is_fastq = False
    if lines[2][0]=='+':
        is_fastq = True

    #print fname
    #print 'is_fastq:', is_fastq
    
    for i in range(len(lines)):
        
        if not is_fastq and (lines[i][0]=='>' or lines[i].strip()==''):
            continue

        if is_fastq and (lines[i][0]=='+' or lines[i][0]=='@' or lines[i-1][0]=='+'):
            continue

        if is_fastq and not lines[i-1][0]=='@':
            continue

        if is_fastq and i % 4 != 1:
            continue

        #print i, is_fastq, lines[i-1][0], not lines[i-1][0]=='@', lines[i-1][0]=='@'
        #print lines[i]
        #if line[0:5]!=lines[i-1][1:6]:
        #    print 'WTF'
        #    continue
        #print lines[(i-1):(i+1)]

        bclen = 5
        
        lines[i-1] = lines[i-1][0]+lines[i][:bclen] + '\n'
        lines[i+1] = lines[i+1][0]+lines[i][:bclen] + '\n'
        lines[i] = lines[i][bclen:]
        lines[i+2] = lines[i+2][bclen:]
        #print lines[(i-1):(i+1)]


    fname = fname.replace('.fastq', '').replace('.fasta', '')
    fname_add = '.fasta'
    if is_fastq: fname_add = '.fastq'
    f = open('%s.notag%s' % (fname,fname_add), 'w')
    f.writelines(lines)
    f.close()



def aligned_unique(fname):

    fname = '%s' % (fname,)
    f = open(fname)
    lines = f.readlines()
    f.close()
    newlines = []

    previd = ''
    prevstrand = ''
    prevchr = ''
    prevstart = ''
    chr2locs = {}
    
    for j,line in enumerate(lines):
        s = line.split('\t')
        if int(s[-2]) > 0:
            #print 'skipping'
            continue
        id = s[0]
        strand = s[1]
        chr = s[2]
        start = int(s[3])

        if not chr2locs.has_key(chr):
            chr2locs[chr] = {}

        if not chr2locs[chr].has_key(start):
            chr2locs[chr][start] = {}

        if not chr2locs[chr][start].has_key(strand):
            chr2locs[chr][start][strand] = {}

        if chr2locs[chr][start][strand].has_key(id):
            #print 'already got it'
            continue
        
        chr2locs[chr][start][strand][id] = 1

        newline = s
        newlines.append('\t'.join(newline))
        
        if j%10000==0:
            print 'count', j
            print 'number of unique hits', len(newlines)
    
    f = open('%s.uniquetags' % (fname,), 'w')
    f.writelines(newlines)
    f.close()

                                                                                                                                                                                

def main():
    #align('wt.fa')
    #align('155.fa')
    task = sys.argv[1]
    fnames = sys.argv[2:]
    for fname in fnames:
        if task=='rename': fastx_rename(fname)
        if task=='unique': aligned_unique(fname)

if __name__=='__main__':
    main()
