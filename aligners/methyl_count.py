#!/usr/bin/python


import os, sys, pickle, time
from blist import blist


def get_genome(genome_fname):
	genome = {}
	f = open(genome_fname)
	lines = f.readlines()
	f.close()
	rm_lines = [i  for i,l in enumerate(lines) if l[0]=='>']
	rm_lines.append(len(lines))
	print rm_lines
	for i in range(len(rm_lines)-1):
		idx = rm_lines[i]
		chr = lines[idx][1:-1]
		genome[chr] = (''.join(lines[(idx+1):rm_lines[i+1]])).replace('\n','')

		print >> sys.stderr, chr
		print >> sys.stderr, 'A', genome[chr].count('A')
		print >> sys.stderr, 'C', genome[chr].count('C')
		print >> sys.stderr, 'G', genome[chr].count('G')
		print >> sys.stderr, 'T', genome[chr].count('T')
		print >> sys.stderr, 'N', genome[chr].count('N')
		
	
	#for i in range(len(rm_lines))[::-1]:
	#	if rm_lines[i]:
	#		lines.pop(i)
	#genome = (''.join(lines)).replace('\n','')

	return genome


def get_consensus(align_fname, genome):
	is_circular_genome = True

	consensus = {}
	chr_sizes = {}
	for chr in genome.keys():
		nchr = len(genome[chr])
		chr_sizes[chr] = nchr
		consensus[chr] = {}
		for c in ['A', 'C', 'G', 'T', 'N', '.']:
			x = blist([0.0])
			x *= nchr
			consensus[chr][c] = x
		
	verbose = False
	i = -1
	f = open(align_fname)
	for line in f:
		i += 1
		if i % 100000 == 0:
			print >> sys.stderr, i
		s = line.split('\t')
		read = s[4][:50]
		if s[1]=='-': read = s[4][-50:]
		read_len = len(read)
		pos = int(s[3])
		chr = s[2].replace('minus_','').replace('plus_', '')
		s[6] = float(s[6])

		nchr = chr_sizes[chr]
		if verbose:
			print >> sys.stderr, '----------------------------------------------'
			if s[1]=="+":
				print >> sys.stderr, read
				print >> sys.stderr, genome[pos:(pos+read_len)]
			else:
				print >> sys.stderr, read
				print >> sys.stderr, genome[pos:(pos+read_len)]
			
		for j in range(read_len):
			idx = pos + j
			if idx >= nchr and is_circular_genome:
				idx = idx - nchr
			consensus[chr][read[j]][idx] = consensus[chr][read[j]][idx] + (1.0/(s[6]+1.0))

	f.close()
	print >> sys.stderr, 'Reads processed', i
	return consensus


def output_perc(consensus, genome):
	not_counted = 0
	counted = 0
	cpg = [0, 0]
	chg = [0, 0]
	chh = [0, 0]
	ismeth = 0

	print('\t'.join(['chr', 'idx','char','seq','A','C','G','T','count','bscount','meth','context']))	
	for chr in sorted(consensus.keys()):
		for i in range(len(genome[chr])):
			out = []
			out.append(chr)
			out.append(i+1)
			out.append(genome[chr][i])
			out.append(genome[chr][i-3:i+4])

			c = genome[chr][i]
			fs = rs = False
			count = bscount = 0
			if c == 'C':
				count = consensus[chr]['T'][i]
				bscount = consensus[chr]['C'][i]
				fs = True
			elif c == 'G':
				count = consensus[chr]['A'][i]
				bscount = consensus[chr]['G'][i]
				rs = True

			out.extend([consensus[chr]['A'][i], consensus[chr]['C'][i], consensus[chr]['G'][i], consensus[chr]['T'][i]])

			meth = 0
			if count + bscount > 0:
				meth = ( bscount / (count+bscount) )
			out.extend([count, bscount, meth])

			if c != 'C' and c != 'G':
				out.append('NA')
				##continue
			else:
				idx = 0
				if meth > 0.2:
					ismeth += 1
					idx = 1

				if (genome[chr][i+1]=='G' and fs) or (genome[chr][i-1]=='C' and rs):
					cpg[idx] += 1
					out.append('cpg')
				elif (genome[chr][i+1]!='G' and genome[chr][i+2]=='G' and fs) or (genome[chr][i-1]!='C' and genome[chr][i-2]=='C' and rs):
					chg[idx] += 1
					out.append('chg')
				elif (genome[chr][i+1]!='G' and genome[chr][i+2]!='G' and fs) or (genome[chr][i-1]!='C' and genome[chr][i-2]!='C' and rs):
					chh[idx] += 1				
					out.append('chh')

				if count + bscount > 10: counted += 1
				else: not_counted += 1

			print('\t'.join([str(o) for o in out]))

	print >> sys.stderr, 'Counted', counted
	print >> sys.stderr, 'Not Counted', not_counted
	print >> sys.stderr, 'meC', ismeth
	print >> sys.stderr, 'CpG', cpg, cpg[1] / float(cpg[0]+cpg[1])
	print >> sys.stderr, 'CHG', chg, chg[1] / float(chg[0]+chg[1])
	print >> sys.stderr, 'CHH', chh, chh[1] / float(chh[0]+chh[1])

	
def main():

	genome_fname = sys.argv[1]
	align_fname = sys.argv[2]

	print >> sys.stderr, 'Getting genome'
	t1 = time.clock()
	genome_pickle_fname = '%s.pickle' % (genome_fname,)
	if os.path.exists(genome_pickle_fname):
		f = open(genome_pickle_fname)
		genome = pickle.load(f)
		f.close()		
		
	else:
		genome = get_genome(genome_fname)
		f = open(genome_pickle_fname, 'w')
		pickle.dump(genome, f)
		f.close()

	t2 = time.clock()
	print >> sys.stderr, 'Time to get genome', t2-t1
								    
		
	print >> sys.stderr, 'Getting consensus from alignments'
	t1 = time.clock()
	consensus_fname = '%s.consensus' % (align_fname,)
	if os.path.exists(consensus_fname):
		f = open(consensus_fname)
		consensus = pickle.load(f)
		f.close()		
	else:
		consensus = get_consensus(align_fname, genome)
		f = open(consensus_fname, 'w')
		pickle.dump(consensus, f)
		f.close()

	t2 = time.clock()
	print >> sys.stderr, 'Time to get consensus', t2-t1
	
	##consensus = get_consensus(align_fname, genome)
	output_perc(consensus, genome)


if __name__=='__main__':
	main()




