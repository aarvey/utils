#!/usr/bin/python


import sys, os, bisect, time, pysam, aln, gzip

verbose = True


def main():
    fname = sys.argv[1]
    
    t1 = time.clock()

    samfile = pysam.Samfile(fname)
    out_fname = '%s.correctqual.bam' % (fname)
    outf = pysam.Samfile(out_fname, "wb", template=samfile)
    records = []
    for i,s in enumerate(samfile.fetch()):
        records.append(s)
        if i > 10**5:     break

    print 'computing quals'
    qual_counts = [0 for i in range(100)]
    for s in records:
        for j in [ord(c)-33 for c in s.qual]:
            qual_counts[j] = qual_counts[j] + 1
    if sum(qual_counts[50:]) == 0:
        print 'Already using correct qual range'
        sys.exit(0)
        
    print 'NEED TO CORRECT!'

    t2 = time.clock()
    print 'time to read', t2-t1


    for i,s in enumerate(samfile.fetch()):
        records.append(s)
        ##if i > 10**5:     break

    
    #qual_counts = [0 for i in range(100)]
    #for s in records:
    #    for j in [ord(c)-33 for c in s.qual]:
    #        qual_counts[j] = qual_counts[j] + 1
    #    ##print qual_counts
    # 
    #print 'Total reads:', i
    #for i in range(len(qual_counts)):
    #    if qual_counts[i] > 0:
    #        print i, qual_counts[i]

    t2 = time.clock()
    print 'time to quals', t2-t1
    

    if sum(qual_counts[50:]) > 500:
        for s in records:
            s.qual = ''.join([chr(ord(q)-31)  for q in s.qual])
            outf.write(s)


    outf.close()
    samfile.close()
    t2 = time.clock()
    print 'time to finish', t2-t1

    sys.exit(0)

            

if __name__=='__main__':
    main()
