#!/usr/bin/python


import sys, os, bisect, time, pysam, aln, gzip, random
random.seed(1*3*5*7*11*13*17*19*23-1)
verbose = True

def main():
    fname = sys.argv[1]
    
    t1 = time.clock()
    testing = False

    samfile = pysam.Samfile(fname)
    out_fname = '%s.myrmdup.bam' % (fname)
    out_fname = '/dev/stdout'
    outf = pysam.Samfile(out_fname, "wb", template=samfile)
    rlen = 10
    records = []
    for i,s in enumerate(samfile.fetch()):
        ##print s.cigar
        ##print s.cigarstring
        ##s.qname = 'n%d' % (i,)
        records.append(s)
        if (testing and i > 50000):  break
        ##if i > 10**7:  break
        
    t2 = time.clock()
    print 'time to read', t2-t1
    
    i = 0
    out_records = []
    while i < len(records):
        j = i
        tmp_recs = []
        while j < len(records):
            s = records[j]
            if len(tmp_recs) > 0 and tmp_recs[-1].pos!=s.pos: break
            tmp_recs.append(s)
            if testing: print j, s.pos, s.is_duplicate, s.cigar, s.is_reverse, s
            j += 1
        strands = [s.is_reverse for s in tmp_recs]
        strand_idx = [[i for i,strand in enumerate(strands) if strand], [i for i,strand in enumerate(strands) if not strand]]
        if testing: 
            print 'selecting records for position', s.pos
            print strands
            print strand_idx
        for strand in range(2):
            if len(strand_idx[strand]) == 0: continue
            ridx = random.sample(strand_idx[strand], 1)[0]
            if testing:
                print strand
                print strand_idx[strand]
                print len(strand_idx[strand])
                print 'idx selecting for strand', strand
                print 'idx selected for strand', strand, 'is', ridx
            s = tmp_recs[ridx]
            s.is_duplicate = 0
            out_records.append(s)
        i = j + 1
        
    t2 = time.clock()
    print 'time to look/edit', t2-t1
    for i,s in enumerate(out_records):
        outf.write(s)

    outf.close()
    t2 = time.clock()
    print 'time to finish write', t2-t1

    sys.exit(0)

            

if __name__=='__main__':
    main()
