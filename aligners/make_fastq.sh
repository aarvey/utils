#!/bin/sh

##for f in `find | grep 'lite.sra$' | sort | tail -n 20`; do 
##for f in `find | grep 'lite.sra$'`; do 
for f in `find | grep 'sra$'`; do 
    cwd=`pwd`; 
    dir=`echo $f | sed 's/SRR[0-9]*.sra$//g' | sed 's/SRR[0-9]*.lite.sra$//g'`
    echo "Going into $dir"
    cd $dir
    newf=`basename $f | sed 's/.lite.sra$//g' | sed 's/.sra$//g'`; 
    f=`basename $f`
    echo "$f -> $newf"; 
    if [ ! `ls -l $f | cut -d' ' -f5` -gt 100 ]; then
	echo "FILE IS TOO SMALL!!!!"
	cd $cwd
	continue
    fi
    acc=`basename $f sra`; 
    acc="$newf"
    if [ -f "$acc.fastq" ]; then 
	echo "exists"; 
	cd $cwd
	continue; 
    fi
    if [ -f "$acc.fastq.gz" ]; then 
	echo "exists"; 
	cd $cwd
	continue; 
    fi
    if [ -f "$f.md5" ]; then 
	md5sum -b $f > $f.my.md5
	diff --brief $f.my.md5 $f.md5
	if [ "$?" -gt 0 ]; then 
	    echo "YOU HAVE A BAD DOWNLOADED FILE!!!!"
	    cd $cwd
	    continue
	fi
    fi
    ##/kanibeast1/aarvey/programs/sratoolkit.2.1.10-centos_linux64/bin/fastq-dump --gzip -A $acc $f;
    ##nice -n 10 /kanibeast1/aarvey/programs/sratoolkit.2.2.0-centos_linux64/bin/fastq-dump --gzip -A $acc $f;
    nice -n 10 /home/aarvey/programs/sratoolkit.2.3.5-2-ubuntu64/bin/fastq-dump --gzip -A $acc $f;
    if [ "$?" -eq 0 ]; then
	rm $f
    else
	rm $acc.fastq.gz
    fi
    echo "Going back to $cwd"
    cd $cwd
done
