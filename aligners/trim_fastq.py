OAOAOAOAA#!/usr/bin/python

import sys

minq = int(sys.argv[1])
minlen = int(sys.argv[2])
maxlen = int(sys.argv[3])
files = sys.argv[4:]
posavg = []
newposavg = []
for fname in files:
    f = open(fname)
    lines = f.readlines()
    f.close()
    
    for j in range(len(lines)/4):
        i = j * 4
        titleline = lines[i]
        seqline = lines[i+1]
        qualline = lines[i+3]

        qual = [ord(c) for c in qualline.strip()]
        #posavg.append(qual)
        islow = [q < minq  for q in qual]
        newlen = len(islow)
        if sum(islow) > 1:
            newlen = islow.index(True)
            if sum(islow) > 2:
                newlen = islow.index(True, newlen+1)
        if newlen < minlen:
            lines[i+0]=''
            lines[i+1]=''
            lines[i+2]=''
            lines[i+3]=''
            continue

        if newlen > maxlen:
            newlen = maxlen

        idx = titleline.find("length=")
        origlen = titleline[(idx+7):(idx+9)]
        titleline = titleline.replace('length=' + origlen, 'length=%d' % (newlen,))

     

        lines[i] = titleline
        lines[i+1] = seqline[:newlen] + '\n'
        lines[i+2] = titleline
        lines[i+3] = qualline[:newlen] + '\n'

        lines[i] = '>' + titleline
        lines[i+1] = seqline[:newlen] + '\n'
        lines[i+2] = ''
        lines[i+3] = ''

    f = open(fname + '.trim', 'w')
    f.writelines(lines)
    f.close()
            

