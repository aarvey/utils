#!/usr/bin/python

import sys, os
import commands
import string
import pickle
import traceback

import socket
import threading
import SocketServer


def usage():
    print 'loc2seq.py genome_dir out_suffix coords_fname[s] '
    print '   genome_dir is a directory with files named chr*.fa'
    
def genome_dir_to_port(dir):
    while dir[len(dir)-1]=='/' or dir[len(dir)-1]=='\\':
        dir = dir[:-1]
    print 'Hashing dir', dir
    port = hash(dir)
    port = int(port % 5e4 + 10000)
    port = port
    return(port)
    

# Get the names of all chromosomes
# The names must be UCSC chromosome names since we use the UCSC promoter
# file for the coordinates
def dir2genome(dir):
    chr_fnames = commands.getoutput('ls ' + os.path.join(dir, 'chr*.fa'))
    chr_fnames = chr_fnames.split('\n')
    chr_seqs = {}    
    for chr_fname in chr_fnames:
        key = chr_fname.replace('.fa','')
        key = key.split('/')[-1]
        print key
        chr_seqs[key] = fname_to_seq(chr_fname)
    return chr_seqs
def fname_to_seq(fname):
    f = open(fname)
    lines = f.readlines()
    f.close()
    lines = map(string.strip, lines)
    #comment_linenums = [line[0]=='>'  for line in lines]
    if lines[0][0]=='>':
        lines = lines[1:]
    seq = ''.join(lines)
    return seq
def fname_to_seqs(fname):
    f = open(fname)
    lines = f.readlines()
    f.close()
    seqs = []
    for line in lines:
        if line[0]=='>':
            seqs.append([])
        seqs[-1].append(line.strip())
    for i in range(len(seqs)):
        seqs[i] = (seqs[i][0], ''.join(seqs[i][1:]))
    return seqs



SOCK_SEND_SEP="SOCK_SEND_SEP"
def recv_bs(sock):
    all_data = []
    data = sock.recv(32)
    tmp = data.split(SOCK_SEND_SEP)
    if len(tmp) == 1:
        print 'There is not any SOCK_SEND_SEP in the first 32 bytes!!!'
    if len(tmp) > 2:
        print 'More than one SOCK_SEND_SEP is present in the first 32 bytes!!!'
    recv_buffer_size = int(tmp[0])
    print 'Receiver allocating buffer of size', recv_buffer_size
    all_data.append(tmp[1])
    len_data = len(tmp[1])
    while len_data < recv_buffer_size:
        data = sock.recv(2**16)
        all_data.append(data)
        len_data += len(data)
        print 'Received data of size', len(data),  'totalling', len_data
    data = ''.join(all_data)
    
    print 'Received data of size', len(data)
    data = pickle.loads(data)
    
    return(data)

def send_bs(sock, dat):
    dat_ps = pickle.dumps(dat)
    ps_size = str(len(dat_ps))
    print 'Sender recommends buffer size', ps_size
    sock.send(ps_size + SOCK_SEND_SEP)
    sock.send(dat_ps)
    
if False:
    coords_ps_size = str(len(coords_ps))
    print 'Client recommends buffer size', coords_ps_size
    sock.send(coords_ps_size + SOCK_SEND_SEP)
    print 'Client sending coords'
    sock.send(coords_ps)

def coords2records_client(coords, ip, port):
    print 'Opening socket'
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect((ip, port))
    print 'Sending coordindates'
    send_bs(sock, coords)
    print 'Receiving sequences'
    response = recv_bs(sock)
    sock.close()
    return response
coords2records = coords2records_client

def coords2records_server(coords, chr_seqs):
    records = []
    for coord in coords:
        if chr_seqs.has_key(coord.chr):
            seq = chr_seqs[coord.chr][(coord.start-1):coord.end].lower()
            if (coord.strand!=coord.PLUSSTRAND1 and coord.strand!=coord.PLUSSTRAND2) and \
                   (coord.strand==coord.NEGSTRAND1 or coord.strand==coord.NEGSTRAND2):
                seq = revcomp(seq)
        else:
            seq = 'Chromosome is not found'
        records.append(Record(coord, seq))
    return records


class Loc:
    """
    Location
    """
    def __init__(self, chr, start, stop, strand, name):
        self.chr = chr
        self.start = start
        self.end = stop
        self.strand = strand
        self.name = name
        self.PLUSSTRAND1='1'
        self.PLUSSTRAND2='+'
        self.NEGSTRAND1='0'
        self.NEGSTRAND2='-'
    def tostring(self):
        name = self.name
        if name==None:
            name=''
        else:
            name = name + '\t'
        return name + self.chr + ':' + str(self.start) + '-' + str(self.end) + \
               '(' + self.strand + ')'
        
class Record:
    def __init__(self, loc, seq):
        self.seq = seq
        self.loc = loc
        self.name = ''

    def tostring(self):
        return '>' + self.name + '' + self.loc.tostring() + '\n' + self.seq + '\n'
    
def complement(seq):
        abuffer = []
        for i in seq:
            if i == 'A':
                abuffer.append('T')
            elif i == 'C':
                abuffer.append('G')
            elif i == 'T':
                abuffer.append('A')
            elif i == 'G':
                abuffer.append('C')
            elif i == 'N':
                abuffer.append('N')
            elif i == 'a':
                abuffer.append('t')
            elif i == 'c':
                abuffer.append('g')
            elif i == 't':
                abuffer.append('a')
            elif i == 'g':
                abuffer.append('c')
            elif i == 'n':
                abuffer.append('n')
        return abuffer
            
def revcomp(s):
    s = s.lower()
    s = s[::-1]
    s = complement(s)
    return ''.join(s)



if False:
    all_data = []
    data = self.request.recv(32)
    tmp = data.split(SOCK_SEND_SEP)
    if len(tmp) == 1:
        print 'There is not any SOCK_SEND_SEP in the first 32 bytes!!!'
    if len(tmp) > 2:
        print 'More than one SOCK_SEND_SEP is present in the first 32 bytes!!!'
    recv_buffer_size = int(tmp[0])
    print 'Server allocating buffer of size', recv_buffer_size
    all_data.append(tmp[1])
    len_data = len(tmp[1])
    while len_data < recv_buffer_size:
        data = self.request.recv(2**16)
        all_data.append(data)
        len_data += len(data)
        print 'Server received data of size', len(data),  'totalling', len_data
    data = ''.join(all_data)




class ThreadedTCPRequestHandler(SocketServer.BaseRequestHandler):
    def handle(self):
        print 'Start server thread---------------------------------------------'
        coords = recv_bs(self.request)
        cur_thread = threading.currentThread()
        print 'Number of chromosomes:', len(chrs_global)
        print '%s' % (cur_thread.getName())
        response = coords2records_server(coords, chrs_global)
        send_bs(self.request, response)
        print 'End server thread------------------------------------------------'

class ThreadedTCPServer(SocketServer.ThreadingMixIn, SocketServer.TCPServer):
    pass

##def __init__(self, hp, th, genome_dir):
##SocketServer.ThreadingMixIn(hp, th)
##SocketServer.TCPServer.__init__(hp, th)
##self.chrs = dir2genome(genome_dir)
##def __init__(self, genome_dir)


def test_client(ip, port):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    coords = []
    chr = 'chr10'
    start = 1077383
    end = start + 100
    strand = '+'
    name = 'Test'
    coords.append(Loc(chr, start, end, strand, name))
    response = coords2records_client(coords, ip, port)
    print "Received: %s" % response
    print "Received: %s" % response[0].seq
    return


    coords_ps = pickle.dumps(coords)
    sock.connect((ip, port))
    sock.send(coords_ps)
    response_ps = sock.recv(1024)
    response = pickle.loads(response_ps)
    print "Received: %s" % response
    print "Received: %s" % response[0].seq
    sock.close()

if __name__ == "__main__":
    try: 
        genome_dir = sys.argv[1]
    except:
        usage()
        sys.exit(1)

    chrs_global = dir2genome(genome_dir)

    # Port 0 means to select an arbitrary unused port
    host, port = 'localhost', genome_dir_to_port(genome_dir)
    print 'Using host, port', host, port

    try:
        server = ThreadedTCPServer((host, port), ThreadedTCPRequestHandler)
        ip, port = server.server_address
        
        # Start a thread with the server -- that thread will then start one
        # more thread for each request
        server_thread = threading.Thread(target=server.serve_forever)
        # Exit the server thread when the main thread terminates
        server_thread.setDaemon(True)
        server_thread.start()
        print "Server loop running in thread:", server_thread.getName()
        
        test_client(ip, port)
        test_client(ip, port)
        server.serve_forever()
        server.server_close()
        
    except:
        print 'There was an error!'
        server.server_close()
        print sys.exc_info()[0]
        traceback.print_exc()
        
