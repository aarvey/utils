"""
Deal with bowtie files
"""
from __future__ import division
import re, os

_chromosome_map = {
    # human has 22 chr + mt
    'h_sapiens' : dict(zip(range(0,25), 
                       [str(x) for x in range(1,23) + ['MT', 'X', 'Y']])),
    # mouse has 19 chromes
    'm_musculus' : dict(zip(range(0,22), [str(x) for x in range(1,20) + ['MT', 'X', 'Y']]))
}

# _regex_chromosome = {
#     'default' : r'''\W(chr.+?)\W''',
#     'concise' : r'''\d+(+-|\+):<(\d+),\d+,\d+>''' # DON'T CAPTURE STRAND GROUP!
# }

def parse_concise(bfile, chromosome_map, *args, **kwargs):
    """Parse concise bowtie format line by line.
    
    Returns a dict for each line
    
    Concise format::
        
        read_idx{-|+}:<ref_idx,ref_off,mms>
        4-:<1,217538027,0>
        
    Where:
    
      * read_idx is the index of the read mapped
      * {-|+} is the orientation of the read
      * ref_idx is the index of the reference sequence aligned to (0-based)
      * ref_off is the offset into the reference sequence (0-based)
        WARNING: The 0-based offset will likely change in 0.10.1+
      * mms is the number of mismatches in the alignment
      
    Each alignment appears on its own line.
    
    Usage::
    
     Don't call this directly, call parse(...)
        
    """
    regex = re.compile(
        r"""(?P<read_idx>\d+) # the index of the read from input
        (?P<strand>-|\+)      # the strand of alignment
        :<                    # meh
        (?P<ref_idx>\d+),     # index (chr) read aligned to (0-based)
        (?P<ref_off>\d+),     # offset into ref/chromosome  (0-based)
        (?P<mms>\d+)          # number of mismatches in the aglignment
        >""", re.VERBOSE)
    fh = open(bfile, 'r')
    for idx,line in enumerate(fh):
        line = line.strip()
        if not line:
            continue
        match = regex.match(line)
        if match is None:
            raise IOError('Malformed line(?): "' + line + '"')
        info = match.groupdict()
        info['read_idx'] = int(info['read_idx'])
        info['ref_idx'] = int(info['ref_idx'])
        info['ref_off'] = int(info['ref_off'])
        info['mms'] = int(info['mms'])
        yield info
    fh.close()

def parse_default(bfile, chromosome_map='h_sapiens', *args, **kwargs):
    """Parses the default bowtie ASCII output.
    
    Format::
    
      0	-	gi|42406306|ref|NC_000019.8|NC_000019	23976527	\
      TTCGGAGGGAACCAGCTACTAGATGGTTCGAT	IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII	\
      2	3:G>C,28:A>G
    
    """
    colnames = ['read_idx', 'strand', 'ref_idx', 'ref_off', 'tag',
                'phred', 'nobs', 'mms']
    fh = open(bfile, 'r')
    for idx,line in enumerate(fh):
        pieces = line.strip().split('\t')
        read = dict(zip(colnames[:len(pieces)], pieces))
        if ('mms' not in read):
            read['mms'] = '0'
            yield read
    fh.close()

def parse(bfile, organism='h_sapiens', format='default'):
    """Parses bowtie files and yeilds each alignment one at a time.
    
    Parameters
    ----------
    bfile : string
        Path to the bowtie file to parse
    organism : string
        This isn't used
    format : string ('deafult'|'concise')
        The format of the bowtie file
    
    Returns
    -------
    An iterator that provides a dict of info for each line alignment
    in the bowtie file.
    
    Note that all values are returned as strings
    
    Usage
    -----
    
    Usage::
        
        from llutils.aligners import bowtie
        for read in bowtie.parse('/path/to/concise.alignment.txt', format='concise'):
            print 'Chromosome index: %(ref_idx)' % read
     
    """
    parsers = {'concise' : parse_concise, 'default' : parse_default}
    try:
        parser = parsers[format]
    except KeyError,e:
        raise ValueError("``type`` must be either 'concise' or 'default'")
    try:
        chromosome_map = _chromosome_map[organism]
    except KeyError,e:
        raise ValueError("No chromosome:index map for ``" + organism + "``")
    return parser(bfile, chromosome_map)


def split_aligned(bfile, outbase=None, outpath='.', format='default',
                  count_reads=False, verbose=False):
    """Splits the solexa realigned data into separate files (by chromosome)
    
    count_reads = True will report how many times a read aligns to the genome
    and report it to a tab-delimited file::
    
        read    #-of-places-it-aligns-to
    
    The third column is the number of different places any such read occurs.
    I'm ignoring dealing with the number of times the same read appears in the
    sample
    
    """
    from itertools import izip
    if outbase is None:
        outbase = bfile[:bfile.rfind('.')]
    idcount = {}
    def get_file(chromosome, files):
        if chromosome.startswith('chr'):
            matches = chromosome
        else:
            matches = 'splices'
        try:
            return files[matches]
        except KeyError, e:
            files[matches] = open(os.path.join(outpath, outbase + '.' + matches + '.txt'), 'w')
            return files[matches]
    
    files = {}
    bh = open(bfile, 'r')
    for idx,(line,read) in enumerate(izip(bh, parse(bfile, format=format))):
        if verbose and idx % 100000 == 0:
            print "Processing line", idx
        splitfile = get_file(read['ref_idx'], files)
        splitfile.write(line)
        if count_reads:
            idx = int(read['read_idx'])
            idcount[idx] = idcount.get(idx, 0) + 1
    bh.close()
    for filename in files:
        files[filename].close()
    
    if count_reads:
        if verbose:
            print "Serializing read counts ..."
        keys = sorted(idcount.keys())
        fh = open(os.path.join(outpath, outbase + '.readcounts.txt'), 'w')
        for key in keys:
            fh.write('%d\t%d\n' % (key, idcount[key]))
        fh.close()


def to_bed(bfile, outfile, organism='h_sapiens', format='default', split=False,
           track_name='bowtie-export', track_description='bowtie-export',
           sample_name='bowtie-sample', pos_color='', read_length=32,
           chromosome_map=None, verbose_by=None):
    """Converts a bowtie output file to bed
    
    BED File columns (first 3 are mandatory)
    
    cols = ['chromosome', 'start', 'stop', 'name', 'score', 'strand',
            'thick_start', 'thick_stop', 'item_rgb', 'block_count',
            'block_sizes', 'block_starts']
    
    The positions reported in bed files should be 0-indexed
    
    Assumptions
    -----------
    This functions makes some big assumptions about your bed file.
      * A "normal" chromosome starts with "chrXX". Chromosomes with '_' in 
        their name (eg: NM_*** or NR_***) are assumed to align to splice
        junctions

      * Your the ref_idx reported (aka chromosome) in the bowtie file
        should be how you want it (chr1, chr2, etc.)
    
    Parameters
    ----------
    bfile : string
        Path to the bowtie file to parse
    outfile : string
        Path to the output bed file if split=False, otherwise it is the
        basename of the bedfile, eg: OUTFILE.chr1.bed, OUTFILE.chr2.bed, etc
    organism : string
        Not really used
    format : {'default', 'concise'}
    split : boolean
        Whether or not to split the bedfile by chromosome
    
    """
    # key  : number of misalignments
    # value: bed score for read
    read_scores = {0:1000, 1:723, 2:500, 3:278, 4:176}
    
    if format is None:
        # Gues what format the file is since we aren't told
        bowfile = open(bfile, 'r')
        first = bowfile.readline()
        if _concise_re.match(first):
            format = 'concise'
        else:
            format = 'default'
        bowfile.close()
    
    def get_chromosome(read, with_prefix=True):
        chromo = read['ref_idx']
        if with_prefix and chromo[:3] != 'chr':
            chromo = 'chr' + chromo
        if '_' in chromo:
            # This should only happen when a read aligns to our "fake"
            # splice junction chromosome
            chromo = 'splicejx'
        return chromo
    
    bedfiles = {}
    def get_bedfile(read):
        if split:
            key = get_chromosome(read)
            fname = '.'.join((outfile, key, 'bed'))
        else:
            key = 'default'
            fname = outfile
        
        try:
            bedfile = bedfiles[key]
        except KeyError,e:
            header = 'track name="%(name)s" description="%(description)s" ' \
                     'useScore=1 visibility="squish" itemRgb="On"\n'
            insert = {'name' : track_name, 'description' : track_description}
            if key != 'default':
                insert['name'] += ' ' + key
                insert['description'] += ' ' + key
            bedfiles[key] = open(fname, 'w')
            bedfiles[key].write(header % insert)
            bedfile = bedfiles[key]
        return bedfile
    
    def get_readscore(read):
        # TODO: Parse mms in default reads to do a better scoring
        if format == 'concise':
            misaligned = read['mms']
        else:
            misaligned = read['mms'].count('>')
        return read_scores[misaligned]
        
    rgb = {'+' : '255,0,0', '-' : '0,0,255'}
    for idx,read in enumerate(parse(bfile, organism, format=format)):
        if verbose_by is not None and idx % verbose_by == 0:
            print "Parsing read " + str(idx)
        bedfile = get_bedfile(read)
        end = int(read['ref_off']) + read_length - 1
        chromo = get_chromosome(read, with_prefix=True)
        bedfile.write('%(chr)s %(start)d %(end)d %(name)s %(score)s ' \
                      '%(strand)s %(tstart)d %(tend)d %(rgb)s\n' % \
                      {'chr' : chromo,
                       'start' : int(read['ref_off']),
                       'end' : end,
                       'name' : sample_name,
                       'score' : get_readscore(read),
                       'strand' : read['strand'],
                       'tstart' : int(read['ref_off']),
                       'tend' : end,
                       'rgb' : rgb[read['strand']],
                      })
    
    for bedfile in bedfiles:
        bedfiles[bedfile].close()
