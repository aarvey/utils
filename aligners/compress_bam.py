#!/usr/bin/python


import sys, os, bisect, time, pysam, aln, gzip

verbose = True


def main():
    fname = sys.argv[1]
    
    t1 = time.clock()

    samfile = pysam.Samfile(fname)
    out_fname = '%s.compress.bam' % (fname)
    outf = pysam.Samfile(out_fname, "wb", template=samfile)
    rlen = 10
    for i,s in enumerate(samfile.fetch()):
        ##print s.cigar
        ##print s.cigarstring
        s.cigar = ''
        s.cigar = [(0,rlen)]
        ##s.cigarstring = ''
        s.qname = 'n%d' % (i,)
        strand = (not s.is_reverse)*2-1
        #s.rlen = rlen
        #s.alen = rlen
        #s.qlen = rlen
        if (strand == -1):
            s.seq = s.seq[-rlen:]
            s.qual = '' #s.qual[-rlen:]
        else:
            s.seq = s.seq[:rlen]
            s.qual = '' #s.qual[:rlen]

        outf.write(s)

    outf.close()
    t2 = time.clock()
    print 'time to read', t2-t1

    sys.exit(0)

            

if __name__=='__main__':
    main()
