

import sys


class region:
    def __init__(self, chr, start, end):
        self.chr = chr
        self.start = start
        self.end = end

def main():
    f = sys.stdin
    region_strs = sys.argv[1:]
    regions = []
    for r in region_strs:
        s = r.split(':')
        chr = s[0]
        start = int(s[1].split('-')[0])
        end = int(s[1].split('-')[1])
        regions.append(region(chr, start, end))


    for line in f:
        s = line.split('\t')
        for r in regions:
            if  s[2]==chr  and  int(s[3]) > r.start  and  int(s[3]) < r.end:
                sys.stdout.write(line)
                break
    



if __name__=="__main__":
    main()
