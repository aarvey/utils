#!/usr/bin/python
import sys, string, bisect, loc2seq

def main():
    f = sys.stdin
    n = -1
    if len(sys.argv) > 1:
        n = int(sys.argv[1])
    i = 0
    for line in f:
        if i >= n:
            sys.stdout.write(line)
        i += 1
    f.close()


try:     
    main()
except IOError:
    sys.exit(0)
