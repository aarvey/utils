import sys, string, loc2seq

def revcomp(seq,
         table=string.maketrans('ACBDGHKMNSRUTWVYacbdghkmnsrutwvy',
                                'TGVHCDMKNSYAAWBRTGVHCDMKNSYAAWBR'),nl='\n'):
    s = (''.join(seq)).translate(table, nl)[::-1]
    return s
    

def revcomp_min(seq,
         table=string.maketrans('ACGTacgt',
                                'TGCAtgca'),nl='\n'):
    s = (''.join(seq)).translate(table, nl)[::-1]
    return s
    


def main():
    seq = []
    print 'Reading in chrs'
    genomedir = sys.argv[1] # '/unagidata/leslielab/local/share/genomes/mm9'
    fnameroot = sys.argv[2] # 'mm9'
    chrs = loc2seq.dir2genome(genomedir)
    revchrs = {}

    if False:
        print 'Rev-comping chrs'
        for k in chrs.keys():
            print k
            revchrs[k] = revcomp(chrs[k])
            
    print 'Bisulfite converting chrs'
    gtoa_chrs = {}
    for k in chrs.keys():
        print k
        gtoa_chrs[k] = chrs[k].replace('G', 'A').replace('g',  'a')
        chrs[k] = chrs[k].replace('C', 'T').replace('c','t')
        ##revchrs[k] = revchrs[k].replace('C', 'T').replace('c','t')
        

    f = open('%s/%s_bs_plus.fa'%(genomedir, fnameroot), 'w')
    for k in chrs.keys():
        print k
        f.write('>plus_' + k + '\n')
        f.write(chrs[k])
        f.write('\n')

    f = open('%s/%s_bs_minus.fa'%(genomedir, fnameroot), 'w')
    for k in chrs.keys():
        f.write('>minus_' + k + '\n')
        f.write(gtoa_chrs[k])
        f.write('\n')
        
    f.close()

if __name__=='__main__':
    main()
    
    

