
import sys, traceback
import bisect

def usage():
    print 'python match_pairend_alignments.py  alignfile1  alignfile2  outfile'
    print '\t alignfile{1,2}  -- Bowtie alignment files for the left and right ends'
    print '\t                    of paired end reads'
    print '\t                    '
    print '\tThere are components of this code that need to be edited:  '
    print '\t\t get_name(line) --  Should return a unique identifier for a line'
    print '\t\t                    that would match only its pair in the other file'

    


def main():

    verbose = False
    output_matches = True    
    try:
        fname1 = sys.argv[1]
        f1 = open(fname1)
        lines1 = f1.readlines()
        f1.close()
        
        fname2 = sys.argv[2]
        f2 = open(fname2)
        lines2 = f2.readlines()
        f2.close()

        outfname = sys.argv[3]
        if (len(sys.argv)>4):
            output_matches = (int(sys.argv[4]) != 0)
    except:
        print sys.exc_info()[0]
        traceback.print_exc()
        usage()
        sys.exit(1)
    print 'Lines in file 1:', len(lines1)
    print 'Lines in file 2:', len(lines2)
    
    #return(line[:line.index('length')])
    def get_name(line):
        return line.split('\t')[0]
    def get_name(line):
        return line.split('\t')[0][:-2]

    print 'Start sort'
    names1 = map(get_name, lines1)
    print 'got names for left'
    
    print 'Start sort'
    names2 = map(get_name, lines2)
    print 'got names for right'
    ord = range(len(names2))
    ord.sort(key = lambda j : names2[j])
    names2.sort()
    print 'sorted right (for bisection)'


    print 'names1:'
    for i in range(10):
        print('\t%s ->  %s' % (names1[i], lines1[i].strip()))

    print 'names2:'
    for i in range(10):
        print('\t%s' % (names2[i],))

    print 'names2 ordering:'
    for o in ord[:10]:
        print('\t%d ->  %s' % (o, names2[o]))


    testing = False
    if testing:
        joint_names = set(names1)
        joint_names = joint_names.intersection(set(names2))
        print 'Found joint names:'
        tmp = list(joint_names)
        tmp.sort()
        print tmp[:20]
    
    outlines = []
    i = 0
    kk = len(names2)
    for i1, n1 in enumerate(names1):
        tmpidx = bisect.bisect_left(names2, n1)
        
        if verbose:
            #print '\n-------------------------------------------------------------\n'
            #print i1, n1, lines1[i1].strip()
            #print tmpidx, ord[tmpidx], i2, names2[tmpidx], lines2[i2].strip()
            junkjunkjunkjunk = 0 
            
        if tmpidx!=kk and names2[tmpidx]==n1 and output_matches:
            i2 = ord[tmpidx]
            outlines.append(lines1[i1])
            outlines.append(lines2[i2])
        elif (tmpidx==kk or names2[tmpidx]!=n1) and not output_matches:
            outlines.append(lines1[i1])
            

    insert_sizes = []
    def revcomp(s):
        s = s[::-1]
    for i in range(len(outlines)/2):
        line1 =  outlines[i*2+1]
        line2 =  outlines[i*2]
        s1 = line1.split('\t')
        s2 = line2.split('\t')
        loc1 = int(s1[3])
        loc2 = int(s2[3])
        len1 = len(s1[4])
        len2 = len(s2[4])
        insert_size = loc2 - loc1 - len1
        insert_sizes.append(insert_size)
        ##if insert_size < 0 or insert_size > 500: continue
        #print '----------------------------------------------'
        ##print insert_size
        #print line1, line2,   
        #print s1[2], s2[2], loc2, loc1, loc2 - loc1, len1, len2, s1[0], s2[0]
        #print s1[4]
        #print ''.join([' ' for i in range(len1+insert_size)]), s2[4]
        

    print '\n'
    print '\n'
    print '\n'
    print ''.join(outlines[:10])
    print 'Number of outlines:', len(outlines)
    f = open('%s' % (outfname,), 'w')
    ##f = open(sys.stdout)
    f.writelines(outlines)
    f.close()

    f = open('%s.inserts' % (outfname,), 'w')
    f.writelines([str(a)+'\n'  for a in insert_sizes])
    f.close()
    






def order(x, NoneIsLast = True, decreasing = False):
    """
    Returns the ordering of the elements of x. The list
    [ x[j] for j in order(x) ] is a sorted version of x.
    Missing values in x are indicated by None. If NoneIsLast is true,
    then missing values are ordered to be at the end.
    Otherwise, they are ordered at the beginning.
    """
    omitNone = False
    if NoneIsLast == None:
        NoneIsLast = True
        omitNone = True
        
    n  = len(x)
    ix = range(n)
    if None not in x:
        ix.sort(reverse = decreasing, key = lambda j : x[j])
    else:
        # Handle None values properly.
        def key(i, x = x):
            elem = x[i]
            # Valid values are True or False only.
            if decreasing == NoneIsLast:
                return not(elem is None), elem
            else:
                return elem is None, elem
            ix = range(n)
            ix.sort(key=key, reverse=decreasing)
            
    if omitNone:
        n = len(x)
        for i in range(n-1, -1, -1):
            if x[ix[i]] == None:
                n -= 1
        return ix[:n]
    return ix
        

def main22():
    fname1 = sys.argv[1]
    f1 = open(fname1)
    lines1 = f1.readlines()
    f1.close()
    
    fname2 = sys.argv[2]
    f2 = open(fname2)
    lines2 = f2.readlines()
    f2.close()

    print 'Lines in file 1:', len(lines1)
    print 'Lines in file 2:', len(lines2)

    def get_name(line):
        #return(line[:line.index('length')])
        return line.split('\t')[0][:-3]
    
    name2line = {}
    for i in range(len(lines1)):
        name = get_name(lines1[i])
        name2line[name] = [lines1[i]]
        if (i % 100000)==0:
            print i
        
    for j in range(len(lines2)):
        line = lines2[j]
        name = get_name(line)
        if name2line.has_key(name):
            name2line[name].append(line)
        if (j % 100000)==0:
            print j

    outlines = []
    for name, lines in name2line.items():
        if len(lines) < 2:
            continue
        outlines.extend(lines)
        

    f = open('%s.matched' % (sys.argv[1],), 'w')
    f.writelines(outlines)
    f.close()
    










if __name__=="__main__":
    main()
