"""
Deal with importing/exporting/mangling data.

Eland (Solexa Realigned) Format:

  AAAAAAAAAAGAAAAAAAAAAAAAAAAAAAAG 16000 9
  ATTAAATAAGGTCAGCCACATTCATGGGCATC 16000 2
  ACAAGCCAGAACAAATTGGAGCTCTGGCAGAT 0 0
  ACCAAGCCCATGGGCACCTCCCCTCCGGCGGG 13906 1 chr2:105227916 F ACCAAGCCCATGGGCACCTCCCCTCTGGTGGG 12859
  AGAAAACAATAATAGCATGCTAAATCACAAAT 16000 1 chr13:30804195 F AGAAAACAATAATAGCATGCTAAATCACAAAT 12859
 
"""
import os, re

def parse(efile, verbose=False):
    """Yieds (sequence, )
    
    Eland files have a minimum of 3 columns, and maximum of 7:
    Column I   : tag sequence
    Column II  : alignment score, for 32 bp, 16000 = perfect match
                 14593 = 1bp mismatch, 13906 = 2bp mismatch
    Column III : # of hit in the genome, 1 = unique hit
    Column IV  : Chr position
    Column V   : Chr direction
    Column VI  : matched genome sequence
    Column VII : next best possible alignment score
    """
    efile = open(efile, 'r')
    for idx,line in enumerate(efile):
        pieces = line.strip().split(' ')
        result = {'tag' : pieces[0], 'score' : int(pieces[1]), 'nhits' : int(pieces[2])}
        if len(pieces) > 3:
            result['alignment'] = pieces[3]
            result['strand'] = pieces[4]
        if len(pieces) > 5:
            result['rest'] = pieces[5:]
        yield result
    efile.close()
    raise StopIteration("End of File")

def split_eland(efile, outbase=None, outpath='.', splitby=r'''chr(.+):'''):
    """Splits the solexa realigned data into separate files (by chromosome)"""
    if outbase is None:
        outbase = efile[:efile.rfind('.')]
    if isinstance(splitby, str):
        splitby = re.compile(splitby)
    
    def get_file(line, files):
        matches = splitby.findall(line)
        if not matches:
            matches = 'no_chr'
        else:
            matches = matches[0]
        try:
            return files[matches]
        except KeyError, e:
            files[matches] = open(os.path.join(outpath, outbase + '.' + matches + '.txt'), 'w')
            return files[matches]
    
    files = {}
    efile = open(efile, 'r')
    for line in efile:
        splitfile = get_file(line, files)
        splitfile.write(line)
    efile.close()
    for filename in files:
        files[filename].close()
