#include <stdio.h>

// dout is npos-length output array.
// n - number of positions in start and end (and length of tc count array)
// spos - starting position
void csumoverlap(int *n, int *start, int *end, int *spos, int *npos, int *step, int *dout)
{
  int i,j,k;
  k = *step;
  int sp = *spos;
  for(i = 0; i< *n; i++) {
    // size of the window to which the contributions should be added
    int ws=(int) (start[i]- sp);
    int we=(int) (end[i]- sp);
    if(ws<0) { ws=0; } 
    if(we>= *npos) { we= *npos -1; }
    
    for(j=ws;j<=we;++j) {
      dout[j]+=1;
    }
  }
}
