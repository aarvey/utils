
# dout is npos-length output array.
# n - number of positions in start and end (and length of tc count array)
# spos - starting position
def sumoverlap(idx, start, end, count, spos, npos, step, wrap, min_count):
    if len(start) != len(end):
        raise "Error"

    k = step
    sp = spos
    t1 = time.clock()
    dout = [0.0] * (npos-spos)
    t2 = time.clock()
    print 'Time to make output array: %0.2f' % (t2-t1,) 
    
    t1 = time.clock()
    # size of the window to which the contributions should be added
    ##for i in xrange(len(start)):
    select_idx = []
    for i in idx:
        origws = ws = start[i]-sp
        origwe = we = end[i]- sp
        
        if ws<0:  ws = 0
        if we >= npos: we = npos - 1

        incr = 1.0/count[i]
        dout[ws:we] = [a+incr for a in dout[ws:we]]
        select_idx.extend(range(ws,we))
        ##for j in range(ws, we):
        ##    dout[j] += 1

        if wrap and origws<0:
            ws = npos+origws  # origws is negative
            we = npos
            select_idx.extend(range(ws,we))
            dout[ws:we] = [a+incr for a in dout[ws:we]]

        if wrap and origwe>npos:
            ws = 0
            we = origwe-npos
            select_idx.extend(range(ws,we))
            dout[ws:we] = [a+incr for a in dout[ws:we]]
    t2 = time.clock()
    print 'Time to populate output array: %0.2f' % (t2-t1,) 

    t1 = time.clock()
    idx = list(set(select_idx)) 
    idx = [j   for j in idx if dout[j] > min_count]
    #idx = [j   for j in xrange(len(dout)) if dout[j]>0.01]
    pos = [i-spos   for i in idx]
    vals = [dout[i]   for i in idx]
    print len(dout), len(idx), len(pos), len(vals)
    t2 = time.clock()
    print 'Time to select output array: %0.2f' % (t2-t1,)
    return [pos, vals]



def usage():
    print os.path.basename(sys.argv[0]), 'usage:'
    print '  python ~/ll-utils/chromatin/csumoverlap.py FILE EXT MID STEP  GSIZE WRAP'
    print '  FNAME - a bowtie alignment file'
    print '  EXT - amount to extend each read'
    print '  MID - size of middle of extension'
    print '  STEP - step size'
    print '  GSIZE - size of the genome'
    print '  WRAP - should the read wrap around if it goes past the begining/end of the genome? (circular genomes)'
    print ''
    print 'Example: '
    print 'python ~/ll-utils/chromatin/csumoverlap.py wgEncodeHaibTfbsGm12878Six5Pcr1xRawDataRep2.ebv.bowtie.align 150 75 1 ebv.chrom.sizes 1'
    print ''
    print ''
    

import sys, time, os, math, aln
def main():

    try:
        fname = sys.argv[1]
        k = 2
        ext = int(sys.argv[k]);     k += 1
        mid = int(sys.argv[k]);     k += 1
        step = int(sys.argv[k]);     k += 1
        gsize_fname = sys.argv[k];     k += 1
        wrap = 0; use_readlen = 0; stranded = 0; min_count = 0.000001
        if len(sys.argv) > k:   wrap = int(sys.argv[k]);     k += 1
        if len(sys.argv) > k:   use_readlen = int(sys.argv[k]);     k += 1
        if len(sys.argv) > k:   stranded = int(sys.argv[k]);     k += 1
        if len(sys.argv) > k:   min_count = float(sys.argv[k]);     k += 1
    except:
        usage()
        raise()
        
    verbose = False
    #print use_readlen, stranded

    chr_sizes = {}
    f = open(gsize_fname)
    lines = f.readlines()
    for i in range(len(lines)):
        chr = lines[i].split()[0]
        size = lines[i].split()[1]
        chr_sizes[chr] = int(size)
    f.close()

    t1 = time.clock()
    alnfile = aln.alnfile(fname)
    alnfile.read_alns(False)
    records = alnfile.records
    alnfile.close()
    #print records[1].pos
    #print records[1].chr
    #print records[1].tags
    #print records[1].posl
    t2 = time.clock()
    print 'Time to read in: %0.2f' % (t2-t1,)
    del lines
    del alnfile.lines
    
    
    t1 = time.clock()
    start = [0] * len(records)
    end = [0] * len(records)
    chrl = [0] * len(records)
    count = [0] * len(records)
    strands = [0] * len(records)
    total_reads = 0
    for i in range(len(records)):
        s = records[i]
        strand = int(s.strand=='+')*2-1
        strands[i] = strand
        read_len = len(s.read)
        chrl[i] = s.chr
        x = s.pos
        if strand < 0:  x += read_len
        if use_readlen:
            strand = min(0, strand)
            start[i] = x+strand*read_len
            end[i] = start[i]+read_len
        else:
            start[i] = int(x+strand*ext/2-mid/2)-1
            end[i] = int(x+strand*ext/2+mid/2)
        count[i] = float(s.naln+1)

    del records
    del alnfile

    diffs = [e-s  for e,s in zip(end, start)]
    if sum([d<=0  for d in diffs]) > 0:
        raise 'You have starts that are less then or equal to ends!'

    t2 = time.clock()
    print 'Time to make n=%d lists: %0.2f' % (len(start), t2-t1,)
    
        
    t1 = time.clock()
    ord = range(len(start))
    ord.sort(key = lambda j : start[j])
    start = [start[j]   for j in ord]
    end = [end[j]   for j in ord]
    strands = [strands[j]   for j in ord]
    chrl = [chrl[j]   for j in ord]
    t2 = time.clock()
    print 'Time to order in: %0.2f' % (t2-t1,) 


    def output_overlaps(strand=None):
        if strand:
            sstart = [start[i]   for i in range(len(start))  if strands[i]==strand]
            send = [end[i]   for i in range(len(start))  if strands[i]==strand]
            scount = [count[i]   for i in range(len(start))  if strands[i]==strand]
        else:
            sstart = start
            send = end
            scount = count

        sfname = fname
        if strand == -1:
            sfname = '%s.strandminus' % (sfname)
        if strand == 1:
            sfname = '%s.strandplus' % (sfname)


        f = open('%s.vals.wig' % (sfname), 'w')
        chrs = sorted(chr_sizes.keys())
        lines = ['track type=wiggle_0 name="%s" description="%s" visibility=full color=200,100,0 altColor=0,100,200 priority=20\n' % (sfname, sfname)]
        #print lines
        #print chrs
        overlaps_list = []
        for i in range(len(chrs)):
            selected_chr = chrs[i]
            print selected_chr
            idx = [j   for j in xrange(len(chrl)) if chrl[j]==selected_chr]
            if len(idx)==0: continue
            t1 = time.clock()
            overlaps_list = sumoverlap(idx, sstart, send, scount, 0, chr_sizes[selected_chr], step, wrap, min_count)
            t2 = time.clock()
            print 'Time to compute overlaps: %0.2f   (%s)' % (t2-t1,selected_chr)
            t1 = time.clock()
            lines = []
            lines.append("variableStep chrom=%s\n" % (selected_chr,))
            lines.extend(['%d %0.2f\n' % (overlaps_list[0][i], overlaps_list[1][i])  for i in xrange(len(overlaps_list[1]))])
            f.writelines(lines)
            t2 = time.clock()
            print 'Time to write overlaps: %0.2f   (%s)' % (t2-t1,selected_chr)


        f.close()
        return 
        
        f = open('%s.vals.nonorm' % (sfname), 'w')
        f.writelines(['%0.2f\n' % (a)  for i,a in enumerate(overlaps)])
        f.close()
        
        norm = ((mid+1) * float(total_reads)) / float(10**6)
        if verbose:
            print 'mid', mid+1
            print 'norm rpm', norm
            print 'norm', (mid+1) * float(total_reads)
            print 'sum overlaps', sum(overlaps)
        norm = sum(overlaps) / float(10**6)
        overlaps = [float(a) / norm  for a in overlaps]
        print 'sum overlaps', sum(overlaps)

        
        f = open('%s.vals' % (sfname), 'w')
        f.writelines(['%0.2f\n' % (a)  for i,a in enumerate(overlaps)])
        f.close()

        f = open('%s.vals.wig' % (sfname), 'w')
        chrs = sorted(chr_sizes.keys())
        print chrs
        lines = ['track type=wiggle_0 name="%s" description="%s" visibility=full color=200,100,0 altColor=0,100,200 priority=20\n' % (sfname, sfname)]
        for chr in chrs: 
            lines.append("variableStep chrom=chr1\n")
            lines.extend(['%d %0.2f\n' % (i,a)  for i,a in overlaps_list[chr]])
            f.writelines(lines)
        f.close()

    if stranded:
        output_overlaps(1)
        output_overlaps(-1)
    else:
        output_overlaps()
        


if __name__=='__main__':
    main()
    
    #try:
    #    main()
    #except:
    #    pass
    #    ##usage()
    #    ##print "Unexpected error:", sys.exc_info()[0]
    #    ##raise


