def usage():
    print os.path.basename(sys.argv[0]), 'usage:'
    print '  python ~/ll-utils/chromatin/csumoverlap.py FILE EXT MID STEP  GSIZE WRAP'
    print '  FNAME - a bowtie alignment file'
    print '  EXT - amount to extend each read'
    print '  MID - size of middle of extension'
    print '  STEP - step size'
    print '  GSIZE - size of the genome'
    print '  WRAP - should the read wrap around if it goes past the begining/end of the genome? (circular genomes)'
    print '  USE_READ_LEN - use the read length for overlaps'
    print ''
    print 'Example: '
    print 'python ~/ll-utils/chromatin/csumoverlap.py wgEncodeHaibTfbsGm12878Six5Pcr1xRawDataRep2.ebv.bowtie.align 150 75 1 ebv.chrom.sizes 1'
    print ''
    print ''
    

import sys, time, os, math, aln, traceback, array  #, numpy
def main():

    try:
        fname = sys.argv[1]
        k = 2
        ext = int(sys.argv[k]);     k += 1
        mid = int(sys.argv[k]);     k += 1
        step = int(sys.argv[k]);     k += 1
        ##gsize_fname = sys.argv[k];     k += 1
        total_reads = int(sys.argv[k]);     k += 1
        min_dout = float(sys.argv[k]);     k += 1
        min_dout_norm = float(sys.argv[k]);     k += 1
        wrap = 0; use_readlen = 0; stranded = 0; min_count = 0.000001
        if len(sys.argv) > k:   wrap = int(sys.argv[k]);     k += 1
        if len(sys.argv) > k:   use_readlen = int(sys.argv[k]);     k += 1
        if len(sys.argv) > k:   stranded = int(sys.argv[k]);     k += 1
        if len(sys.argv) > k:   min_count = float(sys.argv[k]);     k += 1
    except:
        usage()
        traceback.print_exception(sys.exc_info()[0], sys.exc_info()[1], sys.exc_info()[2])
        raise
        
    verbose = False
    #print use_readlen, stranded

    ##chr_sizes = {}
    ##f = open(gsize_fname)
    ##lines = f.readlines()
    ##for i in range(len(lines)):
    ##    chr = lines[i].split()[0]
    ##    size = lines[i].split()[1]
    ##    chr_sizes[chr] = int(size)
    ##f.close()

    def output_dout(dout, chr):
        t1 = time.clock()
        sfname = '%s.%s' % (fname, chr)
        f = open('%s.vals.wig' % (sfname), 'w')
        lines = []
        lines.append("variableStep chrom=%s\n" % (chr,))
        lines.extend(['%d %0.2f\n' % (i+1, d)  for i,d in enumerate(dout)])
        ##lines.extend(['%d %0.2f\n' % (i+1, d)  for i,d in enumerate(dout) if d > min_dout])
        ##lines.extend(['%d %0.2f\n' % (i+1, max(dout[i:(i+step)]))  for i in range(0,len(dout), step)])
        f.writelines(lines)
        f.close()
        norm = float(total_reads+1) / float(10**6)
        f = open('%s.normvals.wig' % (sfname), 'w')
        lines = []
        lines.append("variableStep chrom=%s\n" % (chr,))
        lines.extend(['%d %0.2f\n' % (i+1, d/norm)  for i,d in enumerate(dout)])
        f.writelines(lines)
        f.close()
        t2 = time.clock()
        print 'Time to write overlaps for %s: %0.2f' % (chr, t2-t1)

    
    
    alnfile = aln.alnfile(fname)
    print alnfile.chr2len
    i = 0
    prev_chr = ''
    t1 = time.clock()
    dout  = [0.0]
    ##for s in alnfile.fetch():
    for s in alnfile:
        if (i+1) % 1000000 == 0:
            print i
        i += 1
        
        read_len = len(s.read)
        x = s.pos
        if s.strand < 0:  x += read_len
        if use_readlen:
            start = x+min(0, s.strand)*read_len
            end = start+read_len
        else:
            start = int(x+s.strand*ext/2-mid/2)-1
            end = int(x+s.strand*ext/2+mid/2)
        count = float(s.naln+1)


        chr_len = npos = alnfile.chr2len[s.chr]
        if prev_chr == '':
            print 'Getting', s.chr
            t1 = time.clock()
            dout = [float(0.0)] * chr_len
            prev_chr = s.chr

        if s.chr != prev_chr:
            t2 = time.clock()
            print 'Time to get %s: %0.2f' % (prev_chr, t2-t1)
            output_dout(dout, prev_chr)
            prev_chr = s.chr
            print 'Getting', prev_chr
            t1 = time.clock()
            dout = [float(0.0)] * chr_len

        origws = ws = start
        origwe = we = end

        ws = max(ws, 0)
        we = min(we, npos-1)
        
        incr = 1.0/count
        dout[ws:we] = [a+incr for a in dout[ws:we]]


        ##if wrap and origws<0:
        ##    ws = npos+origws  # origws is negative
        ##    we = npos
        ##    dout[ws:we] = [a+incr for a in dout[ws:we]]
            
        ##if wrap and origwe>npos:
        ##    ws = 0
        ##    we = origwe-npos
        ##    dout[ws:we] = [a+incr for a in dout[ws:we]]

        ##print s
        ##if i > 200000: sys.exit(0); print dout[:300]; sys.exit(0);    

    t2 = time.clock()
    print 'Time to get %s: %0.2f' % (prev_chr, t2-t1)
    output_dout(dout, prev_chr)


if __name__=='__main__':
    main()
    
    #try:
    #    main()
    #except:
    #    pass
    #    ##usage()
    #    ##print "Unexpected error:", sys.exc_info()[0]
    #    ##raise


