#include <stdio.h>
#include <R.h> // "/home/aarvey/R/R-2.13.1/include/R.h"
#include <Rinternals.h> //"/home/aarvey/R/R-2.13.1/include/Rinternals.h"


int bisection(int *x, int n, int val, int *verbose, int scan_right) {
  if (x[n-1] <= val) {
    return n-1;
  }
  if (x[0] >= val) {
    return -1;
  }
  int left = 0;
  int right = n-1;
  int cntr = 0;
  int idx = 0;
  while (cntr < 1000) {
    cntr += 1;
    idx = (right+left)/2;
    if (x[idx] < val) {
      left = idx;
    } else if (x[idx] > val) {
      right = idx;
    }
    if(x[idx]==val) {
      if(*verbose){printf("eq %d (%d  %d)\n", idx-1, x[idx], val);}
      if(scan_right){while(x[idx+1] == val){idx++;}
      }else{while(x[idx-1] == val){idx--;}}
      return (idx-1);
    }
    if(x[idx-1]==val) {
      idx = idx - 1;
      if(*verbose){printf("eq-1 %d (%d  %d)\n", idx-2, x[idx-1], val);}
      if(scan_right){while(x[idx+1] == val){idx++;}
      }else{while(x[idx-1] == val){idx--;}}
      return (idx-1);
    }
    if (idx < n && x[idx] <= val && val < x[idx+1]) {
      if(*verbose){printf("<n %d (%d  %d<>%d  %d)\n", idx, x[idx-1], x[idx], val, x[idx+1]);}
      return idx;
    } else if (idx > 1 && x[idx-1] <= val && val < x[idx]) {
      if(*verbose){printf(">1 %d (%d  %d<>%d  %d)\n", idx, x[idx-1], val, x[idx], x[idx+1]);}
      return (idx-1);
    }
  }
  warning("Infinite loop");
  return -2;
}

void regionreads(int *x, int *n, int *starts, int *ends, int *m, int *counts, int *start_idx, int *end_idx, int *verbose) {
  int i, s, e;
  for(i = 0; i<*m; i++) {
    s = bisection(x, *n, starts[i], verbose, 0);
    e = bisection(x, *n, ends[i], verbose, 1);
    //if(*verbose){printf("%d  %d (%d-%d)\n", i, s, e, e-s);}
    start_idx[i] = s;
    end_idx[i] = e;
    if(*verbose){printf("%d  %d (%d-%d)\n", i, e-s, s, e);}
    if (s < -1 || e < -1) {
      counts[i] = -1;
    } else {
      counts[i] = e - s;
    }
  }
}

