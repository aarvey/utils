
import sys, string, bisect, os, cPickle, gzip

def read_ld(ld_fname):
    save_fname = '%s.pickled' % ld_fname
    if os.path.exists(save_fname):
        sys.stderr.write('reading from cache \n')
        f = gzip.open(save_fname)
        ret = cPickle.load(f)
        f.close()
        return ret
    
    f = open(ld_fname)
    records = []; revs = []
    i=-1; r2=0; x1=0; x4=0;
    for x in f:
        i = i + 1
        if i==0: continue
        x = x.split()
        r2 = float(x[6])
        x1 = int(x[1])
        x4 = int(x[4])
        records.append((x1,x4,r2))
        if i % 1000000 == 0:
            sys.stderr.write(str(i)+'\n') #print records[-1]
    lines = f.readlines()
    f.close()

    f = gzip.open(save_fname, 'wb')
    cPickle.dump(records, f)
    f.close()
    
    sys.exit(0)

    sys.stderr.write('misc sorting start \n') 
    rev = map(lambda x: (x[1], x[0], x[2]), records)
    revs.sort(key=lambda x: x[0])
    bps = map(lambda x: x[0], records)
    bps2 = map(lambda x: x[0], revs)
    sys.stderr.write('misc sorting end \n') 

    ret = [revs, records, bps, bps2]
    f = open(save_fname, 'wb')
    pickle.dump(ret, f)
    f.close()

def main():
    ld_fname = sys.argv[1]
    query_fname = sys.argv[2]

    read_ld(ld_fname)

    f = open(query_fname)
    qs = f.readlines()
    f.close()
    qs = map(int, qs)
    
    ret = []
    for q in qs:
        i = bisect.bisect_left(bps, q)
        if i != len(bps) and bps[i] == q:
            while i < len(bps) and bps[i] == q:
                ret.append([i, records[i]])
                i = i + 1
        else:
            ret.append([i,(q,-1,-1)])
            
        i = bisect.bisect_left(bps2, q)
        if i != len(bps2) and bps2[i] == q:
            while i < len(bps2) and bps2[i] == q:
                ret.append([-i, revs[i]])
                i = i + 1
        else:
            ret.append([i,(q,-1,-1)])

    for i in range(len(ret)):
        print i, ret[i][0],  ret[i][1][0],  ret[i][1][1],  ret[i][1][2] 
    
    sys.exit(0)
    del lines
    print 'finished with everything'
    while 1:
        lines = []

class record:
    def __init__(self, x):
        x = x.split()
        ##self.chr = x[0]
        self.bp1 = int(x[1])
        self.bp2 = int(x[4])
        self.r2 = float(x[6])
    def __str__(self):
        return '%d %d %0.2f' % (self.bp1, self.bp2, self.r2)

if __name__=='__main__':
    main()
