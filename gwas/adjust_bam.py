#!/usr/bin/python


import sys, os, bisect, time, pysam, aln, gzip

verbose = True


def main():
    fname = sys.argv[1]
    read_name_replace = 'HWI-ST835:438:H733FADXX'
    fname_trim = fname.replace('Sample_', '').split('.')[0]
    print fname_trim
    t1 = time.clock()

    samfile = pysam.Samfile(fname)
    out_fname = '%s.compress.bam' % (fname)
    print samfile.header
    samfile.header['RG'] = [{'ID':fname_trim}]
    print samfile.header
    outf = pysam.Samfile(out_fname, "wb", template=samfile)
    print outf.references
    print outf.header
    ##outf.header['RG'] = [{'ID':fname_trim}]
    ##outf.header = outf.header + {'RG': [{'ID':fname_trim, 'PL':'ILLUMINA', 'LB':fname_trime,  'PI':200, 'DS':fname_trim, 'SM':fname_trim      CN:SC
    rlen = 10
    for i,s in enumerate(samfile.fetch()):
        s.qname = s.qname.replace(read_name_replace, '')
        s.tags = s.tags + [('RG', fname_trim)]
        outf.write(s)
        if i > 100000: break
        continue

    outf.close()
    t2 = time.clock()
    print 'time to read', t2-t1

    sys.exit(0)

            

if __name__=='__main__':
    main()
