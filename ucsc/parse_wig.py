
fnames = []
for i in range(19):
    fnames.append(('chr%d.wig' % (i+1,), '%d' %(i+1,)))
fnames.append(('chrX.wig', 'X'))
fnames.append(('chrY.wig', 'Y'))

print fnames


for (fname, chr) in fnames:
    print fname
    
    f = open(fname)
    lines = f.readlines()
    f.close()

    tracks = []
    track_names = []
    for line in lines:
        if line[:5] == 'track':
            tracks.append([])
            track_names.append(line.split('name=')[1].split()[0])
            continue
        if line[:5] == 'varia':
            continue
        #tracks[-1].append((int(line.split()[0]), float(line.split()[1])))
        tracks[-1].append(line)

    newlines = []
    newlines.append('loc\t' + '\t'.join(track_names) + '\n')
    for i in range(len(tracks[1])):
        newline = []
        newline.append(tracks[0][i].split()[0])
        for track in tracks:
            newline.append(track[i].split()[1])
        newlines.append('\t'.join(newline) + '\n')
    #for track, tname in zip(tracks, track_names):
    #    print tname
    #    outfname = 'chr%s_%s.txt' % (chr, tname)
    #    print outfname
    #    f = open(outfname, 'w')
    #    f.writelines(track)
    #    f.close()
    f = open(fname + '.mat', 'w')
    f.writelines(newlines)
    f.close()
