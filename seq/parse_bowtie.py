ncchr_to_chr = {}
chr_to_num = {}
for i in range(1,23):
    ncchr_to_chr['NC_0000%.2d' % i] = 'chr%d' % i
    chr_to_num['chr%d' % i] = str(i)
ncchr_to_chr['NC_000023'] = 'chrX'
chr_to_num['chrX'] = '23'
ncchr_to_chr['NC_000024'] = 'chrY'
chr_to_num['chrY'] = '24'
ncchr_to_chr['NC_001807'] = 'chrM'
chr_to_num['chrM'] = '25'

    
newlines = []
for i in range(1,7):
    fname = 'erange/polyA_plus_lane%d.bowtie.txt' % (i,)
    f = open(fname)
    lines = f.readlines()
    f.close()
    previd = ''
    for j,line in enumerate(lines):
        s = line.split('\t')
        if int(s[-2]) > 0:
            #print 'skipping'
            continue
        #print s[-2]
        id = s[0]
        #if id==previd:
        #    print newlines
        #    previd = id
        #    continue
        #previd = id
        strand = str(int(s[1]=='+'))
        ncchr = s[2].split('|')[3].split('.')[0]
        chr = ncchr_to_chr[ncchr]
        chr = chr_to_num[ncchr_to_chr[ncchr]]
        start = s[3]
        newlines.append('\t'.join([id, strand, chr, start]) + '\n')
        if j%100000==0:
            print 'count', j
            print 'number of unique hits', len(newlines)
    #print '\n'.join(newlines)
    #break


f = open('unique_mapped_counts.txt', 'w')
f.writelines(newlines)
f.close()



