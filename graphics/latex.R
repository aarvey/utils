

pdfs.to.tex <- function(fnames, fout) {
    latex <- c()
    latex <- c(latex, "\\documentclass[11pt]{article}");     latex <- c(latex, "\\usepackage{graphicx}");     latex <- c(latex, "\\usepackage{fullpage}")
    latex <- c(latex, "\\begin{document}")
    fnames <- fnames
    file.exists(fnames)
    latex <- c(latex, sprintf("\\noindent "))
    if (!is.null(fnames)) {
        for (j in 1:length(fnames)) {
            fname <- fnames[j]
            new.fname <- paste("new", gsub("-pdf$", ".pdf", gsub("\\.", "-", fname)), sep="_")
            system(sprintf("cp %s %s", fname, new.fname))
            latex <- c(latex, sprintf("\\includegraphics[width=1.5in,height=1.5in]{%s}", gsub(".pdf", "", basename(new.fname))))
            if (j%%4==0) {
                latex <- c(latex, sprintf("\\\\"))
            }
        }
    }
    latex <- c(latex, sprintf("\\\\"))
    latex <- c(latex, "\\end{document}")
    latex.out.fname <- sprintf("%s.tex", fout)
    show(latex.out.fname)
    writeLines(latex, latex.out.fname)
    system(sprintf("pdflatex %s > /dev/null", basename(latex.out.fname)))
}



